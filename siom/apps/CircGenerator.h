#ifndef _CircGenerator_h_
#define _CircGenerator_h_

#include "RunControl/Common/Controllable.h"

#include "siom/apps/generator.h"
#include "siom/core/SIOMActiveObject.h"

#define CIRC_GENERATOR_ERR_NOERR      0
#define CIRC_GENERATOR_ERR_ARGS      -1

class CircGeneratorThread : public SIOM::ActiveObject
{
 public:
  CircGeneratorThread ()
  {
    this->start ();
  };

  virtual ~CircGeneratorThread ()
  {
    this->join ();
    generator_end ();
  };

 protected:
  virtual void run ()
  {
    generator_set_running (false);
    generator_loop ();
  };

 public:
  void set_running (bool flag) {generator_set_running (flag);}
};

class CircGenerator : public daq::rc::Controllable
{
 public:
  CircGenerator ()
    : _setup (false) {};

  virtual ~CircGenerator () noexcept {};

  void setup (int argc, char **argv);

  // Inherited from Controllable

  virtual void configure              (const daq::rc::TransitionCmd&);
  virtual void connect                (const daq::rc::TransitionCmd&) {};
  virtual void prepareForRun          (const daq::rc::TransitionCmd&);
  virtual void stopDC                 (const daq::rc::TransitionCmd&);
  virtual void disconnect             (const daq::rc::TransitionCmd&) {};
  virtual void unconfigure            (const daq::rc::TransitionCmd&);
  virtual void publish                ();

 private:
  CircGeneratorThread *_thr;

  bool _setup;
};

inline void CircGenerator::setup (int argc, char **argv)
{
  if (! _setup) 
  {
    generator_args (argc, argv);
    _setup = true;
  }
}

inline void CircGenerator::configure (const daq::rc::TransitionCmd&)
{
  generator_init ();

  _thr = new CircGeneratorThread ();
}

inline void CircGenerator::prepareForRun (const daq::rc::TransitionCmd&)
{
  _thr->set_running (true);
}

inline void CircGenerator::stopDC (const daq::rc::TransitionCmd&)
{
  _thr->set_running (false);
}

inline void CircGenerator::unconfigure (const daq::rc::TransitionCmd&)
{
  delete _thr;
}

inline void CircGenerator::publish ()
{
}

#endif
