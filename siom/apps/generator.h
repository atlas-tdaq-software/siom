#ifndef _generator_h_
#define _generator_h_

/* Global defaults */

#define DEFAULT_MAXEV           -1
#define DEFAULT_SEQLEN           10000
#define DEFAULT_RATE             10.0
#define DEFAULT_LENGTH           780
#define DEFAULT_DLEN             96
#define DEFAULT_PTIME            100
#define DEFAULT_STIME            10
#define DEFAULT_QLEN             100
#define DEFAULT_CIRCROOT         "/tmp/_Circ_"
#define DEFAULT_CIRCLEN          (512*1024)
#define DEFAULT_PORT             9876
#define DEFAULT_SLEEP            1
#define DEFAULT_NTHREAD          1
#define DEFAULT_PARTITION        0

/* Global parameters */

#define GENERATOR_MAXLENSEQ      123456
#define GENERATOR_MAXNTHREAD     256
#define GENERATOR_NTHRLEN        16

/* Error codes */

#define GENERATOR_ERR_NOERR      0
#define GENERATOR_ERR_ARGS      -1
#define GENERATOR_ERR_ALLOC     -2
#define GENERATOR_ERR_SIGINST   -3
#define GENERATOR_ERR_TSERR     -4

/* Prototypes */

void generator_help (char *progname);
void generator_set_running (bool r_flag);
int generator_args (int argc, char **argv);
int generator_init (void);
int generator_loop (void);
int generator_end  (void);

#endif
