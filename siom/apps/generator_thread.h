#ifndef _GENERATOR_THREAD_
#define _GENERATOR_THREAD_

#include <string>

#include "siom/core/SIOMActiveObject.h"

class generator_thread : public SIOM::ActiveObject
{
 public:
  generator_thread ()
    : _running (false)
  {
    this->start ();
  };

  virtual ~generator_thread ()
  {
    this->cleanup ();
    this->join ();
  };

 private:
  virtual void run ();

 public:
  void input (int *lenseq, float *tseq, float *pseq,
	      int nlseq, int ntseq, int npseq);
  void output (char *circname, int buflen, int port, bool nobackpressure);

  int init (int maxev);
  void set_running (bool flag) {_running = flag;};
  bool is_running () {return _running;};

  /* access to internal statistics */

  int events () {return _nev;};
  int thrown () {return _thrown;};
  int circfull () {return _full;};
  float occupancy () {return _occupancy;};
  float rate () {return _rate;};

 private: 
  void cleanup ();

 private:
  bool _running;
  bool _throwIfFull;

  int _nev;
  int _maxev;
  int _full;
  int _thrown;

  int _circlen, _circport, _outputCirc;
  int _nlseq, _ntseq, _npseq;

  uint32_t _node;
  uint32_t _proc;

  float _occupancy;
  float _rate;

  int *_lenseq, *_pl;
  float *_tseq, *_pt;
  float *_pseq, *_pp;

  std::string _circname;
};

#endif
