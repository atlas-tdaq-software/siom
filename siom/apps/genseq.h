#ifndef _GENSEQ_H_
#define _GENSEQ_H_

/* function prototypes */

#ifdef __cplusplus
extern "C" {
#endif

int gen_gauss_seq (float *seq, unsigned int nseq, float mean, float sigma);
int gen_exp_seq   (float *seq, unsigned int nseq, float mean);

#ifdef __cplusplus
}
#endif

/* error codes */

#define SEQ_NOERROR                0
#define SEQ_ERROR_MALLOC          -1

#endif
