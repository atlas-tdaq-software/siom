#ifndef _SIOM_ActiveObject_h_
#define _SIOM_ActiveObject_h_

#include <atomic>
#include <thread>

namespace SIOM
{
  class ActiveObject
  {
   public:
    ActiveObject ()
      : _t (0), _quit (false) {};

    virtual ~ActiveObject () {};

    std::thread::id id ()
    {
      return std::this_thread::get_id ();
    };

   private:
    virtual void run () = 0;

    ActiveObject (const ActiveObject &) = delete;
    ActiveObject& operator= (const ActiveObject &) = delete;

   protected:
    void start ()
    {
      _t = new std::thread (&ActiveObject::run, this);
    };

    void join ()
    {
      if (_t)
      {
	_quit.store (true);
	_t->join ();

	delete _t;
	_t = 0;
      }
    };

   protected:
    std::thread *_t;
    std::atomic <bool> _quit;
  };
}

#endif
