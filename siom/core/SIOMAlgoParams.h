#ifndef _SIOM_AlgoParams_h_
#define _SIOM_AlgoParams_h_

namespace SIOM
{
  class AlgoParams
  {
   public:
    AlgoParams (void *p = 0) : _params (p) {};
    virtual ~AlgoParams () {};

    void  address (void *p) {_params = p;};
    void *address () {return _params;};

   private:
    void *_params;
  };
}

#endif
