#ifndef _SIOMCoreConfig_h_
#define _SIOMCoreConfig_h_

#include <string>

// Include files for the database

#include "config/ConfigObject.h"
#include "config/Configuration.h"

// Include files for the dal

#include "dal/Partition.h"

// IS include files

#include <is/infodictionary.h>
#include <rc/RunParams.h>

// MUCaldal include files

#include "siomdal/SIOMApplicationBase.h"
#include "siomdal/SIOMPlugin.h"
#include "siomdal/SIOMInput.h"
#include "siomdal/SIOMOutput.h"
#include "siomdal/SIOMProcessor.h"

// Package include files

#include "siom/core/SIOMIdentifyble.h"

namespace SIOM
{
  class SIOMCoreConfig : public SIOM::SIOMIdentifyble
  {
   public:
    SIOMCoreConfig (const std::string &SIOM_Name);
    virtual ~SIOMCoreConfig ();

    virtual void get_config ();
    virtual void get_runParams (RunParams &rp);

    Configuration *get_confDB ();
    const IPCPartition *get_partition ();
    const siomdal::SIOMApplicationBase *get_appBase ();

    const std::vector <const siomdal::SIOMInput *> &get_Input ();
    const std::vector <const siomdal::SIOMOutput *> &get_Output ();
    const std::vector <const siomdal::SIOMProcessor *> &get_Processors ();

   private:
    Configuration *m_confDB;
    const daq::core::Partition *m_dbPartition;
    const siomdal::SIOMApplicationBase *m_dbApp;

    // For IS run parameters

    IPCPartition* m_IPCPartition;
    ISInfoDictionary* m_ISDict;
  };
}

inline Configuration *SIOM::SIOMCoreConfig::get_confDB ()
{
  return m_confDB;
}

inline const IPCPartition *SIOM::SIOMCoreConfig::get_partition ()
{
  return m_IPCPartition;
}

inline const siomdal::SIOMApplicationBase *SIOM::SIOMCoreConfig::get_appBase ()
{
  return m_dbApp;
}

inline const std::vector <const siomdal::SIOMInput *> &SIOM::SIOMCoreConfig::get_Input ()
{
  return m_dbApp->get_Input ();
}

inline const std::vector <const siomdal::SIOMOutput *> &SIOM::SIOMCoreConfig::get_Output ()
{
  return m_dbApp->get_Output ();
}

inline const std::vector <const siomdal::SIOMProcessor *> &SIOM::SIOMCoreConfig::get_Processors ()
{
  return m_dbApp->get_Processors ();
}

#endif
