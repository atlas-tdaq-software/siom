#ifndef SIOMCOREEXCEPTION_H
#define SIOMCOREEXCEPTION_H

#include "siom/core/SIOMException.h"

namespace SIOM
{
  class SIOMCoreException : public SIOMException
  {
   public:
    enum ErrorCode { UNCLASSIFIED,
                     CONFIGURATION_NOT_FOUND,
		     IPC_PARTITION_ERROR,
		     IS_DICTIONARY_ERROR,
		     DL_OPEN_ERROR,
		     DL_SYM_ERROR,
		     DB_NOT_LOADED,
		     PARTITION_NOT_FOUND,
		     APPLICATION_NOT_FOUND,
		     NO_INPUT_PLUGIN,
		     NO_OUTPUT_PLUGIN,
		     NO_INTERNAL_POOL,
		     NOT_IN_IS,
                     BUFFER_FULL
    };

    SIOMCoreException (ErrorCode error);
    SIOMCoreException (ErrorCode error, std::string description);
    SIOMCoreException (ErrorCode error, const ers::Context &context);
    SIOMCoreException (ErrorCode error, std::string description,
		       const ers::Context &context);
    SIOMCoreException (const std::exception &cause, ErrorCode error,
		       std::string description, const ers::Context &context);

    virtual ~SIOMCoreException() throw () {};

   protected:
    virtual std::string getErrorString (unsigned int errorId) const;
  };

  inline SIOMCoreException::SIOMCoreException (SIOMCoreException::ErrorCode err) 
    : SIOMException ("SIOMCore", err, getErrorString (err)) {}
   
  inline SIOMCoreException::SIOMCoreException (SIOMCoreException::ErrorCode err,
					       std::string description) 
    : SIOMException ("SIOMCore", err, getErrorString (err), description) {}

  inline SIOMCoreException::SIOMCoreException (SIOMCoreException::ErrorCode err,
					       const ers::Context &context)
    : SIOMException ("SIOMCore", err, getErrorString (err), context) {}
   
  inline SIOMCoreException::SIOMCoreException (SIOMCoreException::ErrorCode err,
					       std::string description,
					       const ers::Context &context) 
    : SIOMException ("SIOMCore", err, getErrorString (err),
		     description, context) {}

  inline SIOMCoreException::SIOMCoreException (const std::exception &cause,
					       SIOMCoreException::ErrorCode err,
					       std::string description,
					       const ers::Context &context) 
    : SIOMException (cause, "SIOMCore", err, getErrorString (err),
		     description, context) {}

  inline std::string SIOMCoreException::getErrorString (unsigned int errorId) const
  {
    std::string rc;
    
    switch (errorId)
    { 
      case UNCLASSIFIED:
	rc = "Unclassified error";
	break;
      case CONFIGURATION_NOT_FOUND:
	rc = "Configuration not found";
	break;
      case IPC_PARTITION_ERROR:
	rc = "IPC partition not created";
	break;
      case IS_DICTIONARY_ERROR:
	rc = "IS dictionary not created";
	break;
      case DL_OPEN_ERROR:
	rc = "Error in loading plugin library";
	break;
      case DL_SYM_ERROR:
	rc = "Error in istantiating plugin class";
	break;
      case DB_NOT_LOADED:
	rc = "Database not properly loaded";
	break;
      case PARTITION_NOT_FOUND:
	rc = "Partition object not found in the database";
	break;
      case APPLICATION_NOT_FOUND:
	rc = "Application not found in the database";
	break;
      case NO_INPUT_PLUGIN:
	rc = "No input plugin for SIOM application";
	break;
      case NO_OUTPUT_PLUGIN:
	rc = "No output plugin for SIOM application";
	break;
      case NO_INTERNAL_POOL:
	rc = "No internal pool defined";
	break;
      case NOT_IN_IS:
	rc = "Required info is not in IS";
	break;
      case BUFFER_FULL:
	rc = "Timeout while terminating thread, proceeding to deletion";
	break;
      default:
	rc = "";
      break;
    }

    return rc;
  }
}
#endif //SIOMCOREEXCEPTION_H
