#ifndef _SIOM_DataDestination_h_
#define _SIOM_DataDestination_h_

namespace SIOM
{
  class DataDestination
  {
   public:
    DataDestination (void *d = 0) : _destination (d) {};
    virtual ~DataDestination () {};

    void  address (void *d) {_destination = d;};
    void *address () {return _destination;};

   private:
    void *_destination;
  };
}

#endif
