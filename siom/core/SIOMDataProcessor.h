#ifndef _SIOM_DataProcessor_h_
#define _SIOM_DataProcessor_h_

#include <map>
#include <vector>
#include <string>

#include <sys/time.h>

// TDAQ include files

#include "is/infodictionary.h"

// Package include files

#include "siom_info/siomPlugin.h"

#include "siom/core/SIOMActiveObject.h"
#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMAlgoParams.h"

#define MBYTE       (1024 * 1024)
#define MINSLEEP    10000000
#define SECTOUSEC   1000000
#define SECTONSEC   1000000000

static const double s_processor_initial_invrate = 2 * ((double) MINSLEEP / SECTONSEC);

namespace SIOM
{
  class DataProcessor : public SIOM::ActiveObject
  {
   public:
    DataProcessor (std::vector <SIOM::MemoryPool <uint8_t> *> &inputPoolVector,
		   std::vector <SIOM::MemoryPool <uint8_t> *> &outputPoolVector,
		   SIOM::AlgoParams *params);

    virtual ~DataProcessor ();

    // Data set

    void maxThreads  (unsigned int maxNThreads)      {m_maxThreads = maxNThreads;};
    void pollFactor  (unsigned int pollRateFactor)   {m_pollFactor = pollRateFactor;};
    void minElements (unsigned int minBufElements)   {m_minElements = minBufElements;};

    // IS Info

    virtual ISInfo *getISInfo ();
    virtual void clearInfo ();

   private:
    virtual void run ();

   private:
    virtual SIOM::MemoryBuffer <uint8_t> *can_send (SIOM::MemoryPool <uint8_t> *outMemoryPool,
                                                    unsigned int input_size);
    virtual unsigned int process_data (SIOM::MemoryBuffer <uint8_t> *in,
                                       SIOM::MemoryBuffer <uint8_t> *out) = 0;

    virtual siom_info::siomPlugin *createInfo () = 0;
    virtual void deleteInfo (ISInfo *info);
    virtual void fillSpecificInfo (ISInfo *info) = 0;

   private:
    // class parameters

    std::vector <SIOM::MemoryPool <uint8_t> *> m_inputPoolVector;
    std::vector <SIOM::MemoryPool <uint8_t> *> m_outputPoolVector;
    SIOM::AlgoParams *m_params;

    unsigned int m_maxThreads;
    unsigned int m_minElements;
    unsigned int m_pollFactor;

    // statistics

    siom_info::siomPlugin *m_info;

    uint64_t m_nof_pack;
    double m_nof_Mbytes;

    struct timeval m_t0;
  };
}

inline SIOM::MemoryBuffer <uint8_t> *SIOM::DataProcessor::can_send (SIOM::MemoryPool <uint8_t> *pool,
                                                                    unsigned int input_size)
{
  return pool->reserve (input_size);
}

inline void SIOM::DataProcessor::deleteInfo (ISInfo *info)
{
  if (info)
    delete info;
}

#endif
