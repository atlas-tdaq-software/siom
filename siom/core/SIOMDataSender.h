#ifndef _SIOM_DataSender_h_
#define _SIOM_DataSender_h_

#include <vector>
#include <sys/types.h>

// TDAQ include files

#include "is/infodictionary.h"

// Package include files

#include "siom_info/siomPlugin.h"

#include "siom/core/SIOMActiveObject.h"
#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"

#define MBYTE       (1024 * 1024)
#define MINSLEEP    10000000
#define SECTOUSEC   1000000
#define SECTONSEC   1000000000

static const double s_sender_initial_invrate = 2 * ((double) MINSLEEP / SECTONSEC);

namespace SIOM
{
  class DataSender : public SIOM::ActiveObject
  {
   public:
    DataSender (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		SIOM::DataDestination *dest);

    virtual ~DataSender ();

    // Data set

    void pollFactor  (unsigned int pollRateFactor)   {m_pollFactor = pollRateFactor;};
    void minElements (unsigned int minBufElements)   {m_minElements = minBufElements;};

    // Statistics

    virtual uint64_t packets ();
    virtual double mbytes ();

    // IS Info

    virtual ISInfo *getISInfo ();
    virtual void clearInfo ();

   private:
    virtual void run ();

   public:
    virtual void startOfRun () {};
    virtual void endOfRun () {};

   private:
    virtual bool can_send () = 0;
    virtual int send (SIOM::MemoryBuffer <uint8_t> *b) = 0;

    virtual siom_info::siomPlugin *createInfo () = 0;
    virtual void deleteInfo (ISInfo *info);
    virtual void fillSpecificInfo (ISInfo *info) = 0;

   private:
    // class parameters

    std::vector <SIOM::MemoryPool <uint8_t> *> m_poolVector;
    SIOM::DataDestination *m_dest;

    unsigned int m_minElements;
    unsigned int m_pollFactor;

    // statistics

    siom_info::siomPlugin *m_info;

    uint64_t m_nof_pack;
    double m_nof_Mbytes;

    struct timeval m_t0;
  };
}

inline uint64_t SIOM::DataSender::packets ()
{
  return m_nof_pack;
}

inline double SIOM::DataSender::mbytes ()
{
  return m_nof_Mbytes;
}

inline void SIOM::DataSender::deleteInfo (ISInfo *info)
{
  if (info)
    delete info;
}

#endif
