#ifndef SIOMERRORREPORTING_H
#define SIOMERRORREPORTING_H

/* Copied from ROSErrorReporting to declare SIOM namespace */

#include "ers/ers.h"
#include "ers/Issue.h"

ERS_DECLARE_ISSUE (SIOM, MessageIssue, , )

/** \def CREATE_SIOM_MESSAGE(instanceName, messageContent) Macro to
    create an object which can bet output to ers through one of the
    ers::info() ers::error() or ers::fatal() methods.

   The messageContent is fed into a stringstream, the output of which
   is used to construct the ers::Issue.  This allows usage of normal
   stream syntax (with <<) to get around the fact that ers streams are
   not streams in the sense of iostream.

*/
#define CREATE_SIOM_MESSAGE(instanceName, messageContent) \
    std::ostringstream instanceName##_tStream; \
    instanceName##_tStream << messageContent; \
    SIOM::MessageIssue instanceName(ERS_HERE, instanceName##_tStream.str());


/** \def SEND_SIOM_INFO(messageContent) Macro to
    create an object and output it to ers through
    the ers::info() method.

   The messageContent is fed into a stringstream, the output of which
   is used to construct the ers::Issue.  This allows usage of normal
   stream syntax (with <<) to get around the fact that ers streams are
   not streams in the sense of iostream.

*/
#define SEND_SIOM_INFO(messageContent) \
{ \
    std::ostringstream tStream; \
    tStream << messageContent; \
    SIOM::MessageIssue msg(ERS_HERE, tStream.str()); \
    ers::info(msg); \
}


/** \def CREATE_SIOM_EXCEPTION(instanceName, exceptionClass, errorCode, messageContent)
   Macro to create a SIOMException object.

   \param instanceName name of the variable hold the instantiated
   exception object

   \param exceptionClass  type of the exception to be instantiated

   \param errorCode   enumerated error code from the exceptionClass

   \param messageContent extra information to be streamed into the
   exception description
*/
#define CREATE_SIOM_EXCEPTION(instanceName, exceptionClass, errorCode, messageContent) \
    std::ostringstream instanceName##_tStream; \
    instanceName##_tStream << messageContent; \
    exceptionClass instanceName(exceptionClass::errorCode, instanceName##_tStream.str(), ERS_HERE);


/** \def ENCAPSULATE_SIOM_EXCEPTION(instanceName, exceptionClass, errorCode, oldException, messageContent)
    Macro to encapsulate an exception inside a SIOMException along with
    some explanatory text.
*/
#define ENCAPSULATE_SIOM_EXCEPTION(instanceName, exceptionClass, errorCode, oldException, messageContent) \
    std::ostringstream instanceName##_tStream; \
    instanceName##_tStream << messageContent; \
    exceptionClass instanceName(oldException, exceptionClass::errorCode, instanceName##_tStream.str(), ERS_HERE)

#endif
