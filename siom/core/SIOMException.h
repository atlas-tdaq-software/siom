#ifndef SIOMEXCEPTION_H
#define SIOMEXCEPTION_H

#include <sstream>
#include <string>
#include <iostream>

#include "ers/ers.h"
#include "ers/Issue.h"

namespace SIOM
{
  class SIOMException : public ers::Issue
  {
    template <class> friend class ers::IssueRegistrator;

   public:
    virtual const std::string getDescription () const;
    const std::string getPackage () const;
    const unsigned int getErrorId () const;
    const std::string getErrorMessage () const;

    virtual ~SIOMException () throw () {};

    SIOMException (std::string package, unsigned int errorId,
		   std::string errorText, const ers::Context &context); 
    SIOMException (std::string package, unsigned int errorId,
		   std::string errorText, std::string description,
		   const ers::Context &context); 
    SIOMException (std::string package, unsigned int errorId,
		   std::string errorText);
    SIOMException (std::string package, unsigned int errorId,
		   std::string errorText, std::string description);
    SIOMException (const std::exception &cause, std::string package,
		   unsigned int error, std::string errorText,
		   std::string description, const ers::Context &context);

   protected:

    SIOMException (const ers::Context &context);
    virtual void raise () const /* throw (std::exception) */ {throw *this;}
    static const char *get_uid () {return "SIOMException";}
    virtual const char *get_class_name () const {return get_uid ();}
    virtual ers::Issue *clone() const {return new SIOMException (*this);}

   private:
    const std::string m_package; 
    const unsigned int m_errorId;
    const std::string m_errorText;
    const std::string m_specificDescription;
    virtual std::ostream &print (std::ostream &stream) const;    
    std::ostream &printErrorMessage (std::ostream &stream) const;    
    std::ostream &printDescription (std::ostream &stream) const;    
    bool m_printSpecificDescription;
    bool m_printFilename;
    const std::string m_file;
    const int m_line;
  };
}

inline std::ostream &SIOM::SIOMException::print (std::ostream &stream) const
{
  return printErrorMessage (stream);  
}

#endif //SIOMEXCEPTION_H
 
