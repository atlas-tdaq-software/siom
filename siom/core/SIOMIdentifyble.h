#ifndef _SIOMIdentifyble_H_
#define _SIOMIdentifyble_H_

#include <string>

namespace SIOM
{
  class SIOMIdentifyble
  {
   public:
    std::string getName ();

   protected:
    void setName (const std::string &name);

   private:
    std::string _name;
  };
}

inline std::string SIOM::SIOMIdentifyble::getName ()
{
  return _name ; 
}

inline void SIOM::SIOMIdentifyble::setName (const std::string &name)
{ 
  _name = name; 
}

#endif // _SIOMIdentifyble_H_
