#ifndef _SIOM_InputServer_h_
#define _SIOM_InputServer_h_

#include <vector>

#include "is/infodictionary.h"

#include "siom/core/SIOMActiveObject.h"
#include "siom/core/SIOMMemoryPool.h"

namespace SIOM
{
  class InputServer : public SIOM::ActiveObject
  {
   public:
    InputServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		 unsigned short maxthreads)
      : _maxthreads (maxthreads), _mempools (mpool)
    {};

    virtual ~InputServer () {};

    virtual ISInfo *getISInfo () = 0;
    virtual void clearInfo () = 0;

    // Statistics

    virtual uint64_t packets () = 0;
    virtual double mbytes () = 0;

    // Parameters

    unsigned short maxthreads ()   {return _maxthreads;};

   protected:
    virtual void run () = 0;

   public:
    virtual void startOfRun () {};
    virtual void endOfRun () {};

   protected:
    unsigned short _maxthreads;
    std::vector <SIOM::MemoryPool <uint8_t> *> _mempools;
  };
}

#endif // _SIOM_InputServer_h
