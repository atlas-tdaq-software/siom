#ifndef _SIOM_MemoryBuffer_h_
#define _SIOM_MemoryBuffer_h_

#include <vector>
#include <deque>
#include <mutex>
#include <cstring>

#include "siom/core/SIOMErrorReporting.h"
#include "siom/core/SIOMCoreException.h"

namespace SIOM
{
  template <class T> class MemoryBuffer
  {
   public:
    MemoryBuffer (std::deque <T *> *free_pages, std::mutex *page_mutex,
		  size_t page_size, size_t page_number)
      :_page_size (page_size), _size (0), _n_objects (0),
       _flag (0), _current_pointer (0), _page_mutex (page_mutex), _free_pages (free_pages)
    {
      std::lock_guard <std::mutex> lock (*_page_mutex);

      if (_free_pages->size () < page_number)
      {
	std::string m = "";

	CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			       SIOMCoreException::BUFFER_FULL, m);
	throw e;
      }
      else
      {
	for (size_t i = 0; i < page_number; ++i)
	  this->add_page ();
      }
    };

    virtual ~MemoryBuffer ()
    {
      std::lock_guard <std::mutex> lock (*_page_mutex);

      for (auto i = _memory_pages.begin (); i != _memory_pages.end (); ++i)
        _free_pages->push_back (*i);
    };

    void flag (uint64_t flag) {_flag = flag;};
    void validate (size_t n, uint64_t flag = 0) {_n_objects = n; _flag = flag;};

    size_t size () {return _page_size * _memory_pages.size ();};
    size_t n_pages () {return _memory_pages.size ();};
    size_t page_size () {return _page_size;};

    size_t n_objects () {return _n_objects;};
    uint64_t flag () {return _flag;};

    std::vector <T *> &get_pages () {return _memory_pages;};

    T *append (T *pointer, size_t nobj)
    {
      if (_current_pointer && nobj <= _size - _n_objects)
      {
        T *p = pointer;

        while (nobj)
        {
          size_t page_pos = _current_pointer - *_current_page;
          size_t page_free = _page_size - page_pos;

          if (page_free == 0)
          {
            ++_current_page;
            _current_pointer = *_current_page;
            page_free = _page_size;
          }

          size_t n_cp = nobj > page_free ? page_free : nobj;

          std::memcpy (static_cast <void *> (_current_pointer),
                       static_cast <void *> (p), n_cp * sizeof (T));

          nobj -= n_cp;
          _n_objects += n_cp;
          _current_pointer += n_cp;
        }
      }

      return _current_pointer;
    };

   private:
    void add_page ()
    {
      _memory_pages.push_back (_free_pages->front ());
      _free_pages->pop_front ();
      
      _size += _page_size;

      if (_current_pointer == 0)
      {
        _current_page = _memory_pages.begin ();
	_current_pointer = *_current_page;
      }
    };
    
   private:
    size_t _page_size;
    size_t _size;
    size_t _n_objects;

    uint64_t _flag;

    T *_current_pointer;

    std::mutex *_page_mutex;
    std::deque <T *> *_free_pages;

    std::vector <T *> _memory_pages;
    typename std::vector <T *>::iterator _current_page;
  };
}

#endif
