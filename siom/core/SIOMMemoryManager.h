#ifndef _SIOM_MemoryManager_h_
#define _SIOM_MemoryManager_h_

#include <map>
#include <string>
#include <mutex>

#include "siom/core/SIOMMemoryPool.h"

namespace SIOM
{
/**
 * Keeps all the references to existing memory pools.
 */

  template <class T> class MemoryManager
  {
   public:
    MemoryManager () {};

    virtual ~MemoryManager ()
    {
      for (auto ipool = _pools.begin (); ipool != _pools.end (); ++ipool)
	delete ipool->second;

      _pools.clear ();
    };

    SIOM::MemoryPool <T> *get_pool (std::string &pool_name)
    {
      SIOM::MemoryPool <T> *m;
      typename std::map <std::string, SIOM::MemoryPool <T> *>::iterator miter;

      std::lock_guard <std::mutex> lock (_memory_manager_mutex);

      if ((miter = _pools.find (pool_name)) == _pools.end ())
	m = 0;
      else
	m = miter->second;

      return m;
    };

    SIOM::MemoryPool <T> *create_pool (std::string &pool_name, size_t page_size, size_t page_number)
    {
      SIOM::MemoryPool <T> *m;
      typename std::map <std::string, SIOM::MemoryPool <T> *>::iterator miter;

      std::lock_guard <std::mutex> lock (_memory_manager_mutex);

      if ((miter = _pools.find (pool_name)) == _pools.end ())
      {
	m = new SIOM::MemoryPool <T> (page_size, page_number);
	std::pair <std::string, SIOM::MemoryPool <T> *> mpair (pool_name, m);
	_pools.insert (mpair);
      }
      else
	m = miter->second;

      return m;
    };

    void delete_pool (std::string &pool_name)
    {
      typename std::map <std::string, SIOM::MemoryPool <T> *>::iterator miter;

      std::lock_guard <std::mutex> lock (_memory_manager_mutex);

      if ((miter = _pools.find (pool_name)) != _pools.end ())
      {
	delete miter->second;
	_pools.erase (miter);
      }
    };

   private:
    std::map <std::string, MemoryPool <T> *> _pools;
    std::mutex _memory_manager_mutex;
  };
}

#endif
