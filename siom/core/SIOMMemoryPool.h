#ifndef _SIOM_MemoryPool_h_
#define _SIOM_MemoryPool_h_

#include <deque>
#include <mutex>

#include "siom/core/SIOMCoreException.h"
#include "siom/core/SIOMMemoryBuffer.h"

namespace SIOM
{
/**
 * Provides fast and safe read/write access to a
 * paged memory pool from any number of I/O threads.
 * All the critical operations are protected by 
 * using mutexes.
 */

  template <class T> class MemoryPool
  {
   public:
    MemoryPool (size_t page_size, size_t n_pages)
      : _n_pages (n_pages), _page_size (page_size),
        _max_size (n_pages * page_size),
        _buf (std::unique_ptr <T []> (new T [_max_size]))
    {
      T *p = _buf.get ();

      for (size_t i = 0; i < _n_pages; ++i, p += _page_size)
	_free_pages.push_back (p);
    };

    virtual ~MemoryPool () {};

    bool full () {return _free_pages.empty ();};
    bool empty () {return (_free_pages.size () == _n_pages);};

    size_t page_size () {return _page_size;};
    size_t page_number () {return _n_pages;};

    size_t free_pages () {return _free_pages.size ();};
    size_t used_pages () {return (_n_pages - _free_pages.size ());};
    size_t elements () {return _buffer_q.size ();};

    SIOM::MemoryBuffer <T> *reserve (size_t n_objects)
    {
      size_t np = 1 + ((n_objects - 1) / _page_size);

      try
      {
	return new SIOM::MemoryBuffer <T> (&_free_pages, &_page_mutex, _page_size, np);
      }

      catch (...)
      {
	return 0;
      }
    };

    void validate (SIOM::MemoryBuffer <T> *buffer, size_t nwritten, uint64_t flag = 0)
    {
      buffer->validate (nwritten, flag);

      // Queue the pointer to buffer

      std::lock_guard <std::mutex> lock (_buffer_mutex);
      _buffer_q.push_back (buffer);
    };

    SIOM::MemoryBuffer <T> *get_next ()
    {
      // Pop buffer object from queue (if any)

      std::lock_guard <std::mutex> lock (_buffer_mutex);

      if (_buffer_q.empty ())
        return 0;

      SIOM::MemoryBuffer <T> *b = _buffer_q.front ();
      _buffer_q.pop_front ();

      return b;
    };

    SIOM::MemoryBuffer <T> *get_next_require_flag (uint64_t flag)
    {
      // Pop buffer object from queue (if any)

      std::lock_guard <std::mutex> lock (_buffer_mutex);

      if (_buffer_q.empty ())
        return 0;

      SIOM::MemoryBuffer <T> *b = _buffer_q.front ();

      if ((b->flag () & flag) == flag)
      {
	_buffer_q.pop_front ();
	return b;
      }

      return 0;
    };

    SIOM::MemoryBuffer <T> *get_next_exclude_flag (uint64_t flag)
    {
      // Pop buffer object from queue (if any)

      std::lock_guard <std::mutex> lock (_buffer_mutex);

      if (_buffer_q.empty ())
        return 0;

      SIOM::MemoryBuffer <T> *b = _buffer_q.front ();

      if ((b->flag () & flag) == flag)
	return 0;

      _buffer_q.pop_front ();

      return b;
    };

    void requeue (SIOM::MemoryBuffer <T> *b, uint64_t flag = 0)
    {
      b->flag (flag);

      std::lock_guard <std::mutex> lock (_buffer_mutex);
      _buffer_q.push_back (b);
    };

    void release (SIOM::MemoryBuffer <T> *b)
    {
      delete b;
    };

   private:
    const size_t _n_pages;
    const size_t _page_size;
    const size_t _max_size;

    std::mutex _page_mutex;
    std::mutex _buffer_mutex;

    std::deque <T *> _free_pages;
    std::deque <SIOM::MemoryBuffer <T> *> _buffer_q;

    std::unique_ptr <T []> _buf;
  };
}

#endif
