#ifndef _SIOMPluginFactory_h_
#define _SIOMPluginFactory_h_

#include <dlfcn.h>

#include <iostream>
#include <string>
#include <map>

#include "siom/core/SIOMCoreException.h"
#include "siom/core/SIOMErrorReporting.h"

namespace SIOM
{
  template <class Type> class SIOMPluginFactory
  {
    typedef Type *(*Create) ();
    typedef Type *(*CreateWithConfig) (void *);

   private:
    class SharedLibrary
    {
     public:
      SharedLibrary (void *handle, unsigned long create)
	: _handle (handle), _create (create) {};

      void *handle () {return _handle;};
      unsigned long create () {return _create;};

     private:
      void *_handle;
      unsigned long _create;
    };

    static std::map <std::string, SharedLibrary *> _constructorMap;

   public:

    void load (std::string name);
    Type *create (std::string name, void *configuration = 0);
    void unload (std::string name);
  };
}

template <class Type> 
std::map <std::string, typename SIOM::SIOMPluginFactory <Type>::SharedLibrary *> SIOM::SIOMPluginFactory <Type>::_constructorMap;

template <class Type>
void SIOM::SIOMPluginFactory <Type>::load (std::string name)
{
  if (_constructorMap.find (name) == _constructorMap.end ())
  {
    std::string library="lib" + name + ".so";
    std::cout << "PluginFactory: loading " << name << std::endl;

    void *dlHandle;

    if ((dlHandle = dlopen (library.c_str (), RTLD_LAZY | RTLD_GLOBAL)) == NULL)
    {
      char *error;
      std::string str_error;

      if ((error = dlerror ()) != 0)
      {
	str_error = error;

	CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			       SIOMCoreException::DL_OPEN_ERROR, str_error);
	throw e;
      }
    }
    else
    {
      std::string constructorName = "create" + name;
      unsigned long class_create = (unsigned long) dlsym (dlHandle, constructorName.c_str ());

      char *error;
      std::string str_error;

      if ((error = dlerror ()) != 0)
      {
	str_error = error;

	CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			       SIOMCoreException::DL_SYM_ERROR, str_error);
	throw e;
      }

      _constructorMap[name] = new SharedLibrary (dlHandle, class_create);
    }
  }
}

template <class Type>
void SIOM::SIOMPluginFactory <Type>::unload (std::string name)
{
  if (_constructorMap.find (name) != _constructorMap.end ())
  {
    dlclose (_constructorMap[name]->handle ());

    delete _constructorMap[name];
    _constructorMap.erase (name);
  }
  else
  {
    std::string strerror = "Cannot unload " + name;

    CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			   SIOMCoreException::DL_OPEN_ERROR, strerror);
    throw e;
  }
}

template <class Type>
Type *SIOM::SIOMPluginFactory <Type>::create (std::string name, void *configuration)
{
  if (_constructorMap.find (name) != _constructorMap.end ())
  {
    if (configuration == 0)
    {
      Create create = (Create) _constructorMap[name]->create ();
      return (create ());
    }
    else
    {
      CreateWithConfig create = (CreateWithConfig) _constructorMap[name]->create ();
      return (create (configuration));
    }
  }
  else
  {
    std::string strerror = "Cannot create " + name;

    CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			   SIOMCoreException::DL_OPEN_ERROR, strerror);
    throw e;
  }
}

#endif
