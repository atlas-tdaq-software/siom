#ifndef _SIOM_Sampler_h_
#define _SIOM_Sampler_h_

#include <iostream>
#include <map>
#include <string>
#include <set>

#include "emon/EventSampler.h"

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMActiveObject.h"

#define MBYTE       (1024 * 1024)

#define SIOM_SAMPLER_FLAG 0x1

namespace SIOM
{
  class SIOMSampler: public SIOM::ActiveObject, public emon::PushSampling
  {
   public:
    SIOMSampler (const emon::SelectionCriteria &sc,
    		 emon::EventChannel *channel,
		 std::set <SIOM::MemoryPool <uint8_t>* > &pools,
		 char *header = 0, size_t hsize = 0)
      : _criteria (sc), _channel (channel), _inpools (pools),
        _header (header), _header_size (hsize),
        _sampled (0), _mbytes (0),
        _seqdata (0), _seqdatalen (0)
    {
      _requeue = true;
      this->start ();
    };

    SIOMSampler (const emon::SelectionCriteria &sc,
    		 emon::EventChannel *channel,
		 std::set <SIOM::MemoryPool <uint8_t>* > &inpools,
		 std::set <SIOM::MemoryPool <uint8_t>* > &outpools,
		 char *header = 0, size_t hsize = 0)
      : _criteria (sc), _channel (channel), _inpools (inpools), _outpools (outpools),
        _header (header), _header_size (hsize),
        _sampled (0), _mbytes (0),
        _seqdata (0), _seqdatalen (0)
    {
      _requeue = false;
      this->start ();
    };

    virtual ~SIOMSampler ()
    {
      this->join ();

      if (_seqdata)
	delete [] _seqdata;

      std::cout << "Sampled : " << _sampled << std::endl;
      std::cout << "Sampled MB: " << _mbytes << std::endl;
    };

    // Statistics

    uint64_t sampled_events ();
    double   sampled_mbytes ();

   private:
    void pushEvent (char *pointer, unsigned int size);
    void pushEvent (SIOM::MemoryBuffer <uint8_t> *buffer);
    void pushEvent (unsigned int *pointer, size_t length);

    unsigned int copy_data (SIOM::MemoryBuffer <uint8_t> *input,
			    SIOM::MemoryBuffer <uint8_t> *output);

   private:
    virtual void run ();

   private:
    static const unsigned int s_uint_size = sizeof (unsigned int);
    static const uint64_t s_siom_sampler_flag = SIOM_SAMPLER_FLAG;

    emon::SelectionCriteria                    _criteria;
    emon::EventChannel                        *_channel;
    std::set <SIOM::MemoryPool <uint8_t>* >    _inpools;
    std::set <SIOM::MemoryPool <uint8_t>* >    _outpools;

    bool _requeue;

    // Header and header size if needed

    char  *_header;
    size_t _header_size;

    // Statistics

    uint64_t _sampled;
    double   _mbytes;

    // Private storage for header prepending

    char  *_seqdata;
    size_t _seqdatalen;
  };
}

// Access to statistics

inline uint64_t SIOM::SIOMSampler::sampled_events ()
{
  return _sampled;
}

inline double SIOM::SIOMSampler::sampled_mbytes ()
{
  return _mbytes;
}

inline void SIOM::SIOMSampler::pushEvent (unsigned int *pointer, size_t length)
{
  // Send event

  _channel->pushEvent (pointer, length);

  ++_sampled;
  _mbytes += (double) (s_uint_size * length) / MBYTE;
}

inline unsigned int SIOM::SIOMSampler::copy_data (SIOM::MemoryBuffer <uint8_t> *input,
						  SIOM::MemoryBuffer <uint8_t> *output)
{
  size_t to_copy = input->n_objects ();

  if (to_copy > output->size ())
    return 0;

  auto in_pages = input->get_pages ();

  for (auto pagein = in_pages.begin (); pagein != in_pages.end () && to_copy; ++pagein)
  {
    size_t n = to_copy > input->page_size () ? input->page_size () : to_copy;

    output->append (*pagein, n);
    to_copy -= n;
  }

  return output->n_objects ();
}

#endif
