#ifndef _SIOMSamplerFactory_h_
#define _SIOMSamplerFactory_h_

#include <set>

#include "emon/EventSampler.h"
#include "emon/PushSamplingFactory.h"

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMSampler.h"

namespace SIOM
{
  class SIOMSamplerFactory : public emon::PushSamplingFactory
  {
   public:
    SIOMSamplerFactory (char *header = 0, unsigned int header_length = 0)
      : _requeue (true), _header_length (header_length),
        _header (std::unique_ptr <char []> (new char [header_length]))
    {
      char *p = _header.get ();

      for (unsigned int i = 0; i < _header_length; ++i)
	*(p + i) = header[i];
    };

    SIOMSamplerFactory (bool requeue, char *header = 0, unsigned int header_length = 0)
      : _requeue (requeue), _header_length (header_length),
        _header (std::unique_ptr <char []> (new char [header_length]))
    {
      char *p = _header.get ();

      for (unsigned int i = 0; i < _header_length; ++i)
	*(p + i) = header[i];
    };

    virtual emon::PushSampling *startSampling (const emon::SelectionCriteria &criteria,
					       emon::EventChannel *channel);

    void add_pool       (SIOM::MemoryPool <uint8_t> *p) {this->add_inpool (p);};
    void remove_pool    (SIOM::MemoryPool <uint8_t> *p) {this->remove_inpool (p);};

    void add_inpool     (SIOM::MemoryPool <uint8_t> *p) {_inpools.insert (p);};
    void remove_inpool  (SIOM::MemoryPool <uint8_t> *p) {_inpools.erase (p);};

    void add_outpool    (SIOM::MemoryPool <uint8_t> *p);
    void remove_outpool (SIOM::MemoryPool <uint8_t> *p);

   private:
    bool   _requeue;
    size_t _header_length;
    std::unique_ptr <char []> _header;

    std::set <SIOM::MemoryPool <uint8_t>* > _inpools;
    std::set <SIOM::MemoryPool <uint8_t>* > _outpools;
  };
}

inline void SIOM::SIOMSamplerFactory::add_outpool (SIOM::MemoryPool <uint8_t> *p)
{
  if (! _requeue)
    _outpools.insert (p);
}

inline void SIOM::SIOMSamplerFactory::remove_outpool (SIOM::MemoryPool <uint8_t> *p)
{
  if (! _requeue)
    _outpools.erase (p);
}

inline emon::PushSampling *SIOM::SIOMSamplerFactory::startSampling (const emon::SelectionCriteria &criteria,
								    emon::EventChannel *channel)
{
  SIOM::SIOMSampler *s;

  if (_requeue)
    s = new SIOM::SIOMSampler (criteria, channel, _inpools, _header.get (), _header_length);
  else
    s = new SIOM::SIOMSampler (criteria, channel, _inpools, _outpools, _header.get (), _header_length);

  return s;
}

#endif
