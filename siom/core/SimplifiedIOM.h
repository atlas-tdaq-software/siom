#ifndef _SimplifiedIOM_h_
#define _SimplifiedIOM_h_

#include <sys/times.h>

#include <list>
#include <set>
#include <mutex>

#include "is/infodictionary.h"
#include "RunControl/Common/Controllable.h"

#include "siom/core/SIOMCoreConfig.h"
#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/SIOMPluginFactory.h"
#include "siom/core/pSIOMPlugin.h"
#include "siom/core/pSIOMInput.h"
#include "siom/core/pSIOMOutput.h"
#include "siom/core/pSIOMProcessor.h"

namespace SIOM
{
  class SimplifiedIOM : public daq::rc::Controllable
  {
   public:
    explicit SimplifiedIOM (const std::string &name);
    virtual ~SimplifiedIOM () noexcept;

    virtual void configure                  (const daq::rc::TransitionCmd&);
    virtual void connect                    (const daq::rc::TransitionCmd&);
    virtual void prepareForRun              (const daq::rc::TransitionCmd&);
    virtual void stopDC                     (const daq::rc::TransitionCmd&);
    virtual void disconnect                 (const daq::rc::TransitionCmd&);
    virtual void unconfigure                (const daq::rc::TransitionCmd&);
    virtual void publish                    ();

    void setInteractive (bool interactive);

   private:
    void publishSpecificInfo (const std::string setName,
			      const IPCPartition *ipcPartition, ISInfo *info);
    void cleanup ();
    void isCleanup ();
    void initInfo ();
    void clearInfo ();

   private:
    // Interactive mode flag

    bool m_interactiveMode;

    // Application name

    const std::string m_name;

    // Core configuration

    SIOM::SIOMCoreConfig *core_conf;

    // Factories for creating plug-ins

    SIOM::SIOMPluginFactory <SIOM::SIOMInput>     inputFactory;
    SIOM::SIOMPluginFactory <SIOM::SIOMOutput>    outputFactory;
    SIOM::SIOMPluginFactory <SIOM::SIOMProcessor> processorFactory;

    // Vector of active plug-in

    std::list <SIOM::SIOMInput *> inList;
    std::list <SIOM::SIOMOutput *> outList;
    std::list <SIOM::SIOMProcessor *> procList;
    std::list <SIOM::SIOMPlugin *> pluginList;

    // SIOM memory manager

    SIOM::MemoryManager <uint8_t> m_memory_manager;

    // IS server name

    std::string m_isInfoName;

    // IS items from plugins

    std::set <std::string> m_isItems;

    // Mutex to avoid conflicts in publication and publish according to state

    bool m_publish;
    std::mutex m_publish_mutex;

    // Variables for statistics

    uint64_t m_old_ipack, m_old_opack;
    double   m_old_idata, m_old_odata;

    unsigned int m_numProcessors;
    clock_t m_lastCPU, m_lastSysCPU, m_lastUserCPU;

    struct timeval m_starttime, m_probetime;
  };
}

inline void SIOM::SimplifiedIOM::setInteractive (bool interactive)
{
  m_interactiveMode = interactive;
}

#endif
