#ifndef _SIOMInput_h_
#define _SIOMInput_h_

#include <vector>
#include <map>
#include <string>

#include "is/infodictionary.h"
#include "config/Configuration.h"
#include "config/DalObject.h"

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMInputServer.h"
#include "siom/core/pSIOMPlugin.h"

namespace SIOM
{
  class SIOMInput : public SIOM::SIOMPlugin
  {
   public:
    SIOMInput ();
    virtual ~SIOMInput () noexcept;

    unsigned int max_threads ();
    std::vector <SIOM::MemoryPool <uint8_t> *> &memory_pools ();

    virtual void configure (const daq::rc::TransitionCmd &);
    virtual void connect (const daq::rc::TransitionCmd &);
    virtual void prepareForRun (const daq::rc::TransitionCmd &);
    virtual void stopDC (const daq::rc::TransitionCmd &);
    virtual void disconnect (const daq::rc::TransitionCmd &);
    virtual void unconfigure (const daq::rc::TransitionCmd &);

    virtual void clearInfo ();
    virtual ISInfo *getISInfo ();

    // Statistics

    virtual uint64_t packets ();
    virtual double mbytes ();

   private:
    virtual void plugin_setup (SIOM::MemoryManager <uint8_t> *memory_manager,
			       SIOM::SIOMCoreConfig *core_conf,
			       const DalObject *appConfiguration,
			       const DalObject *objConfiguration, bool debug);
    virtual void plugin_unsetup ();

    void configure ();
    void connect () {};
    void prepareForRun ();
    void stopDC ();
    void disconnect () {};
    void unconfigure ();

    virtual SIOM::InputServer *createServer () = 0;
    virtual void deleteServer (SIOM::InputServer *server);

   private:
    bool m_dbConfig;

    unsigned int m_maxthreads;

    SIOM::InputServer *m_server;
    SIOM::MemoryManager <uint8_t> *m_memory_manager;

    std::vector <std::string> m_poolNames;
    std::vector <SIOM::MemoryPool <uint8_t> *> m_poolVector;
  };
}

inline void SIOM::SIOMInput::configure (const daq::rc::TransitionCmd &)
{
  this->configure ();
}

inline void SIOM::SIOMInput::connect (const daq::rc::TransitionCmd &)
{
  this->connect ();
}

inline void SIOM::SIOMInput::prepareForRun (const daq::rc::TransitionCmd &)
{
  this->prepareForRun ();
}

inline void SIOM::SIOMInput::stopDC (const daq::rc::TransitionCmd &)
{
  this->stopDC ();
}

inline void SIOM::SIOMInput::disconnect (const daq::rc::TransitionCmd &)
{
  this->disconnect ();
}

inline void SIOM::SIOMInput::unconfigure (const daq::rc::TransitionCmd &)
{
  this->unconfigure ();
}

inline uint64_t SIOM::SIOMInput::packets ()
{
  return (m_server ? m_server->packets () : 0);
}

inline double SIOM::SIOMInput::mbytes ()
{
  return (m_server ? m_server->mbytes () : 0);
}

inline unsigned int SIOM::SIOMInput::max_threads ()
{
  return m_maxthreads;
}

inline std::vector <SIOM::MemoryPool <uint8_t> *> &SIOM::SIOMInput::memory_pools ()
{
  return m_poolVector;
}

inline void SIOM::SIOMInput::deleteServer (SIOM::InputServer *server)
{
  delete server;
}

inline void SIOM::SIOMInput::clearInfo ()
{
  if (m_server) m_server->clearInfo ();
}

inline ISInfo *SIOM::SIOMInput::getISInfo ()
{
  return (m_server ? m_server->getISInfo () : 0);
}

#endif
