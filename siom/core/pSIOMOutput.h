#ifndef _SIOMOutput_h_
#define _SIOMOutput_h_

#include <vector>
#include <map>
#include <string>

#include "is/infodictionary.h"
#include "config/Configuration.h"
#include "config/DalObject.h"

#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataSender.h"
#include "siom/core/pSIOMPlugin.h"

namespace SIOM
{
  class SIOMOutput : public SIOM::SIOMPlugin
  {
   public:
    SIOMOutput ();
    virtual ~SIOMOutput () noexcept;

    unsigned int min_buf_elements ();
    unsigned int poll_rate_factor ();
    std::vector <SIOM::MemoryPool <uint8_t> *> &memory_pools ();

    virtual void configure (const daq::rc::TransitionCmd &);
    virtual void connect (const daq::rc::TransitionCmd &);
    virtual void prepareForRun (const daq::rc::TransitionCmd &);
    virtual void stopDC (const daq::rc::TransitionCmd &);
    virtual void disconnect (const daq::rc::TransitionCmd &);
    virtual void unconfigure (const daq::rc::TransitionCmd &);

    virtual void clearInfo ();
    virtual ISInfo *getISInfo ();

    // Statistics

    virtual uint64_t packets ();
    virtual double mbytes ();

   private:
    virtual void plugin_setup (SIOM::MemoryManager <uint8_t> *memory_manager,
			       SIOM::SIOMCoreConfig *core_conf,
			       const DalObject *appConfiguration,
			       const DalObject *objConfiguration, bool debug);
    virtual void plugin_unsetup ();

    void configure ();
    void connect ();
    void prepareForRun ();
    void stopDC ();
    void disconnect ();
    void unconfigure ();

    virtual SIOM::DataSender *createSender () = 0;
    virtual void deleteSender (SIOM::DataSender *sender);

   private:
    bool m_dbConfig;

    unsigned int m_minBufElements;
    unsigned int m_pollRateFactor;

    SIOM::DataSender *m_sender;
    SIOM::MemoryManager <uint8_t> *m_memory_manager;

    std::vector <std::string> m_poolNames;
    std::vector <SIOM::MemoryPool <uint8_t> *> m_poolVector;
  };
}

void SIOM::SIOMOutput::configure (const daq::rc::TransitionCmd &)
{
  this->configure ();
}

void SIOM::SIOMOutput::connect (const daq::rc::TransitionCmd &)
{
  this->connect ();
}

void SIOM::SIOMOutput::prepareForRun (const daq::rc::TransitionCmd &)
{
  this->prepareForRun ();
}

void SIOM::SIOMOutput::stopDC (const daq::rc::TransitionCmd &)
{
  this->stopDC ();
}

void SIOM::SIOMOutput::disconnect (const daq::rc::TransitionCmd &)
{
  this->disconnect ();
}

void SIOM::SIOMOutput::unconfigure (const daq::rc::TransitionCmd &)
{
  this->unconfigure ();
}

inline uint64_t SIOM::SIOMOutput::packets ()
{
  return (m_sender ? m_sender->packets () : 0);
}

inline double SIOM::SIOMOutput::mbytes ()
{
  return (m_sender ? m_sender->mbytes () : 0);
}

inline unsigned int SIOM::SIOMOutput::min_buf_elements ()
{
  return m_minBufElements;
}

inline unsigned int SIOM::SIOMOutput::poll_rate_factor ()
{
  return m_pollRateFactor;
}

inline std::vector <SIOM::MemoryPool <uint8_t> *> &SIOM::SIOMOutput::memory_pools ()
{
  return m_poolVector;
}

inline void SIOM::SIOMOutput::deleteSender (SIOM::DataSender *sender)
{
  delete sender;
}

inline void SIOM::SIOMOutput::clearInfo ()
{
  if (m_sender) m_sender->clearInfo ();
}

inline ISInfo *SIOM::SIOMOutput::getISInfo ()
{
  return (m_sender ? m_sender->getISInfo () : 0);
}

#endif
