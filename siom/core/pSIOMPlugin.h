#ifndef _SIOMPlugin_h_
#define _SIOMPlugin_h_

#include <string>

#include "is/infodictionary.h"
#include "dal/Partition.h"
#include "config/Configuration.h"
#include "config/DalObject.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/RunControlCommands.h"

#include "siom/core/SIOMIdentifyble.h"
#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/SIOMCoreConfig.h"

namespace SIOM
{
  class SIOMPlugin : public SIOM::SIOMIdentifyble, public daq::rc::Controllable
  {
   public:
    SIOMPlugin () {};
    virtual ~SIOMPlugin () noexcept {};

   public:
    void setup (SIOM::MemoryManager <uint8_t> *memory_manager,
		SIOM::SIOMCoreConfig *core_conf,
		const DalObject *appConfiguration,
		const DalObject *objConfiguration, bool debug = true);

    void unsetup ();

    virtual ISInfo *getISInfo () {return 0;};
    virtual void clearInfo () {};

    // Statistics

    virtual uint64_t packets () = 0;
    virtual double mbytes () = 0;

   private:
    virtual void plugin_setup (SIOM::MemoryManager <uint8_t> *memory_manager,
			       SIOM::SIOMCoreConfig *core_conf,
			       const DalObject *appConfiguration,
			       const DalObject *objConfiguration, bool debug) = 0;

    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug) = 0;

    virtual void plugin_unsetup () = 0;
    virtual void specific_unsetup () = 0;
  };
}

#endif
