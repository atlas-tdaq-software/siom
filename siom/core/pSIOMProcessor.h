#ifndef _SIOMProcessor_h_
#define _SIOMProcessor_h_

#include <vector>
#include <map>
#include <string>

#include "is/infodictionary.h"
#include "config/Configuration.h"
#include "config/DalObject.h"

#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataProcessor.h"
#include "siom/core/pSIOMPlugin.h"

namespace SIOM
{
  class SIOMProcessor : public SIOM::SIOMPlugin
  {
   public:
    SIOMProcessor ();
    virtual ~SIOMProcessor () noexcept;

    unsigned int nthreads ();
    unsigned int min_buf_elements ();
    unsigned int poll_rate_factor ();

    std::vector <SIOM::MemoryPool <uint8_t> *> &input_memory_pools ();
    std::vector <SIOM::MemoryPool <uint8_t> *> &output_memory_pools ();

    virtual void configure (const daq::rc::TransitionCmd &);
    virtual void connect (const daq::rc::TransitionCmd &);
    virtual void prepareForRun (const daq::rc::TransitionCmd &);
    virtual void stopDC (const daq::rc::TransitionCmd &);
    virtual void disconnect (const daq::rc::TransitionCmd &);
    virtual void unconfigure (const daq::rc::TransitionCmd &);

    virtual void clearInfo ();
    virtual ISInfo *getISInfo ();

    // Statistics

    virtual uint64_t packets ();
    virtual double mbytes ();

   private:
    virtual void plugin_setup (SIOM::MemoryManager <uint8_t> *memory_manager,
			       SIOM::SIOMCoreConfig *core_conf,
			       const DalObject *appConfiguration,
			       const DalObject *objConfiguration, bool debug);
    virtual void plugin_unsetup ();

    void configure ();
    void connect () {};
    void prepareForRun ();
    void stopDC ();
    void disconnect () {};
    void unconfigure ();

    virtual SIOM::DataProcessor *createProcessor () = 0;
    virtual void deleteProcessor (SIOM::DataProcessor *processor);

   private:
    bool m_dbConfig;

    unsigned int m_nthreads;
    unsigned int m_minBufElements;
    unsigned int m_pollRateFactor;

    SIOM::DataProcessor *m_dataProcessor;
    SIOM::MemoryManager <uint8_t> *m_memory_manager;

    std::vector <std::string> m_poolInputNames;
    std::vector <std::string> m_poolOutputNames;
    std::vector <SIOM::MemoryPool <uint8_t> *> m_poolInputVector;
    std::vector <SIOM::MemoryPool <uint8_t> *> m_poolOutputVector;
  };
}

void SIOM::SIOMProcessor::configure (const daq::rc::TransitionCmd &)
{
  this->configure ();
}

void SIOM::SIOMProcessor::connect (const daq::rc::TransitionCmd &)
{
  this->connect ();
}

void SIOM::SIOMProcessor::prepareForRun (const daq::rc::TransitionCmd &)
{
  this->prepareForRun ();
}

void SIOM::SIOMProcessor::stopDC (const daq::rc::TransitionCmd &)
{
  this->stopDC ();
}

void SIOM::SIOMProcessor::disconnect (const daq::rc::TransitionCmd &)
{
  this->disconnect ();
}

void SIOM::SIOMProcessor::unconfigure (const daq::rc::TransitionCmd &)
{
  this->unconfigure ();
}

inline uint64_t SIOM::SIOMProcessor::packets ()
{
  return 0;
}

inline double SIOM::SIOMProcessor::mbytes ()
{
  return 0;
}

inline unsigned int SIOM::SIOMProcessor::nthreads ()
{
  return m_nthreads;
}

inline unsigned int SIOM::SIOMProcessor::min_buf_elements ()
{
  return m_minBufElements;
}

inline unsigned int SIOM::SIOMProcessor::poll_rate_factor ()
{
  return m_pollRateFactor;
}

inline std::vector <SIOM::MemoryPool <uint8_t> *> &SIOM::SIOMProcessor::input_memory_pools ()
{
  return m_poolInputVector;
}

inline std::vector <SIOM::MemoryPool <uint8_t> *> &SIOM::SIOMProcessor::output_memory_pools ()
{
  return m_poolOutputVector;
}

inline void SIOM::SIOMProcessor::deleteProcessor (SIOM::DataProcessor *processor)
{
  delete processor;
}

inline void SIOM::SIOMProcessor::clearInfo ()
{
  if (m_dataProcessor) m_dataProcessor->clearInfo ();
}

inline ISInfo *SIOM::SIOMProcessor::getISInfo ()
{
  return (m_dataProcessor ? m_dataProcessor->getISInfo () : 0);
}

#endif
