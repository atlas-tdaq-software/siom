#ifndef _SIOM_CircDataSender_h_
#define _SIOM_CircDataSender_h_

#include <string>
#include <vector>

// TDAQ include files

#include "is/infodictionary.h"
#include "circ/Circservice.h"

// Package include files

#include "siom_info/siomCircOutput.h"

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

namespace SIOM
{
  class CircDataSender : public SIOM::DataSender
  {
   public:
    CircDataSender (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		    SIOM::DataDestination *dest);

    virtual ~CircDataSender ();

   private:
    virtual bool can_send ();
    virtual int send (SIOM::MemoryBuffer <uint8_t> *b);

    virtual siom_info::siomPlugin *createInfo ();
    virtual void fillSpecificInfo (ISInfo *info);

   private:

    // Circular buffer and server's parameters

    std::string    _bufname;
    int            _bufsize;
    unsigned short _port;

    int _circ;
  };
}

inline SIOM::CircDataSender::~CircDataSender ()
{
  this->join ();
  CircCloseCircConnection (_port, const_cast <char *> (_bufname.c_str ()), _circ);
}

inline bool SIOM::CircDataSender::can_send ()
{
  return _circ < 0 ? false : true;
}

inline siom_info::siomPlugin *SIOM::CircDataSender::createInfo ()
{
  auto info = new siom_info::siomCircOutput ();
  return info;
}

inline void SIOM::CircDataSender::fillSpecificInfo (ISInfo *info)
{
}

#endif
