#ifndef _SIOM_CircServer_h_
#define _SIOM_CircServer_h_

// Standard include files

#include <set>
#include <vector>

#include <sys/time.h>

// TDAQ include files

#include "is/infodictionary.h"
#include "circ/Circservice.h"

// Package include files

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMInputServer.h"

#include "siom/plugin/SIOMCircServerThread.h"

#include "siom_info/siomCircInput.h"

namespace SIOM
{
  class CircServer : public SIOM::InputServer
  {
   public:
    CircServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		unsigned short maxthreads,
		unsigned short port,
		unsigned int pollfactor,
		bool pack_events = false);

    virtual ~CircServer ();

    virtual ISInfo *getISInfo ();
    virtual void clearInfo ();

    virtual void startOfRun ();
    virtual void endOfRun ();

   protected:
    virtual void run ();

   public:
    // Access to variables

    unsigned short port () {return _port;};

    // Statistics

    virtual uint64_t packets ();
    virtual double mbytes ();

   private:
    // circ threads

    unsigned short _nthreads;
    std::set <SIOM::CircServerThread *> _threads;

    // class parameters

    unsigned short _port;
    unsigned int   _pollRateFactor;
    bool           _pack;

    unsigned int   _active_pools;

    // IS Info

    struct timeval _t0;

    siom_info::siomCircInput _info;

    // Locks to protect open/close operations and local cid lists
 
    std::set <std::mutex *> _global_mutex;
  };
}

inline void SIOM::CircServer::startOfRun ()
{
  for (auto it = _threads.begin (); it != _threads.end (); ++it)
    (*it)->set_poll_factor (_pollRateFactor);
}

inline void SIOM::CircServer::endOfRun ()
{
  for (auto it = _threads.begin (); it != _threads.end (); ++it)
    (*it)->set_poll_factor (0);
}

inline uint64_t SIOM::CircServer::packets ()
{
  return _info.ProcessedPackets;
}

inline double SIOM::CircServer::mbytes ()
{
  return _info.ProcessedData;
}

#endif // _SIOM_CircServer_h
