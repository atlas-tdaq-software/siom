#ifndef _SIOM_CircServerAddress_h_
#define _SIOM_CircServerAddress_h_

#include <string>

namespace SIOM
{
  class CircServerAddress
  {
   public:
    CircServerAddress (std::string   &bufname,
		       int            bufsize,
		       unsigned short server_port)
      : m_bufname (bufname), m_bufsize (bufsize), m_port (server_port)
    {};

    ~CircServerAddress () {};

    // Data access

    std::string bufname ();
    int bufsize ();
    unsigned short port ();

   private:
    std::string m_bufname;
    int m_bufsize;
    unsigned short m_port;
  };
}

inline std::string SIOM::CircServerAddress::bufname ()
{
  return m_bufname;
}

inline int SIOM::CircServerAddress::bufsize ()
{
  return m_bufsize;
}

inline unsigned short SIOM::CircServerAddress::port ()
{
  return m_port;
}

#endif
