#ifndef _SIOM_CircServerThread_h_
#define _SIOM_CircServerThread_h_

#include <list>
#include <mutex>

#include "circ/Circ.h"

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMActiveObject.h"

#define DELTA       5
#define MBYTE       (1024 * 1024)
#define MINSLEEP    10000000
#define SECTONSEC   1000000000

static const double s_circthr_initial_invrate = 2 * ((double) MINSLEEP / SECTONSEC);

namespace SIOM
{
  class CircServerThread : public SIOM::ActiveObject
  {
   public:
    CircServerThread (SIOM::MemoryPool <uint8_t> *memPool,
		      std::mutex *mutex,
		      unsigned int pollfactor = 0,
		      bool pack = false);

    virtual ~CircServerThread ();

   protected:
    virtual void run ();

   public:
    void add    (int circ);
    void remove (int circ);

    std::list <int> &active_list ();

    /* Access to internal statistics */

    unsigned int    ncirc ()             {return _ncirc;};
    uint64_t        packets ()           {return _npack;};
    uint64_t        thrown ()            {return _nthrown;};
    double          Mbytes ()            {return _Mbytes;};

    void            reset_counters ();

    /* Auxiliary methods for parameter modification */

    void set_poll_factor (unsigned int f) {_pollRateFactor = f;};

   private:
    SIOM::MemoryPool <uint8_t> *_pool;
    std::mutex *_global_mutex;
    std::mutex _internal_mutex;
    unsigned int _pollRateFactor;
    bool _pack;

    std::list <int> _circlist;

    /* Internal statistics */

    unsigned int        _ncirc;
    uint64_t            _npack;
    uint64_t            _ndelta;
    uint64_t            _nthrown;
    double              _Mbytes;
  };
}

inline void SIOM::CircServerThread::reset_counters ()
{
  _npack = 0;
  _ndelta = 0;
  _nthrown = 0;
  _Mbytes = 0;
}

inline std::list <int> &SIOM::CircServerThread::active_list ()
{
  return _circlist;
}

#endif
