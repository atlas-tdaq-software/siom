#ifndef _SIOM_DummyDataProcessor_h_
#define _SIOM_DummyDataProcessor_h_

#include <vector>

// Package include files

#include "siom_info/siomDummyProcessor.h"

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataProcessor.h"

namespace SIOM
{
  class DummyDataProcessor : public SIOM::DataProcessor
  {
   public:
    DummyDataProcessor (std::vector <SIOM::MemoryPool <uint8_t> *> &inputPoolVector,
			std::vector <SIOM::MemoryPool <uint8_t> *> &outputPoolVector,
			SIOM::AlgoParams *p)
      : SIOM::DataProcessor (inputPoolVector, outputPoolVector, p)
    {
      this->start ();
    };

    virtual ~DummyDataProcessor ()
    {
      this->join ();
    };

   private:
    virtual unsigned int process_data (SIOM::MemoryBuffer <uint8_t> *in,
                                       SIOM::MemoryBuffer <uint8_t> *out);

    virtual siom_info::siomPlugin *createInfo ();
    virtual void fillSpecificInfo (ISInfo *info);

   private:
  };
}

inline siom_info::siomPlugin *SIOM::DummyDataProcessor::createInfo ()
{
  auto info = new siom_info::siomDummyProcessor ();
  return info;
}

inline void SIOM::DummyDataProcessor::fillSpecificInfo (ISInfo *info)
{
}

#endif
