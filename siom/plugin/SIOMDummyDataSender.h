#ifndef _SIOM_DummyDataSender_h_
#define _SIOM_DummyDataSender_h_

#include <map>
#include <vector>

// Package include files

#include "siom_info/siomDummyOutput.h"

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

namespace SIOM
{
  class DummyDataSender : public SIOM::DataSender
  {
   public:
    DummyDataSender (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		     SIOM::DataDestination *dest)
      : SIOM::DataSender (mpool, dest)
    {
      this->start ();
    };

    virtual ~DummyDataSender ()
    {
      this->join ();
    };

   private:
    virtual bool can_send ();
    virtual int send (SIOM::MemoryBuffer <uint8_t> *b);

    virtual siom_info::siomPlugin *createInfo ();
    virtual void fillSpecificInfo (ISInfo *info);

   private:
  };
}

inline bool SIOM::DummyDataSender::can_send ()
{
  return true;
}

inline siom_info::siomPlugin *SIOM::DummyDataSender::createInfo ()
{
  auto info = new siom_info::siomDummyOutput ();
  return info;
}

inline void SIOM::DummyDataSender::fillSpecificInfo (ISInfo *info)
{
}

#endif
