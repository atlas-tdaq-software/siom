#ifndef _SIOM_DummyServer_h_
#define _SIOM_DummyServer_h_

// Standard include files

#include <set>
#include <vector>

#include <sys/time.h>

// TDAQ include files

#include "is/infodictionary.h"

// Package include files

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMInputServer.h"
#include "siom/core/SIOMActiveObject.h"

#include "siom/plugin/SIOMDummyServerThread.h"

#include "siom_info/siomDummyInput.h"

namespace SIOM
{
  class DummyServer : public SIOM::InputServer
  {
   public:
    DummyServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		 unsigned short maxthreads);

    virtual ~DummyServer ();

    virtual ISInfo *getISInfo ();
    virtual void clearInfo ();

    virtual void startOfRun ();
    virtual void endOfRun ();

   protected:
    virtual void run ();

   public:
    // Statistics

    virtual uint64_t packets ();
    virtual double mbytes ();

   private:
    void startThreads ();

   private:
    std::set <SIOM::DummyServerThread *> _threads;

    unsigned int _active_pools;

    // IS Info

    struct timeval _t0;

    siom_info::siomDummyInput _info;
  };
}

inline void SIOM::DummyServer::startOfRun ()
{
  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    (*i)->startTrigger ();
}

inline void SIOM::DummyServer::endOfRun ()
{
  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    (*i)->stopTrigger ();
}

inline uint64_t SIOM::DummyServer::packets ()
{
  return _info.ProcessedPackets;
}

inline double SIOM::DummyServer::mbytes ()
{
  return _info.ProcessedData;
}

#endif // _SIOM_DummyServer_h
