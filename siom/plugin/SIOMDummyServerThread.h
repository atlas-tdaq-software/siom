#ifndef _SIOM_DummyServerThread_h_
#define _SIOM_DummyServerThread_h_

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMActiveObject.h"

#define MBYTE   (1024 * 1024)

namespace SIOM
{
  class DummyServerThread : public SIOM::ActiveObject
  {
   public:
    explicit DummyServerThread (SIOM::MemoryPool <uint8_t> *memPool)
      : _pool (memPool), _running (false), _npack (0), _Mbytes (0)
    {
      this->start ();
    };

    virtual ~DummyServerThread ()
    {
      this->join ();
    };

   protected:
    virtual void run ();

   public:
    void startTrigger () {_running = true;};
    void stopTrigger  () {_running = false;};

    /* Access to internal statistics */

    uint64_t   packets ()           {return _npack;};
    double     Mbytes ()            {return _Mbytes;};

    void       reset_counters ()    {_npack = 0; _Mbytes = 0;};

   private:
    SIOM::MemoryPool <uint8_t> *_pool;
    bool _running;

    /* Internal statistics */

    uint64_t _npack;
    double   _Mbytes;
  };
}

#endif
