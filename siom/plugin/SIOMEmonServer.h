#ifndef _SIOM_EmonServer_h_
#define _SIOM_EmonServer_h_

// Standard include files

#include <set>
#include <vector>
#include <string>

#include <sys/time.h>

// TDAQ include files

#include "is/infodictionary.h"
#include "ipc/partition.h"
#include "emon/SelectionCriteria.h"

// Package include files

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMInputServer.h"
#include "siom/core/SIOMActiveObject.h"

#include "siom/plugin/SIOMEmonServerThread.h"

#include "siom_info/siomEmonInput.h"

namespace SIOM
{
  class EmonServer : public SIOM::InputServer
  {
   public:
    EmonServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		unsigned short maxthreads,
		std::string &part_name,
		const emon::dal::SamplingParameters *sampling_parameters);

    virtual ~EmonServer ();

    virtual ISInfo *getISInfo ();
    virtual void clearInfo ();

    virtual void startOfRun ();
    virtual void endOfRun ();

   protected:
    virtual void run ();

   public:
    // Statistics

    virtual uint64_t packets ();
    virtual double mbytes ();

   private:
    void startThreads ();

   private:
    const std::map <const std::string, emon::Logic> bits_logic = {{"IGNORE", emon::logic::IGNORE},
								  {"AND", emon::logic::AND},
								  {"OR", emon::logic::OR}};

    const std::map <const std::string, emon::Origin> bits_origin = {{"BEFORE_PRESCALE", emon::origin::BEFORE_PRESCALE},
								    {"AFTER_PRESCALE", emon::origin::AFTER_PRESCALE},
								    {"AFTER_VETO", emon::origin::AFTER_VETO}};

    std::string _partition_name;
    const emon::dal::SamplingParameters *_sampling_parameters;

    emon::SelectionCriteria *_selection_criteria;
    std::set <SIOM::EmonServerThread *> _threads;
    unsigned int _active_pools;

    // IS Info

    struct timeval _t0;

    siom_info::siomEmonInput _info;
  };
}

inline void SIOM::EmonServer::startOfRun ()
{
  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    (*i)->startTrigger ();
}

inline void SIOM::EmonServer::endOfRun ()
{
  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    (*i)->stopTrigger ();
}

inline uint64_t SIOM::EmonServer::packets ()
{
  return _info.ProcessedPackets;
}

inline double SIOM::EmonServer::mbytes ()
{
  return _info.ProcessedData;
}

#endif // _SIOM_EmonServer_h
