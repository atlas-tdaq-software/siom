#ifndef _SIOM_EmonServerThread_h_
#define _SIOM_EmonServerThread_h_

#include <vector>
#include <string>
#include <mutex>

#include "ipc/partition.h"
#include "emon/EventIterator.h"
#include "emon/SelectionCriteria.h"

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMActiveObject.h"

#define MBYTE   (1024 * 1024)

namespace SIOM
{
  class EmonServerThread : public SIOM::ActiveObject
  {
   public:
    EmonServerThread (SIOM::MemoryPool <uint8_t> *memPool,
		      const std::string &part_name,
		      const std::string &sampler_type,
		      const std::vector <std::string> &sampler_names,
		      emon::SelectionCriteria *criteria,
		      const unsigned int buffer_limit)
      : _pool (memPool), _partition_name (part_name), _sampler_type (sampler_type), _sampler_names (sampler_names),
        _selection_criteria (criteria), _buffer_limit (buffer_limit),
        _running (false), _npack (0), _Mbytes (0)
    {
      this->start ();
    };

    virtual ~EmonServerThread ()
    {
      this->join ();
    };

   protected:
    virtual void run ();

   public:
    void startTrigger ();
    void stopTrigger  ();

    /* Access to internal statistics */

    uint64_t   packets ()           {return _npack;};
    double     Mbytes ()            {return _Mbytes;};

    void       reset_counters ()    {_npack = 0; _Mbytes = 0;};

   private:
    SIOM::MemoryPool <uint8_t> *_pool;
    std::string _partition_name;
    std::string _sampler_type;
    std::vector <std::string> _sampler_names;
    emon::SelectionCriteria *_selection_criteria;
    unsigned int _buffer_limit;

    bool _running;
    std::mutex _internal_mutex;

    emon::EventIterator *_event_iterator;

    /* Internal statistics */

    uint64_t _npack;
    double   _Mbytes;
  };
}

#endif
