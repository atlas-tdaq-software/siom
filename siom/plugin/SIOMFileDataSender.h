#ifndef _SIOM_FileDataSender_h_
#define _SIOM_FileDataSender_h_

#include <unistd.h>
#include <errno.h>

#include <vector>
#include <string>
#include <mutex>

// TDAQ include files

#include "is/infodictionary.h"

// Package include files

#include "siom_info/siomFileOutput.h"

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMFileOutParameters.h"

namespace SIOM
{
  class FileDataSender : public SIOM::DataSender
  {
   public:
    FileDataSender (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		    SIOM::DataDestination *dest);

    virtual ~FileDataSender ();

   private:
    virtual bool can_send ();
    virtual int send (SIOM::MemoryBuffer <uint8_t> *b);

    virtual void startOfRun ();
    virtual void endOfRun ();

    virtual siom_info::siomPlugin *createInfo ();
    virtual void fillSpecificInfo (ISInfo *info);

   private:
    std::string build_filename (std::string &file_location,
				std::string &file_base_name,
				unsigned int run_number,
				unsigned int file_index);

    std::string publish_file (std::string &filename);

   private:
    SIOM::FileOutParameters *_outpar;

    bool _firstInDataSet;
    bool _writeData;

    unsigned int _index;
    unsigned int _RunNumber;
    unsigned int _written;
    unsigned int _event_number;

    std::string _filename;
    std::string _local_filename;
    std::string _dataset_name;

    int _descriptor;

    std::mutex _fileMutex;
  };
}

inline bool SIOM::FileDataSender::can_send ()
{
  return (true);
}

inline siom_info::siomPlugin *SIOM::FileDataSender::createInfo ()
{
  siom_info::siomFileOutput *info = new siom_info::siomFileOutput ();
  return info;
}

inline void SIOM::FileDataSender::fillSpecificInfo (ISInfo *info)
{
}

inline std::string SIOM::FileDataSender::build_filename (std::string &file_location,
							 std::string &file_base_name,
							 unsigned int run_number,
							 unsigned int file_index)
{
  char srun[16];
  char sindex[16];

  sprintf (srun, "%.8d", run_number);
  sprintf (sindex, "%.4d", file_index);

  std::string filename = file_location + "/" + file_base_name + "." + srun + "._" + sindex + ".writing";

  return filename;
}

inline std::string SIOM::FileDataSender::publish_file (std::string &filename)
{
  std::string newname = filename;
  newname.erase (newname.rfind (".writing", newname.length ()),
		 sizeof (".writing"));
  newname.append (".data");
  rename (filename.c_str (), newname.c_str ());

  filename = newname;

  return filename;
}

#endif
