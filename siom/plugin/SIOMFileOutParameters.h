#ifndef _SIOM_FileOutParameters_h_
#define _SIOM_FileOutParameters_h_

#include <string>
#include "ipc/partition.h"

#include "siom/core/SIOMCoreConfig.h"

namespace SIOM
{
  class FileOutParameters
  {
   public:
    FileOutParameters (std::string &activation_flag,
		       IPCPartition *ipc_partition,
		       std::string &file_location,
		       std::string &file_base_name,
		       unsigned int maxsize,
		       SIOM::SIOMCoreConfig *core_conf)
      : m_activationflag (activation_flag),
        m_ipcpartition (ipc_partition),
        m_filelocation (file_location), m_filebasename (file_base_name),
        m_maxsize (maxsize), m_core_conf (core_conf)
      {};

    ~FileOutParameters () {};

    // Data access

    std::string &activation_flag ();
    IPCPartition *ipc_partition ();
    std::string &file_location ();
    std::string &file_base_name ();
    unsigned int maxsize ();
    SIOM::SIOMCoreConfig *core_conf ();

   private:
    std::string m_activationflag;
    IPCPartition *m_ipcpartition;
    std::string m_filelocation;
    std::string m_filebasename;
    unsigned int m_maxsize;
    SIOM::SIOMCoreConfig *m_core_conf;
  };
}

inline std::string &SIOM::FileOutParameters::activation_flag ()
{
  return m_activationflag;
}

inline IPCPartition *SIOM::FileOutParameters::ipc_partition ()
{
  return m_ipcpartition;
}

inline std::string &SIOM::FileOutParameters::file_location ()
{
  return m_filelocation;
}

inline std::string &SIOM::FileOutParameters::file_base_name ()
{
  return m_filebasename;
}

inline unsigned int SIOM::FileOutParameters::maxsize ()
{
  return m_maxsize;
}

inline SIOM::SIOMCoreConfig *SIOM::FileOutParameters::core_conf ()
{
  return m_core_conf;
}

#endif
