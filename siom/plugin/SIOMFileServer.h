#ifndef _SIOM_FileServer_h_
#define _SIOM_FileServer_h_

// Standard include files

#include <set>
#include <vector>

#include <sys/time.h>

// TDAQ include files

#include "is/infodictionary.h"

// Package include files

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMInputServer.h"

#include "siom/plugin/SIOMFileServerThread.h"

#include "siom_info/siomFileInput.h"

namespace SIOM
{
  class FileServer : public SIOM::InputServer
  {
   public:
    FileServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		unsigned short maxthreads,
		std::vector <std::string> &input_files,
	        bool pack_events);

    virtual ~FileServer ();

    virtual ISInfo *getISInfo ();
    virtual void clearInfo ();

    virtual void startOfRun ();
    virtual void endOfRun  ();

   protected:
    virtual void run ();

   public:
    // Statistics

    virtual uint64_t packets ();
    virtual double mbytes ();

   private:
    void startThreads ();

   private:
    // class parameters

    std::vector <std::string> _files;
    bool _pack;

    unsigned int _active_pools;

    // IS Info

    struct timeval _t0;

    siom_info::siomFileInput _info;

    // server threads and file descriptors

    std::vector <int>  _fd;

    unsigned short     _nthreads;
    std::set <SIOM::FileServerThread *> _threads;
  };
}

inline void SIOM::FileServer::startOfRun ()
{
  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    (*i)->startTrigger ();
}

inline void SIOM::FileServer::endOfRun ()
{
  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    (*i)->stopTrigger ();
}

inline uint64_t SIOM::FileServer::packets ()
{
  return _info.ProcessedPackets;
}

inline double SIOM::FileServer::mbytes ()
{
  return _info.ProcessedData;
}

#endif // _SIOM_FileServer_h
