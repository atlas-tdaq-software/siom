#ifndef _SIOM_FileServerThread_h_
#define _SIOM_FileServerThread_h_

#include <list>
#include <mutex>

#include <sys/select.h>

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMActiveObject.h"

#define MBYTE             (1024 * 1024)

namespace SIOM
{
  class FileServerThread : public SIOM::ActiveObject
  {
   public:
    FileServerThread (SIOM::MemoryPool <uint8_t> *memPool, bool pack = false);
    virtual ~FileServerThread () {this->join ();};

   protected:
    virtual void run ();

   public:
    void add    (int fd);
    void remove (int fd);

    void startTrigger ();
    void stopTrigger  ();

    // Access to internal statistics

    unsigned int    nfile ()             {return _nfile;};
    uint64_t        packets ()           {return _npack;};
    uint64_t        thrown ()            {return _nthrown;};
    double          Mbytes ()            {return _Mbytes;};

    void            reset_counters ();

   private:
    SIOM::MemoryPool <uint8_t> *_pool;
    bool _pack;

    std::list <int> _fdlist;
    std::mutex _fdlist_mutex;

    bool _running;

    // Internal statistics

    unsigned int     _nfile;
    uint64_t         _npack;
    uint64_t         _nthrown;
    double           _Mbytes;
  };
}

inline void SIOM::FileServerThread::reset_counters ()
{
  _npack = 0;
  _nthrown = 0;
  _Mbytes = 0;
}

inline void SIOM::FileServerThread::startTrigger ()
{
  _running = true;
}

inline void SIOM::FileServerThread::stopTrigger ()
{
  _running = false;
}

#endif
