#ifndef SIOMPLUGINEXCEPTION_H
#define SIOMPLUGINEXCEPTION_H

#include "siom/core/SIOMException.h"

namespace SIOM
{
  class SIOMPluginException : public SIOMException
  {
   public:
    enum ErrorCode { SERVER_INITIALIZATION_ERROR,
		     NO_BUFFER_CONNECTION,
		     CONNECTION_BROKEN,
		     UNRECOGNIZED_ERROR
    };

    SIOMPluginException (ErrorCode error);
    SIOMPluginException (ErrorCode error, std::string description);
    SIOMPluginException (ErrorCode error, const ers::Context &context);
    SIOMPluginException (ErrorCode error, std::string description,
			 const ers::Context &context);
    SIOMPluginException (const std::exception &cause, ErrorCode error,
			 std::string description, const ers::Context &context);

    virtual ~SIOMPluginException() throw () {};

   protected:
    virtual std::string getErrorString (unsigned int errorId) const;
  };

  inline SIOMPluginException::SIOMPluginException (SIOMPluginException::ErrorCode err) 
    : SIOMException ("SIOMPlugin", err, getErrorString (err)) {}
   
  inline SIOMPluginException::SIOMPluginException (SIOMPluginException::ErrorCode err,
						   std::string description) 
    : SIOMException ("SIOMPlugin", err, getErrorString (err), description) {}

  inline SIOMPluginException::SIOMPluginException (SIOMPluginException::ErrorCode err,
						   const ers::Context &context)
    : SIOMException ("SIOMPlugin", err, getErrorString (err), context) {}
   
  inline SIOMPluginException::SIOMPluginException (SIOMPluginException::ErrorCode err,
						   std::string description,
						   const ers::Context &context) 
    : SIOMException ("SIOMPlugin", err, getErrorString (err),
		     description, context) {}

  inline SIOMPluginException::SIOMPluginException (const std::exception &cause,
						   SIOMPluginException::ErrorCode err,
						   std::string description,
						   const ers::Context &context) 
    : SIOMException (cause, "SIOMPlugin", err, getErrorString (err),
		     description, context) {}

  inline std::string SIOMPluginException::getErrorString (unsigned int errorId) const
  {
    std::string rc;
    
    switch (errorId)
    {
      case SERVER_INITIALIZATION_ERROR:
	rc = "Error while starting server";
	break;
      case NO_BUFFER_CONNECTION:
	rc = "Error in connecting to input buffers";
	break;
      case CONNECTION_BROKEN:
	rc = "A network connection has broken";
	break;
      default:
	rc = "";
      break;
    }

    return rc;
  }
}

#endif //SIOMPLUGINEXCEPTION_H
