#ifndef _SIOM_SamplingDataProcessor_h_
#define _SIOM_SamplingDataProcessor_h_

#include <vector>

// Package include files

#include "siom_info/siomSamplingProcessor.h"

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMSamplerFactory.h"
#include "siom/core/SIOMAlgoParams.h"
#include "siom/core/SIOMDataProcessor.h"

#include "siom/plugin/SIOMSamplingParameters.h"

namespace SIOM
{
  class SamplingDataProcessor : public SIOM::DataProcessor
  {
   public:
    SamplingDataProcessor (std::vector <SIOM::MemoryPool <uint8_t> *> &inputPoolVector,
			   std::vector <SIOM::MemoryPool <uint8_t> *> &outputPoolVector,
			   SIOM::AlgoParams *params);

    virtual ~SamplingDataProcessor ();

   private:
    virtual unsigned int process_data (SIOM::MemoryBuffer <uint8_t> *in,
                                       SIOM::MemoryBuffer <uint8_t> *out);

    virtual siom_info::siomPlugin *createInfo ();
    virtual void fillSpecificInfo (ISInfo *info);

   private:
    emon::EventSampler       *_event_sampler;
    SIOM::SIOMSamplerFactory *_sampler_factory;
  };
}

inline siom_info::siomPlugin *SIOM::SamplingDataProcessor::createInfo ()
{
  auto info = new siom_info::siomSamplingProcessor ();
  return info;
}

inline void SIOM::SamplingDataProcessor::fillSpecificInfo (ISInfo *info)
{
}

#endif
