#ifndef _SIOM_SamplingParameters_h_
#define _SIOM_SamplingParameters_h_

#include <string>

namespace SIOM
{
  class SamplingParameters
  {
   public:
    SamplingParameters (std::string  &part_name,
        	        std::string  &sampler_type,
			std::string  &sampler_name,
			uint32_t     packet_header,
			unsigned int max_channels,
			unsigned int inverse_sampling_rate)
    : _partition_name (part_name), _type (sampler_type), _name (sampler_name),
      _header (packet_header), _max_channels (max_channels), _max_rate (inverse_sampling_rate)
    {};

    virtual ~SamplingParameters () {};

    // Data access

    std::string   partition_name ()          {return _partition_name;};
    std::string   type ()                    {return _type;};
    std::string   name ()                    {return _name;};
    uint32_t      header ()                  {return _header;};
    unsigned int  max_channels ()            {return _max_channels;};
    unsigned int  inverse_sampling_rate ()   {return _max_rate;};

   private:
    std::string   _partition_name;
    std::string   _type;
    std::string   _name;
    uint32_t      _header;
    unsigned int  _max_channels;
    unsigned int  _max_rate;
  };
}

#endif
