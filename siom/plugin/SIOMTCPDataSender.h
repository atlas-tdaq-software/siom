#ifndef _SIOM_TCPDataSender_h_
#define _SIOM_TCPDataSender_h_

#include <vector>
#include <mutex>

// TDAQ include files

#include "is/infodictionary.h"
#include "transport/TCP.h"

// Package include files

#include "siom_info/siomTCPOutput.h"

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

namespace SIOM
{
  class TCPDataSender : public SIOM::DataSender
  {
   public:
    TCPDataSender (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		   SIOM::DataDestination *dest);

    virtual ~TCPDataSender ();

   private:
    virtual bool can_send ();
    virtual int send (SIOM::MemoryBuffer <uint8_t> *b);

    virtual siom_info::siomPlugin *createInfo ();
    virtual void fillSpecificInfo (ISInfo *info);

   private:
    TCP *_client = 0;
  };
}

inline SIOM::TCPDataSender::~TCPDataSender ()
{
  this->join ();

  // close connections

  if (_client)
    delete _client;
}

inline bool SIOM::TCPDataSender::can_send ()
{
  if (_client)
    return (_client->can_send ());
  else
    return false;
}

inline siom_info::siomPlugin *SIOM::TCPDataSender::createInfo ()
{
  auto info = new siom_info::siomTCPOutput ();
  return info;
}

inline void SIOM::TCPDataSender::fillSpecificInfo (ISInfo *info)
{
}

#endif
