#ifndef _SIOM_TCPServer_h_
#define _SIOM_TCPServer_h_

// Standard include files

#include <vector>
#include <set>

#include <sys/time.h>

// TDAQ include files

#include "is/infodictionary.h"
#include "transport/Transport.h"
#include "transport/TCP.h"

// Package include files

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMInputServer.h"

#include "siom/plugin/SIOMTCPServerThread.h"

#include "siom_info/siomTCPInput.h"

namespace SIOM
{
  class TCPServer : public SIOM::InputServer
  {
   public:
    TCPServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
	       unsigned short maxthreads,
	       unsigned short port,
	       bool pack_events = false);

    virtual ~TCPServer ();

    virtual ISInfo *getISInfo ();
    virtual void clearInfo ();

   protected:
    virtual void run ();

   public:
    // Access to variables

    unsigned short port () {return _port;};

    // Statistics

    virtual uint64_t packets ();
    virtual double mbytes ();

   private:
    // threads

    unsigned short _nthreads;
    std::set <SIOM::TCPServerThread *> _threads;

    // class parameters

    unsigned short _port;
    bool           _pack;

    unsigned int _active_pools;

    // IS Info

    struct timeval _t0;

    siom_info::siomTCPInput _info;

    // server transport class and threads

    TCP *_server;
  };
}

inline uint64_t SIOM::TCPServer::packets ()
{
  return _info.ProcessedPackets;
}

inline double SIOM::TCPServer::mbytes ()
{
  return _info.ProcessedData;
}

#endif // _SIOM_TCPServer_h
