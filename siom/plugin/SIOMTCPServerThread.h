#ifndef _SIOM_TCPServerThread_h_
#define _SIOM_TCPServerThread_h_

#include <map>
#include <mutex>

#include <errno.h>
#include <sys/select.h>

#include "transport/Transport.h"
#include "transport/TCP.h"

#include "siom/core/SIOMActiveObject.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMMemoryBuffer.h"

#define MBYTE             (1024 * 1024)

namespace SIOM
{
  class TCPServerThread : public SIOM::ActiveObject
  {
   public:
    TCPServerThread (SIOM::MemoryPool <uint8_t> *memPool, bool pack = false);
    virtual ~TCPServerThread ();

   protected:
    virtual void run ();

   public:
    void add    (TCP *connection);
    void remove (TCP *connection);

    void       reset_counters ();

    /* Access to internal statistics */

    uint64_t   ntrans ()            {return _ntrans;};
    uint64_t   packets ()           {return _npack;};
    uint64_t   thrown ()            {return _nthrown;};
    double     Mbytes ()            {return _Mbytes;};

   private:
    void remove (int socket);
    int tcp_safe_receive (int socket, uint8_t *buffer, int length);

   private:
    SIOM::MemoryPool <uint8_t> *_pool;
    bool _pack;

    int    _nfds;
    fd_set _select;

    std::map <int, TCP *> _tcp_map;
    std::mutex _fdset_mutex;

    /* Internal statistics */

    uint64_t     _ntrans;
    uint64_t     _npack;
    uint64_t     _nthrown;
    double       _Mbytes;
  };
}

inline void SIOM::TCPServerThread::reset_counters ()
{
  _npack = 0;
  _nthrown = 0;
  _Mbytes = 0;
}

inline void SIOM::TCPServerThread::remove (int socket)
{
  this->remove (_tcp_map[socket]);
}

inline int SIOM::TCPServerThread::tcp_safe_receive (int socket, uint8_t *buffer, int length)
{
  int nrec, received = 0;

  do
  {
    nrec = ::recv (socket, buffer + received,
		   length - received, 0);

    if ((nrec == -1 && errno != EINTR && errno != EAGAIN) || nrec == 0)
      return nrec;
    else if (nrec > 0)
      received += nrec;

  } while (received < length);

  return received;
}

#endif
