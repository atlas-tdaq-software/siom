#ifndef _SIOM_UDPDataSender_h_
#define _SIOM_UDPDataSender_h_

#include <vector>

// TDAQ include files

#include "is/infodictionary.h"
#include "transport/UDP.h"

// Package include files

#include "siom_info/siomUDPOutput.h"

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

namespace SIOM
{
  class UDPDataSender : public SIOM::DataSender
  {
   public:
    UDPDataSender (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
		   SIOM::DataDestination *dest);

    virtual ~UDPDataSender ();

   private:
    virtual bool can_send ();
    virtual int send (SIOM::MemoryBuffer <uint8_t> *b);

    virtual siom_info::siomPlugin *createInfo ();
    virtual void fillSpecificInfo (ISInfo *info);

   private:
    UDP *_client = 0;
  };
}

inline SIOM::UDPDataSender::~UDPDataSender ()
{
  this->join ();

  // Close connections

  if (_client)
    delete _client;
}

inline bool SIOM::UDPDataSender::can_send ()
{
  if (_client)
    return (_client->can_send ());
  else
    return false;
}

inline siom_info::siomPlugin *SIOM::UDPDataSender::createInfo ()
{
  auto info = new siom_info::siomUDPOutput ();
  return info;
}

inline void SIOM::UDPDataSender::fillSpecificInfo (ISInfo *info)
{
}

#endif
