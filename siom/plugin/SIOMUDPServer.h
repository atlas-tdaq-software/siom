#ifndef _SIOM_UDPServer_h_
#define _SIOM_UDPServer_h_

// Standard include files

#include <vector>
#include <set>

#include <sys/time.h>

// TDAQ include files

#include "is/infodictionary.h"
#include "transport/Transport.h"
#include "transport/UDP.h"

// Package include files

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMInputServer.h"

#include "siom/plugin/SIOMUDPServerThread.h"

#include "siom_info/siomUDPInput.h"

namespace SIOM
{
  class UDPServer : public SIOM::InputServer
  {
   public:
    UDPServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
	       unsigned short maxthreads,
	       unsigned short port,
	       bool pack_events = false);

    virtual ~UDPServer ();

    virtual ISInfo *getISInfo ();
    virtual void clearInfo ();

   protected:
    virtual void run ();

   public:
    // Access to variables

    unsigned short port ()         {return _port;};

    // Statistics

    virtual uint64_t packets ();
    virtual uint64_t thrown ();
    virtual uint64_t errors ();
    virtual double mbytes ();

   private:
    // threads

    unsigned short _nthreads;
    std::set <SIOM::UDPServerThread *> _threads;

    // class parameters

    unsigned short _port;
    bool           _pack;

    unsigned int _active_pools;

    // IS Info

    struct timeval _t0;

    siom_info::siomUDPInput _info;
  };
}

inline uint64_t SIOM::UDPServer::packets ()
{
  return _info.ProcessedPackets;
}

inline uint64_t SIOM::UDPServer::thrown ()
{
  return _info.PacketsThrown;
}

inline uint64_t SIOM::UDPServer::errors ()
{
  return _info.SocketErrors;
}

inline double SIOM::UDPServer::mbytes ()
{
  return _info.ProcessedData;
}

#endif // _SIOM_UDPServer_h
