#ifndef _SIOM_UDPServerThread_h_
#define _SIOM_UDPServerThread_h_

#include <map>
#include <list>
#include <mutex>

#include <sys/select.h>

#include "transport/Transport.h"
#include "transport/UDP.h"

#include "siom/core/SIOMActiveObject.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMMemoryBuffer.h"

#define MBYTE             (1024 * 1024)

namespace SIOM
{
  class UDPServerThread : public SIOM::ActiveObject
  {
   public:
    UDPServerThread (SIOM::MemoryPool <uint8_t> *memPool, bool pack = false);
    virtual ~UDPServerThread ();

   protected:
    virtual void run ();

   public:
    void add    (UDP *server);
    void remove (UDP *server);

    void       reset_counters ();

    /* Access to internal statistics */

    uint64_t   ntrans ()            {return _ntrans;};
    uint64_t   packets ()           {return _npack;};
    uint64_t   thrown ()            {return _nthrown;};
    uint64_t   errors ()            {return _socket_errors;};
    double     Mbytes ()            {return _Mbytes;};

    std::map <unsigned int, uint64_t> &source_map () {return _smap;};

   private:
    SIOM::MemoryPool <uint8_t> *_pool;
    bool _pack;

    int     _nfds;
    fd_set  _select;

    std::list <UDP *> _udplist;
    std::mutex _fdset_mutex;

    /* Internal statistics */

    uint64_t     _ntrans;
    uint64_t     _npack;
    uint64_t     _nthrown;
    uint64_t     _socket_errors;
    double       _Mbytes;

    std::map <unsigned int, uint64_t> _smap;
  };
}

inline void SIOM::UDPServerThread::reset_counters ()
{
  _npack = 0;
  _nthrown = 0;
  _Mbytes = 0;
  _socket_errors = 0;
}

#endif
