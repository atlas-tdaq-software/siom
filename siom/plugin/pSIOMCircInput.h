#ifndef _SIOMCircInput_h_
#define _SIOMCircInput_h_

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMInput.h"
#include "siom/core/SIOMInputServer.h"

#include "siom/plugin/SIOMCircServer.h"

namespace SIOM
{
  class SIOMCircInput : public SIOM::SIOMInput
  {
   public:
    SIOMCircInput () {};
    virtual ~SIOMCircInput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration,
				 bool debug);
    virtual void specific_unsetup () {};

    virtual SIOM::InputServer *createServer ();

   private:
    SIOM::CircServer *_circServer;

    unsigned short _port;
    unsigned int   _pollRateFactor;
    bool           _pack;
  };
}

inline SIOM::SIOMCircInput::~SIOMCircInput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::InputServer *SIOM::SIOMCircInput::createServer ()
{
  _circServer = new SIOM::CircServer (this->memory_pools (),
				      this->max_threads (),
				      _port, _pollRateFactor, _pack);

  return _circServer;
}

#endif
