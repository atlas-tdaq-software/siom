#ifndef _SIOMCircOutput_h_
#define _SIOMCircOutput_h_

#include <string>

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMOutput.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMCircDataSender.h"

namespace SIOM
{
  class SIOMCircOutput : public SIOM::SIOMOutput
  {
   public:
    SIOMCircOutput (): _addr (0) {};
    virtual ~SIOMCircOutput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup ();

    virtual SIOM::DataSender *createSender ();

   private:
    SIOM::DataDestination _dest;
    SIOM::CircServerAddress *_addr;
    SIOM::CircDataSender *_circSender;
  };
}

inline SIOM::SIOMCircOutput::~SIOMCircOutput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::DataSender *SIOM::SIOMCircOutput::createSender ()
{
  _circSender = new SIOM::CircDataSender (this->memory_pools (),
					  &_dest);

  return _circSender;
}

#endif
