#ifndef _SIOMDummyInput_h_
#define _SIOMDummyInput_h_

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMInput.h"
#include "siom/core/SIOMInputServer.h"

#include "siom/plugin/SIOMDummyServer.h"

namespace SIOM
{
  class SIOMDummyInput : public SIOM::SIOMInput
  {
   public:
    SIOMDummyInput () {};
    virtual ~SIOMDummyInput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration,
				 bool debug);
    virtual void specific_unsetup () {};

    virtual SIOM::InputServer *createServer ();

   private:
    SIOM::DummyServer *_dummyServer;
  };
}

inline SIOM::SIOMDummyInput::~SIOMDummyInput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::InputServer *SIOM::SIOMDummyInput::createServer ()
{
  _dummyServer = new SIOM::DummyServer (this->memory_pools (),
					this->max_threads ());
  return _dummyServer;
}

#endif
