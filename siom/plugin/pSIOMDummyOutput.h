#ifndef _SIOMDummyOutput_h_
#define _SIOMDummyOutput_h_

#include <map>
#include <vector>
#include <string>

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMOutput.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMDummyDataSender.h"

namespace SIOM
{
  class SIOMDummyOutput : public SIOM::SIOMOutput
  {
   public:
    SIOMDummyOutput () {};
    virtual ~SIOMDummyOutput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup () {};

    virtual SIOM::DataSender *createSender ();

   private:
    SIOM::DataDestination _dest;
    SIOM::DummyDataSender *_dummySender;
  };
}

inline SIOM::SIOMDummyOutput::~SIOMDummyOutput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::DataSender *SIOM::SIOMDummyOutput::createSender ()
{
  _dummySender = new SIOM::DummyDataSender (this->memory_pools (),
					    &_dest);

  return _dummySender;
}

#endif
