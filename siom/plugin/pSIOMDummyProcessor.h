#ifndef _SIOMDummyProcessor_h_
#define _SIOMDummyProcessor_h_

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMProcessor.h"
#include "siom/core/SIOMDataProcessor.h"

#include "siom/plugin/SIOMDummyDataProcessor.h"

namespace SIOM
{
  class SIOMDummyProcessor : public SIOMProcessor
  {
   public:
    SIOMDummyProcessor () {};
    virtual ~SIOMDummyProcessor () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup () {};

    virtual SIOM::DataProcessor *createProcessor ();

   private:
    SIOM::DummyDataProcessor *_dummyProcessor;
  };
}

inline SIOM::SIOMDummyProcessor::~SIOMDummyProcessor () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::DataProcessor *SIOM::SIOMDummyProcessor::createProcessor ()
{
  _dummyProcessor = new SIOM::DummyDataProcessor (this->input_memory_pools (),
						  this->output_memory_pools (),
						  0);
  return _dummyProcessor;
}

#endif
