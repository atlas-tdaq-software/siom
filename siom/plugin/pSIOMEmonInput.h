#ifndef _SIOMEmonInput_h_
#define _SIOMEmonInput_h_

#include <vector>
#include <string>

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

#include "ipc/partition.h"
#include "emon/SelectionCriteria.h"

// Package include files

#include "siom/core/pSIOMInput.h"
#include "siom/plugin/SIOMEmonServer.h"

namespace SIOM
{
  class SIOMEmonInput : public SIOM::SIOMInput
  {
   public:
    SIOMEmonInput () {};
    virtual ~SIOMEmonInput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration,
				 bool debug);
    virtual void specific_unsetup () {};

    virtual SIOM::InputServer *createServer ();

   private:
    SIOM::EmonServer *_emonServer;

    std::string _partition_name;
    const emon::dal::SamplingParameters *_sampling_parameters;
  };
}

inline SIOM::SIOMEmonInput::~SIOMEmonInput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::InputServer *SIOM::SIOMEmonInput::createServer ()
{
  _emonServer = new SIOM::EmonServer (this->memory_pools (),
				      this->max_threads (),
				      _partition_name,
				      _sampling_parameters);

  return _emonServer;
}

#endif
