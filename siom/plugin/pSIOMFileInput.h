#ifndef _SIOMFileInput_h_
#define _SIOMFileInput_h_

#include <vector>
#include <string>

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMInput.h"
#include "siom/core/SIOMInputServer.h"

#include "siom/plugin/SIOMFileServer.h"

namespace SIOM
{
  class SIOMFileInput : public SIOM::SIOMInput
  {
   public:
    SIOMFileInput () {};
    virtual ~SIOMFileInput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup () {};

    virtual SIOM::InputServer *createServer ();

   private:
    SIOM::FileServer *_fileServer;

    std::vector <std::string> _files;
    bool _pack;
  };
}

inline SIOM::SIOMFileInput::~SIOMFileInput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::InputServer *SIOM::SIOMFileInput::createServer ()
{
  _fileServer = new SIOM::FileServer (this->memory_pools (),
				      this->max_threads (),
				      _files, _pack);

  return _fileServer;
}

#endif
