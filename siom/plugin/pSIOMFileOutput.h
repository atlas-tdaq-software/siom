#ifndef _SIOMFileOutput_h_
#define _SIOMFileOutput_h_

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMOutput.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMFileOutParameters.h"
#include "siom/plugin/SIOMFileDataSender.h"

namespace SIOM
{
  class SIOMFileOutput : public SIOM::SIOMOutput
  {
   public:
    SIOMFileOutput () : _addr (0) {};
    virtual ~SIOMFileOutput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup ();

    virtual SIOM::DataSender *createSender ();

   private:
    SIOM::DataDestination _dest;
    SIOM::FileOutParameters *_addr;
    SIOM::FileDataSender *_fileSender;

    const IPCPartition *_partition;
  };
}

inline SIOM::SIOMFileOutput::~SIOMFileOutput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::DataSender *SIOM::SIOMFileOutput::createSender ()
{
  _fileSender = new SIOM::FileDataSender (this->memory_pools (),
					  &_dest);

  return _fileSender;
}

#endif
