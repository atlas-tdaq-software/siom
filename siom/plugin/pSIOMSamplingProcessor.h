#ifndef _SIOMSamplingProcessor_h_
#define _SIOMSamplingProcessor_h_

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMProcessor.h"
#include "siom/core/SIOMDataProcessor.h"
#include "siom/core/SIOMAlgoParams.h"

#include "siom/plugin/SIOMSamplingDataProcessor.h"
#include "siom/plugin/SIOMSamplingParameters.h"

namespace SIOM
{
  class SIOMSamplingProcessor : public SIOMProcessor
  {
   public:
    SIOMSamplingProcessor () : _samplingParameters (0) {};
    virtual ~SIOMSamplingProcessor () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup ();

    virtual SIOM::DataProcessor *createProcessor ();

   private:
    SIOM::AlgoParams             _params;
    SIOM::SamplingParameters    *_samplingParameters;
    SIOM::SamplingDataProcessor *_samplingProcessor;
  };
}

inline SIOM::SIOMSamplingProcessor::~SIOMSamplingProcessor () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::DataProcessor *SIOM::SIOMSamplingProcessor::createProcessor ()
{
  _samplingProcessor = new SIOM::SamplingDataProcessor (this->input_memory_pools (),
							this->output_memory_pools (),
							&_params);
  return _samplingProcessor;
}

#endif
