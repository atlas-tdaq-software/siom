#ifndef _SIOMTCPInput_h_
#define _SIOMTCPInput_h_

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMInput.h"
#include "siom/core/SIOMInputServer.h"

#include "siom/plugin/SIOMTCPServer.h"

namespace SIOM
{
  class SIOMTCPInput : public SIOM::SIOMInput
  {
   public:
    SIOMTCPInput () {};
    virtual ~SIOMTCPInput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup () {};

    virtual SIOM::InputServer *createServer ();

   private:
    SIOM::TCPServer *_tcpServer;

    unsigned short _port;
    bool           _pack;
  };
}

inline SIOM::SIOMTCPInput::~SIOMTCPInput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::InputServer *SIOM::SIOMTCPInput::createServer ()
{
  _tcpServer = new SIOM::TCPServer (this->memory_pools (),
				    this->max_threads (),
				    _port, _pack);

  return _tcpServer;
}

#endif
