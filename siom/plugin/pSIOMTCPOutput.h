#ifndef _SIOMTCPOutput_h_
#define _SIOMTCPOutput_h_

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"
#include "transport/NetAddress.h"

// Package include files

#include "siom/core/pSIOMOutput.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMTCPDataSender.h"

namespace SIOM
{
  class SIOMTCPOutput : public SIOM::SIOMOutput
  {
   public:
    SIOMTCPOutput () : _addr (0) {};
    virtual ~SIOMTCPOutput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup ();

    virtual SIOM::DataSender *createSender ();

   private:
    NetAddress *_addr;
    SIOM::DataDestination _dest;
    SIOM::TCPDataSender *_tcpSender;
  };
}

inline SIOM::SIOMTCPOutput::~SIOMTCPOutput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::DataSender *SIOM::SIOMTCPOutput::createSender ()
{
  _tcpSender = new SIOM::TCPDataSender (this->memory_pools (),
					&_dest);

  return _tcpSender;
}

#endif
