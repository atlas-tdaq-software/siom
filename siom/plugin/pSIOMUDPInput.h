#ifndef _SIOMUDPInput_h_
#define _SIOMUDPInput_h_

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siom/core/pSIOMInput.h"
#include "siom/core/SIOMInputServer.h"

#include "siom/plugin/SIOMUDPServer.h"

namespace SIOM
{
  class SIOMUDPInput : public SIOM::SIOMInput
  {
   public:
    SIOMUDPInput () {};
    virtual ~SIOMUDPInput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup () {};

    virtual SIOM::InputServer *createServer ();

   private:
    SIOM::UDPServer *_udpServer;

    unsigned short _port;
    bool           _pack;
  };
}

inline SIOM::SIOMUDPInput::~SIOMUDPInput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::InputServer *SIOM::SIOMUDPInput::createServer ()
{
  _udpServer = new SIOM::UDPServer (this->memory_pools (),
				    this->max_threads (),
				    _port, _pack);

  return _udpServer;
}

#endif
