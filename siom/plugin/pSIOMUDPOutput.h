#ifndef _SIOMUDPOutput_h_
#define _SIOMUDPOutput_h_

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"
#include "transport/NetAddress.h"

// Package include files

#include "siom/core/pSIOMOutput.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMUDPDataSender.h"

namespace SIOM
{
  class SIOMUDPOutput : public SIOM::SIOMOutput
  {
   public:
    SIOMUDPOutput () : _addr (0) {};
    virtual ~SIOMUDPOutput () noexcept;

   private:
    virtual void specific_setup (SIOM::SIOMCoreConfig *core_conf,
				 const DalObject *appConfiguration,
				 const DalObject *objConfiguration, bool debug);
    virtual void specific_unsetup ();

    virtual SIOM::DataSender *createSender ();

   private:
    NetAddress *_addr;
    SIOM::DataDestination _dest;
    SIOM::UDPDataSender *_udpSender;
  };
}

inline SIOM::SIOMUDPOutput::~SIOMUDPOutput () noexcept
{
  this->specific_unsetup ();
}

inline SIOM::DataSender *SIOM::SIOMUDPOutput::createSender ()
{
  _udpSender = new SIOM::UDPDataSender (this->memory_pools (),
					&_dest);

  return _udpSender;
}

#endif
