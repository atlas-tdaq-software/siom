#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/CmdLineParser.h"

/* parameters and function declarations */

#include "siom/apps/generator.h"
#include "siom/apps/CircGenerator.h"

/* global variables */

CircGenerator *cg;
daq::rc::ItemCtrl *GeneratorItem;
daq::rc::CmdLineParser *GeneratorParser;

int circ_generator_init (int argc, char **argv)
{
  int errcode = CIRC_GENERATOR_ERR_NOERR;

  /* Parse arguments */

  try
  {
    GeneratorParser = new daq::rc::CmdLineParser (argc, argv, true);
  }

  catch (daq::rc::CmdLineHelp& e)
  {
    std::cerr << e.what () << std::endl;
    return CIRC_GENERATOR_ERR_ARGS;
  }

  catch (daq::rc::CmdLineError& e)
  {
    std::cerr << e.what () << std::endl;
    return CIRC_GENERATOR_ERR_ARGS;
  }

  /* Create CircGenerator */

  std::shared_ptr <CircGenerator> cg = std::make_shared <CircGenerator> ();

  if (GeneratorParser->interactiveMode ())
    setenv ("TDAQ_DB_IMPLEMENTATION", "OKS", 1);

  /* set up parameters */

  cg->setup (argc, argv);
 
  /* create the control thread */

  GeneratorItem = new daq::rc::ItemCtrl (*GeneratorParser, cg);
  GeneratorItem->init ();

  return errcode;
}

int circ_generator_end ()
{
  int errcode = CIRC_GENERATOR_ERR_NOERR;

  delete GeneratorItem;
  delete GeneratorParser;
  delete cg;

  return errcode;
}

int circ_generator_loop ()
{
  int errcode = CIRC_GENERATOR_ERR_NOERR;

  GeneratorItem->run ();

  return errcode;
}

int main (int argc, char **argv)
{
  int err = CIRC_GENERATOR_ERR_NOERR;

  if ((err = circ_generator_init (argc, argv)) != CIRC_GENERATOR_ERR_NOERR)
    return err;

  if ((err = circ_generator_loop ()) != CIRC_GENERATOR_ERR_NOERR)
    return err;

  if ((err = circ_generator_end ()) != CIRC_GENERATOR_ERR_NOERR)
    return err;
}
