#include <string>

#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"

// parameters

#define SIOM_ERR_NOERR      0
#define SIOM_ERR_ARGS      -1

// Simplified IOM object

#include "siom/core/SimplifiedIOM.h"

// global variables

SIOM::SimplifiedIOM *siom;
daq::rc::ItemCtrl *SIOMItem;
daq::rc::CmdLineParser *SIOMParser;

int siom_init (int argc, char **argv)
{
  int errcode = SIOM_ERR_NOERR;

  /* Parse arguments */

  try
  {
    SIOMParser = new daq::rc::CmdLineParser (argc, argv, true);
  }

  catch (daq::rc::CmdLineHelp& e)
  {
    std::cerr << e.what () << std::endl;
    return SIOM_ERR_ARGS;
  }

  catch (daq::rc::CmdLineError& e)
  {
    std::cerr << e.what () << std::endl;
    return SIOM_ERR_ARGS;
  }

  /* Create SIOM */

  const std::string application_name = SIOMParser->applicationName ();

  std::shared_ptr <SIOM::SimplifiedIOM> siom = std::make_shared <SIOM::SimplifiedIOM> (application_name);
  siom->setInteractive (SIOMParser->interactiveMode ());

  if (SIOMParser->interactiveMode ())
    setenv ("TDAQ_DB_IMPLEMENTATION", "OKS", 1);

  /* create the control thread */

  SIOMItem = new daq::rc::ItemCtrl (*SIOMParser, siom);
  SIOMItem->init ();

  return errcode;
}

int siom_end ()
{
  int errcode = SIOM_ERR_NOERR;

  delete SIOMItem;
  delete SIOMParser;
  delete siom;

  return errcode;
}

int siom_loop ()
{
  int errcode = SIOM_ERR_NOERR;

  SIOMItem->run ();

  return errcode;
}

int main (int argc, char **argv)
{
  int err = SIOM_ERR_NOERR;

  if ((err = siom_init (argc, argv)) != SIOM_ERR_NOERR)
    return err;

  if ((err = siom_loop ()) != SIOM_ERR_NOERR)
    return err;

  if ((err = siom_end ()) != SIOM_ERR_NOERR)
    return err;

  return err;
}
