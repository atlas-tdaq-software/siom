#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <stdint.h>

#include <iostream>
#include <list>
#include <thread>

#include "rcc_time_stamp/tstamp.h"

#include "siom/apps/generator_thread.h"
#include "siom/apps/generator.h"
#include "siom/apps/genseq.h"

/* global library variables */

static int g_maxev   = DEFAULT_MAXEV;
static int g_seqlen  = DEFAULT_SEQLEN;
static int g_length  = DEFAULT_LENGTH;
static int g_dlen    = DEFAULT_DLEN;
static int g_ptime   = DEFAULT_PTIME;
static int g_stime   = DEFAULT_STIME;
static int g_circlen = DEFAULT_CIRCLEN;
static int g_port    = DEFAULT_PORT;
static int g_nthread = DEFAULT_NTHREAD;
static int g_sleep   = DEFAULT_SLEEP;

static bool g_started = false;
static bool g_backpressure = false;

static int g_nrunning = 0;

static float g_rate   = DEFAULT_RATE;

static char *g_circroot       = (char *) NULL;

static std::list<generator_thread *> g_threads;

/* internal signal handler prototype */

void generator_sighandler (int num);

void generator_help (char *progname)
{
  fprintf (stderr,
	   "Usage: %s [options]\n"
	   "\t[-N <n>]      generate <n> events then stop, default=<run forever>\n"
	   "\t[-g <len>]    length of emulated event distance sequence, default=%d\n"
	   "\t[-v <rate>]   average event rate, default=%f\n"
	   "\t[-l <len>]    average event length (bytes), default=%d\n"
	   "\t[-d <dlen>]   sigma of event length (bytes), default=%d\n"
	   "\t[-p <time>]   average processing time (usec), default=%d usec\n"
	   "\t[-S <time>]   sigma of processing time (usec), default=%d usec\n"
	   "\t[-c <root>]   output buffer name root, default=%s\n"
	   "\t[-b <clen>]   output buffer length (bytes), default=%d\n"
	   "\t[-r <port>]   buffer server port, default = %d\n"
	   "\t[-u]          create backpressure if buffer full, default=false\n"
	   "\t[-t <thread>] number of threads, default=%d, max=%d\n"
	   "\t[-w <sec>]    sleep time for statistics collection, default=%d sec\n"
	   "\t[-h]          help\n",
	   progname, DEFAULT_SEQLEN, DEFAULT_RATE,
	   DEFAULT_LENGTH, DEFAULT_DLEN,
	   DEFAULT_PTIME, DEFAULT_STIME,
           DEFAULT_CIRCROOT, DEFAULT_CIRCLEN, DEFAULT_PORT,
	   DEFAULT_NTHREAD, GENERATOR_MAXNTHREAD, DEFAULT_SLEEP);
}

int generator_args (int argc, char **argv)
{
  int errcode = GENERATOR_ERR_NOERR;
  int i;

  for (i=1; i < argc; ++i)
  {
    if (argv[i][0] == '-')
      switch (argv[i][1])
      {
        case 'n':
        case 's':
        case 'P':
	  ++i;
	  break;
        case 'i':
	  break;
        case 'N':
	  g_maxev = atoi (argv[++i]);
	  break;
        case 'g':
	  g_seqlen = atoi (argv[++i]);
	  break;
        case 'v':
	  g_rate = atof (argv[++i]);
	  break;
        case 'l':
	  g_length = atoi (argv[++i]);
	  break;
        case 'd':
	  g_dlen = atoi (argv[++i]);
	  break;
        case 'p':
	  g_ptime = atoi (argv[++i]);
	  break;
        case 'S':
	  g_stime = atoi (argv[++i]);
	  break;
        case 'w':
	  g_sleep = atoi (argv[++i]);
	  break;
        case 'c':
	  ++i;
	  if ((g_circroot = (char *) malloc (strlen (argv[i]) + 2))
	      == (char *) NULL)
	    return GENERATOR_ERR_ALLOC;

	  strcpy (g_circroot, argv[i]);
          strcat (g_circroot, "_");

	  break;
        case 'b':
	  g_circlen = atoi (argv[++i]);
	  break;
        case 'r':
	  g_port = atoi (argv[++i]);
	  break;
        case 'u':
	  g_backpressure = true;
	  break;
        case 't':
	  g_nthread = atoi (argv[++i]);
	  break;
        case 'h':
	  generator_help (argv[0]);
	  exit (GENERATOR_ERR_NOERR);
        default:
	  generator_help (argv[0]);
	  exit (GENERATOR_ERR_ARGS);
      }
    else
    {
      generator_help (argv[0]);
      exit (GENERATOR_ERR_ARGS);
    }
  }

  if (g_circroot == (char *) NULL)
  {
    if ((g_circroot = (char *) malloc (strlen (DEFAULT_CIRCROOT) + 1))
	== (char *) NULL)
      return GENERATOR_ERR_ALLOC;

    strcpy (g_circroot, DEFAULT_CIRCROOT);
  }

  if (g_nthread > GENERATOR_MAXNTHREAD)
  {
    generator_help (argv[0]);
    exit (GENERATOR_ERR_ARGS);
  }

  return errcode;
}

void generator_sighandler (int num)
{
  /* on signal num set status to end */

  if (num == SIGINT || num == SIGQUIT)
  {
    std::cout << "signal " << num << " received" << std::endl;
    g_nrunning = 0;
  }
}

int generator_init ()
{
  int i;
  int errcode = GENERATOR_ERR_NOERR;

  char *circname;

  TS_ErrorCode_t terr;

  /* init timestamp library */

  if ((terr = ts_open (1, TS_DUMMY)) != TSE_OK)
    return GENERATOR_ERR_TSERR;

  /* install termination signal handler */

  if (signal (SIGINT, generator_sighandler) == SIG_ERR)
    return GENERATOR_ERR_SIGINST;

  if (signal (SIGQUIT, generator_sighandler) == SIG_ERR)
    return GENERATOR_ERR_SIGINST;

  /* allocate space for circular buffer name */

  if ((circname = (char *) malloc (strlen (g_circroot) +
				   GENERATOR_NTHRLEN + 1)) == (char *) NULL)
    return GENERATOR_ERR_ALLOC;

  for (i=0; i < g_nthread; ++i)
  {
    int nl, nt, np;
    int *islen;
    float *slen, *stime, *sproc;

    int j;

    nt = g_seqlen;
    np = (int) ((float) nt * sqrt (2.));
    nl = GENERATOR_MAXLENSEQ;

    /* initialise event length sequence */

    if ((slen = (float *) malloc (nl * sizeof (float))) ==
	(float *) NULL)
    {
      generator_end ();
      return GENERATOR_ERR_ALLOC;
    }

    if ((islen = (int *) malloc (nl * sizeof (int))) ==
	(int *) NULL)
    {
      generator_end ();
      return GENERATOR_ERR_ALLOC;
    }

    gen_gauss_seq (slen, nl, (float) g_length, (float) g_dlen);

    for (j=0; j < nl; ++j)
      islen[j] = (int) slen[j];

    /* initialise random time sequences
       (both event distance and processing time)
       for each thread                           */

    float tau = 1000000./g_rate;

    if ((stime = (float *) malloc (nt * sizeof (float))) ==
	(float *) NULL)
    {
      generator_end ();
      return GENERATOR_ERR_ALLOC;
    }

    gen_exp_seq (stime, nt, tau);

    if ((sproc = (float *) malloc (np * sizeof (float))) ==
	(float *) NULL)
    {
      generator_end ();
      return GENERATOR_ERR_ALLOC;
    }

    gen_gauss_seq (sproc, np, (float) g_ptime, (float) g_stime);

    /* associate a circular buffer name to the thread */

    sprintf (circname, "%s%d", g_circroot, i); 

    /* define a thread */

    generator_thread *t = new generator_thread ();

    /* define thread input sequences */

    t->input (islen, stime, sproc, nl, nt, np);

    /* define thread output circular buffer */

    t->output (circname, g_circlen, g_port, (! g_backpressure));

    /* initialise thread */

    if ((errcode = t->init (g_maxev)) != 0)
    {
      delete t;
      generator_end ();
      return GENERATOR_ERR_ALLOC;
    }

    /* add the thread pointer to the thread vector */

    g_threads.push_back (t);
  }

  free (circname);

  return errcode;
}

void generator_set_running (bool r_flag)
{
  std::list <generator_thread *>::iterator i;

  for (i = g_threads.begin (); i != g_threads.end (); ++i)
    (*i)->set_running (r_flag);

  g_started = true;
}

int generator_end ()
{
  int errcode = GENERATOR_ERR_NOERR;
  char hname[1024];

  gethostname (hname, 1024);

  std::cout << "Calling generator_end on node " << hname << std::endl;

  /* cancel running threads */

  std::list <generator_thread *>::iterator i;

  if (g_started)
    for (i = g_threads.begin (); i != g_threads.end (); ++i)
      delete (*i);

  g_threads.clear ();

  ts_close (TS_DUMMY);

  return errcode;
}

int generator_loop ()
{
  tstamp t1;

  int errcode = GENERATOR_ERR_NOERR;

  std::list <generator_thread *>::iterator i;

  /* start clock for statistic timer */

  ts_clock (&t1);

  g_nrunning = g_nthread;

  do
  {
    float totrate = 0, dt;
    tstamp t2;

    ts_clock (&t2);
    dt = ts_duration (t1, t2);

    for (i = g_threads.begin (); i != g_threads.end (); ++i)
    {
      /* check thread completion */

      if ((*i)->is_running () == false)
      {
        delete (*i);
	g_threads.erase (i);
	--g_nrunning;
	break;
      }
      else if (dt > (float) g_sleep)
      {
	int   nev = (*i)->events ();
	int   thr = (*i)->thrown ();
	int   ful = (*i)->circfull ();
	float occ = (*i)->occupancy ();
	float rate = (*i)->rate ();

	std::cout << "In last " << dt << " seconds: Thread events " 
		  << nev << " Lost events " << thr << " Buffer full " 
		  << ful << " Occupancy " << occ << " Event rate "
		  << rate << std::endl;

	totrate += rate;
	ts_clock (&t1);
      }
    }

    if (dt > (float) g_sleep)
      std::cout << "Total rate = " << totrate << std::endl; 

    /* sleep 1 second */

    sleep (1);

  } while (g_nrunning);

  return errcode;
}
