#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <iostream>
#include <netdb.h>

#include "rcc_time_stamp/tstamp.h"

#include "circ/Circ.h"
#include "circ/Circservice.h"

#include "siom/apps/generator.h"
#include "siom/apps/generator_thread.h"

using namespace std;

#define MAXNAMESIZE  2048
#define SECTOUSEC    1000000

void generator_thread::input (int *lenseq, float *tseq, float *pseq,
	 		      int nlseq, int ntseq, int npseq)
{
  _lenseq = _pl = lenseq;
  _tseq = _pt = tseq;
  _pseq = _pp = pseq;

  _nlseq = nlseq;
  _ntseq = ntseq;
  _npseq = npseq;
}

void generator_thread::output (char *circname, int buflen, int port, bool nobackpressure)
{
  _circname = circname;
  _circlen  = buflen;
  _circport = port;
  _outputCirc = -1;
  _throwIfFull = nobackpressure;
}

int generator_thread::init (int maxev)
{
  TS_ErrorCode_t terr;

  if ((terr = ts_open (1, TS_DUMMY)) != TSE_OK)
  {
    std::cerr << "Warning: ts library is closed!" << std::endl;
    return 1;
  }

  _nev = 0;
  _maxev = maxev;
  _occupancy = 0.;
  _full = 0;
  _thrown = 0;
  _rate = 0;

  char *hname = (char *) malloc (MAXNAMESIZE);
  gethostname (hname, MAXNAMESIZE);
  struct hostent *hent = gethostbyname (hname);
  _node = *((uint32_t *) hent->h_addr);
  _proc = (uint32_t) getpid ();

  free (hname);

  return 0;
}

void generator_thread::run ()
{
  TS_ErrorCode_t terr;
  struct timeval t0, t1;
 
  int oldnev = 0;

  gettimeofday (&t0, 0);

  while (_nev != _maxev && ! _quit.load ())
  {
    if (_running)
    {
      unsigned int nyield;

      if (_outputCirc < 0)
	if ((_outputCirc = CircOpenCircConnection (_circport,
						   const_cast <char *> (_circname.c_str ()),
						   _circlen)) < 0)
	{
	  std::cerr << "No server available" << std::endl;
	  sleep (1);
	  continue;
	}

      if ((_pl - _lenseq) == _nlseq)
	_pl = _lenseq;

      if ((_pt - _tseq) == _ntseq)
	_pt = _tseq;

      if ((_pp - _pseq) == _npseq)
	_pp = _pseq;

      unsigned int length = *_pl - (*_pl % sizeof (uint32_t)); ++_pl;

      if (length < (5 * sizeof (uint32_t)))
	continue;

      float etime = *_pt; ++_pt;
      float ptime = *_pp; ++_pp;

      int t = (int) (ptime > etime ? ptime : etime);

      /* wait t microseconds */

      if (t > 0)
      {
	if (t > 10000)
	  usleep ((unsigned long) t);
	else
	{
	  if ((terr = ts_wait (t, &nyield)) != TSE_OK)
	    std::cerr << "Warning: ts library is closed!" << std::endl;
	}
      }

      /* reserve a chunk of the circular buffer */

      bool discard = false;
      char *ptr;

      if ((ptr = CircReserve (_outputCirc, ++_nev, length)) == (char *) -1)
      {
	++_full;

	if (_throwIfFull)
	  discard = true;
	else
	  while ((ptr = CircReserve (_outputCirc, _nev, length)) == (char *) -1)
	  {
	    ++_full;
            std::this_thread::yield ();
	  }
      }

      if (discard)
	++_thrown;
      else
      {
	int words = length / sizeof (uint32_t);

	int i;
	uint32_t *iptr = (uint32_t *) ptr;

	/* fill the buffer */

	bzero (ptr, length);

	*iptr = length; ++iptr;
	*iptr = _nev; ++iptr;
	*iptr = _node; ++iptr;
	*iptr = _proc; ++iptr;
	*iptr = (uint32_t) ((unsigned long) this & 0xffffffff); ++iptr;

	for (i = 5; i < words ; ++i, ++iptr)
	  *iptr = i;

	/* validate the buffer */

	CircValidate (_outputCirc, _nev, ptr, length);
      }
    }
    else
    {
      if (_outputCirc >= 0)
      {
	/* disconnect from the circular buffer */

	CircCloseCircConnection (_circport,
				 const_cast <char *> (_circname.c_str ()),
				 _outputCirc);
	_outputCirc = -1;
      }
    }
	
    gettimeofday (&t1, 0);

    float dsec, dusec;

    if ((dsec = (float) (t1.tv_sec - t0.tv_sec)) > 3)
    {
      dusec = (float) (t1.tv_sec - t0.tv_sec) / SECTOUSEC;
      _rate = (float) (_nev - oldnev) / (dsec + dusec);

      oldnev = _nev;
      memcpy (&t0, &t1, sizeof (struct timeval));
    }

    if (_outputCirc >= 0)
      _occupancy = (float) CircGetOccupancy (_outputCirc) / _circlen;

    std::this_thread::yield ();
  }

  _running = false;

  while (! _quit.load ())
    sleep (1);
}

void generator_thread::cleanup ()
{
  ts_close (TS_DUMMY);
}
