#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

#include "siom/apps/genseq.h"

int gen_gauss_seq (float *seq, unsigned int nseq, float mean, float sigma)
{
  struct timeval t;

  unsigned int i;
  float *p;

  gettimeofday (&t, 0);
  srand ((int) t.tv_usec);

  for (i=0, p=seq; i < nseq; ++i, ++p)
  {
    float s1, s2, w;

    do
    {
      s1 = 2. * ((float) rand () / (float) RAND_MAX) - 1.;
      s2 = 2. * ((float) rand () / (float) RAND_MAX) - 1.;

      w = s1 * s1 + s2 * s2;
    } while (w == 0. || w >= 1.);

    w = sqrt (-2. * log (w) / w);

    *p = s1 * w;
    *p *= sigma;
    *p += mean;
  }

  return nseq;
}

int gen_exp_seq (float *seq, unsigned int nseq, float mean)
{
  struct timeval t;

  unsigned int i;
  float *p;

  gettimeofday (&t, 0);
  srand ((int) t.tv_usec);

  for (i=0, p=seq; i < nseq; ++i, ++p)
  {
    float x;

    do
    {
      x = (float) rand () / (float) RAND_MAX;
    } while (x == 0. || x == 1.);

    *p = - mean * log (x);
  }

  return nseq;
}
