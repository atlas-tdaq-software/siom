#include <iostream>

#include <stdlib.h>
#include <execinfo.h>

#include "siom/core/SIOMBackTrace.h"

/* Obtain a backtrace and print it to stderr. */

void SIOM::print_trace (void)
{
   void *array[MAX_BACKTRACE_ELEMENTS];
   int size, i;
   char **strings;

   size = backtrace (array, MAX_BACKTRACE_ELEMENTS);
   strings = backtrace_symbols (array, size);

   std::cerr << "Obtained stack frames " << size << std::endl;

   for (i = 0; i < size; i++)
     std::cerr << " " << strings[i] << std::endl;

   free (strings);
}
