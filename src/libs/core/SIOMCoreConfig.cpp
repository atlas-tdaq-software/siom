#include <string>

// Include files for the database

#include "config/ConfigObject.h"
#include "config/Configuration.h"

// dal headers

#include "dal/Partition.h"
#include "dal/Application.h"
#include "dal/util.h" // for substitute_variables

// DFdal headers

#include "DFdal/MemoryPool.h"
#include "DFdal/EventStorage.h"

// IS headers and run parameters

#include <is/infodictionary.h>
#include <rc/RunParams.h>

// siom headers

#include "siom/core/SIOMCoreException.h"
#include "siom/core/SIOMErrorReporting.h"
#include "siom/core/SIOMCoreConfig.h"

SIOM::SIOMCoreConfig::SIOMCoreConfig (const std::string &SIOM_Name)
{
  char *pname = (char *) NULL;

  this->setName (SIOM_Name);

  m_confDB = 0;

  // For the Run parameters stored in IS

  if ((pname = getenv ("TDAQ_PARTITION")) == (char *) NULL)
  {
    CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			   SIOMCoreException::IPC_PARTITION_ERROR,
			   "Undefined partition");
    ers::fatal (e);
    throw e;
  }

  m_IPCPartition = new IPCPartition (pname);

  if (! m_IPCPartition)
  {
    CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			   SIOMCoreException::IPC_PARTITION_ERROR,
			   pname);
    ers::fatal (e);
    throw e;
  }

  m_ISDict = new ISInfoDictionary (*m_IPCPartition);

  if (! m_ISDict)
  {
    std::ostringstream message;
    message << " Partition " << pname;

    CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			   SIOMCoreException::IS_DICTIONARY_ERROR,
                           message.str());
    ers::error (e);
    throw e;
  }
}

SIOM::SIOMCoreConfig::~SIOMCoreConfig ()
{ 
  if (m_confDB)
  {
    delete m_confDB;
    m_confDB = 0;
  }

  if (m_ISDict)
  {
    delete m_ISDict;
    m_ISDict = 0;
  }

  if (m_IPCPartition)
  {
    delete m_IPCPartition;
    m_IPCPartition = 0;
  }
}

void SIOM::SIOMCoreConfig::get_config ()
{
  if (! m_confDB)
  {
    try
    {
      m_confDB = new Configuration ("");
    }

    catch (std::exception &ex)
    {
      ENCAPSULATE_SIOM_EXCEPTION (e, SIOMCoreException,
			  SIOMCoreException::CONFIGURATION_NOT_FOUND,
			  ex,
			  "Configuration exception");
      ers::fatal (e);
      throw e;
    }
  }
  else
  {
    CREATE_SIOM_MESSAGE (message, "Configuration already created: did not reload");
    ers::info (message);
  }

  if (! m_confDB->loaded ())
  {
    delete m_confDB;
    m_confDB = 0;

    std::ostringstream message;
    message << " Partition " << getenv ("TDAQ_PARTITION");

    CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			   SIOMCoreException::DB_NOT_LOADED,
                           message.str());
    ers::fatal (e);
    throw e;
  }

  std::string any;

  if (! (m_dbPartition = daq::core::get_partition (*m_confDB, any)))
  {
    CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			   SIOMCoreException::PARTITION_NOT_FOUND,
			   getenv ("TDAQ_PARTITION"));
    ers::fatal (e);
    throw e;
  }

  // Ask that all $variables in the DB are automatically substituted

  m_confDB->register_converter (new daq::core::SubstituteVariables (*m_dbPartition));

  // get application configuration

  const std::string name = this->getName ();
  char *dbName, *c;
  std::string s;

  if ((c = (char *) strchr (name.c_str (), ':')))
  {
    size_t ilen = (size_t) (c - name.c_str ());
    dbName = (char *) malloc (ilen + 1);
    strncpy (dbName, name.c_str (), ilen); 
    dbName[ilen] = '\0';
    s = dbName;
  }
  else
    s = name;

  if (! (m_dbApp = m_confDB->get <siomdal::SIOMApplicationBase> (s)))
  {
    CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			   SIOMCoreException::APPLICATION_NOT_FOUND,
			   s);
    ers::fatal (e);
    throw e;
  }
}

void SIOM::SIOMCoreConfig::get_runParams (RunParams &ISrunParams)
{
  try
  {
    m_ISDict->getValue ("RunParams.RunParams", ISrunParams);
  }

  catch (daq::is::Exception& runParamsException)
  {
    ENCAPSULATE_SIOM_EXCEPTION (e, SIOMCoreException,
				SIOMCoreException::NOT_IN_IS,
				runParamsException,
				"No run params info in IS");
    ers::warning (e);
    throw e;
  }
}
