#include <map>
#include <string>
#include <vector>
#include <exception>
#include <ctime>
#include <chrono>

#include "siom/core/SIOMActiveObject.h"
#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataProcessor.h"
#include "siom/core/SIOMAlgoParams.h"

SIOM::DataProcessor::DataProcessor (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool_in,
				    std::vector <SIOM::MemoryPool <uint8_t> *> &mpool_out,
				    SIOM::AlgoParams *params)
  : m_inputPoolVector (mpool_in), m_outputPoolVector (mpool_out), m_params (params)
{
  m_minElements = 0;
  m_pollFactor = 1;

  // Init timer

  gettimeofday (&m_t0, 0);

  // Pointer to IS information

  m_info = 0;
}

SIOM::DataProcessor::~DataProcessor ()
{
  if (m_info)
    this->deleteInfo (m_info);
}

void SIOM::DataProcessor::run ()
{
  auto it = m_inputPoolVector.begin ();
  auto ot = m_outputPoolVector.begin ();

  while (! _quit.load ())
  {
    SIOM::MemoryPool <uint8_t> *inMemoryPool = (*it);
    SIOM::MemoryPool <uint8_t> *outMemoryPool = (*ot);

    SIOM::MemoryBuffer <uint8_t> *b = 0;

    // get a buffer pointer from memory pool or switch to next pool

    if (inMemoryPool->elements () > m_minElements)
    {
      if ((b = inMemoryPool->get_next ()))
      {
	SIOM::MemoryBuffer <uint8_t> *outbuf;
	unsigned int input_size = b->size ();
	unsigned int output_size;

	// guess the first available buffer (may depend on the processor)

	while ((outbuf = this->can_send (outMemoryPool, input_size)) == 0)
	{
	  if (++ot == m_outputPoolVector.end ())
	    ot = m_outputPoolVector.begin ();

	  outMemoryPool = (*ot);

	  if (_quit.load ())
            break;

          std::this_thread::yield ();
	}

	// process data

	while ((output_size = this->process_data (b, outbuf)) < input_size)
	{
	  // This can happen when the buffer needs to be extended

	  if (_quit.load ())
            break;

          std::this_thread::yield ();
	}

	// release the input buffer

	inMemoryPool->release (b);

	// release the output buffer

	outMemoryPool->validate (outbuf, output_size);

	// update statistics

	++m_nof_pack;
	m_nof_Mbytes += (double) input_size / MBYTE;
      }

      std::this_thread::yield ();
    }
    else
    {
      // Check the rate and calculate waiting time

      double sec = m_info ?
	(((m_info->PacketRate > 0) ? (double) m_pollFactor / m_info->PacketRate :
	  s_processor_initial_invrate)) : s_processor_initial_invrate;

      if (_quit.load ())
	break;

      if (sec > 1)
	std::this_thread::sleep_for (std::chrono::seconds (1));
      else
      {
	long int nsec = (long int) (sec * SECTONSEC);

	if (nsec > MINSLEEP)
	  std::this_thread::sleep_for (std::chrono::nanoseconds (nsec));
	else
	  std::this_thread::yield ();
      }
    }

    // switch to next buffers

    if (++it == m_inputPoolVector.end ())
      it = m_inputPoolVector.begin ();

    if (++ot == m_outputPoolVector.end ())
      ot = m_outputPoolVector.begin ();
  }
}

void SIOM::DataProcessor::clearInfo ()
{
  m_nof_Mbytes = 0;
  m_nof_pack = 0;

  if (m_info)
  {
    m_info->ProcessedPackets = 0;
    m_info->ProcessedData = 0;
  }

  // Reset timers

  gettimeofday (&m_t0, 0);
}

ISInfo *SIOM::DataProcessor::getISInfo ()
{
  float dtime;
  struct timeval t;

  gettimeofday (&t, 0);
  dtime = (float) (t.tv_sec - m_t0.tv_sec) + (float) (t.tv_usec - m_t0.tv_usec) / SECTOUSEC;

  // Create IS information data object

  if (! m_info)
  {
    m_info = this->createInfo ();

    m_info->ActiveThreads = 1;
    m_info->ActiveMemoryPools = m_inputPoolVector.size () + m_outputPoolVector.size ();

    m_info->ActivePoolSize.clear ();
    m_info->ActivePoolPages.clear ();
    m_info->ActivePoolPageSize.clear ();

    for (auto ipool = m_inputPoolVector.begin (); ipool != m_inputPoolVector.end (); ++ipool)
    {
      SIOM::MemoryPool <uint8_t> *pool = (*ipool);

      m_info->ActivePoolSize.push_back ((float) (pool->page_size () *
						 pool->page_number ()) / (1024 * 1024));
      m_info->ActivePoolPages.push_back (pool->page_number ());
      m_info->ActivePoolPageSize.push_back (pool->page_size ());
    }

    for (auto opool = m_outputPoolVector.begin (); opool != m_outputPoolVector.end (); ++opool)
    {
      SIOM::MemoryPool <uint8_t> *pool = (*opool);

      m_info->ActivePoolSize.push_back ((float) (pool->page_size () *
						 pool->page_number ()) / (1024 * 1024));
      m_info->ActivePoolPages.push_back (pool->page_number ());
      m_info->ActivePoolPageSize.push_back (pool->page_size ());
    }
  }

  m_info->ActivePoolOccupancy.clear ();

  for (auto ipool = m_inputPoolVector.begin (); ipool != m_inputPoolVector.end (); ++ipool)
    m_info->ActivePoolOccupancy.push_back ((float) (*ipool)->used_pages () /
                                           (float) (*ipool)->page_number ());

  for (auto opool = m_outputPoolVector.begin (); opool != m_outputPoolVector.end (); ++opool)
    m_info->ActivePoolOccupancy.push_back ((float) (*opool)->used_pages () /
                                           (float) (*opool)->page_number ());

  uint64_t old_packets = m_nof_pack < m_info->ProcessedPackets ? 0 : m_info->ProcessedPackets;
  double old_data = m_nof_Mbytes < m_info->ProcessedData ? 0 : m_info->ProcessedData;

  m_info->ProcessedPackets = m_nof_pack;
  m_info->ProcessedData = m_nof_Mbytes;

  m_info->PacketRate = dtime > 0 ?
    (float) (m_nof_pack - old_packets) / dtime : 0;
  m_info->DataRate = dtime > 0 ?
    (m_nof_Mbytes - old_data) / dtime : 0;

  this->fillSpecificInfo (m_info);

  memcpy (&m_t0, &t, sizeof (struct timeval));

  return m_info;
}
