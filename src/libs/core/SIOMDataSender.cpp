#include <map>
#include <string>
#include <vector>
#include <exception>
#include <ctime>
#include <chrono>

#include "siom/core/SIOMActiveObject.h"
#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

SIOM::DataSender::DataSender (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
			      SIOM::DataDestination *dest)
  : m_poolVector (mpool), m_dest (dest)
{
  m_minElements = 0;
  m_pollFactor = 1;

  // Init timer

  gettimeofday (&m_t0, 0);

  // Pointer to IS info

  m_info = 0;
}

SIOM::DataSender::~DataSender ()
{
  if (m_info)
    this->deleteInfo (m_info);
}

void SIOM::DataSender::run ()
{
  auto it = m_poolVector.begin ();

  while (! _quit.load ())
  {
    SIOM::MemoryPool <uint8_t> *memoryPool = (*it);
    SIOM::MemoryBuffer <uint8_t> *b = 0;

    // get a buffer pointer from memory pool or switch to next pool

    if (memoryPool->elements () > m_minElements)
    {
      if ((b = memoryPool->get_next ()))
      {
	int sent;

	// send data to destination asap

	while (! this->can_send ())
        {
	  if (_quit.load ())
            break;

          std::this_thread::yield ();
        }

	if ((sent = this->send (b)) > 0)
	{
	  ++m_nof_pack;

	  // update statistics

	  m_nof_Mbytes += (double) sent / MBYTE;
	}

	// release the input buffer

	memoryPool->release (b);
      }

      std::this_thread::yield ();
    }
    else
    {
      // Check the rate and calculate waiting time

      double sec = m_info ?
	(((m_info->PacketRate > 0) ? (double) m_pollFactor / m_info->PacketRate :
	  s_sender_initial_invrate)) : s_sender_initial_invrate;

      if (_quit.load ())
	break;

      if (sec > 1)
	std::this_thread::sleep_for (std::chrono::seconds (1));
      else
      {
	long int nsec = (long int) (sec * SECTONSEC);

	if (nsec > MINSLEEP)
	  std::this_thread::sleep_for (std::chrono::nanoseconds (nsec));
	else
	  std::this_thread::yield ();
      }
    }

    // switch to next buffer

    if (++it == m_poolVector.end ())
      it = m_poolVector.begin ();
  }
}

void SIOM::DataSender::clearInfo ()
{
  m_nof_Mbytes = 0;
  m_nof_pack = 0;

  if (m_info)
  {
    m_info->ProcessedPackets = 0;
    m_info->ProcessedData = 0;
  }

  // Reset timers

  gettimeofday (&m_t0, 0);
}

ISInfo *SIOM::DataSender::getISInfo ()
{
  float dtime;
  struct timeval t;

  gettimeofday (&t, 0);
  dtime = (float) (t.tv_sec - m_t0.tv_sec) + (float) (t.tv_usec - m_t0.tv_usec) / SECTOUSEC;

  // Create IS information data object

  if (! m_info)
  {
    m_info = this->createInfo ();

    m_info->ActiveThreads = 1;
    m_info->ActiveMemoryPools = m_poolVector.size ();

    m_info->ActivePoolSize.clear ();
    m_info->ActivePoolPages.clear ();
    m_info->ActivePoolPageSize.clear ();

    for (auto ipool = m_poolVector.begin (); ipool != m_poolVector.end (); ++ipool)
    {
      SIOM::MemoryPool <uint8_t> *pool = (*ipool);

      m_info->ActivePoolSize.push_back ((float) (pool->page_size () *
						 pool->page_number ()) / (1024 * 1024));
      m_info->ActivePoolPages.push_back (pool->page_number ());
      m_info->ActivePoolPageSize.push_back (pool->page_size ());
    }
  }

  m_info->ActivePoolOccupancy.clear ();

  for (auto ipool = m_poolVector.begin (); ipool != m_poolVector.end (); ++ipool)
    m_info->ActivePoolOccupancy.push_back ((float) (*ipool)->used_pages () /
                                           (float) (*ipool)->page_number ());

  uint64_t old_packets = m_info->ProcessedPackets;
  double old_data = m_info->ProcessedData;

  m_info->ProcessedPackets = m_nof_pack;
  m_info->ProcessedData = m_nof_Mbytes;

  m_info->PacketRate = dtime > 0 ?
    (float) (m_nof_pack - old_packets) / dtime : 0;
  m_info->DataRate = dtime > 0 ?
    (m_nof_Mbytes - old_data) / dtime : 0;

  this->fillSpecificInfo (m_info);

  memcpy (&m_t0, &t, sizeof (struct timeval));

  return m_info;
}
