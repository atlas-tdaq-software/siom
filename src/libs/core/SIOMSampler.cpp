#include <mutex>

#include <string.h>
#include <stdlib.h>

#include "siom/core/SIOMActiveObject.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMSampler.h"

void SIOM::SIOMSampler::run ()
{
  auto ipool = _inpools.begin ();
  auto jpool = _outpools.begin ();

  while (! _quit.load ())
  {
    if (_requeue)
    {
      if (auto b = (*ipool)->get_next_exclude_flag (s_siom_sampler_flag))
      {
	this->pushEvent (b);
	(*ipool)->requeue (b, s_siom_sampler_flag);
      }

      // switch to next buffer

      if (++ipool == _inpools.end ())
	ipool = _inpools.begin ();
    }
    else
    {
      if (auto b = (*ipool)->get_next ())
      {
	this->pushEvent (b);

	// guess the first available buffer (may depend on the processor)

	SIOM::MemoryBuffer <uint8_t> *outbuf;

	while ((outbuf = (*jpool)->reserve (b->n_objects ())) == 0)
	{
	  if (++jpool == _outpools.end ())
	    jpool = _outpools.begin ();

	  if (_quit.load ())
	  {
	    (*ipool)->release (b);
            break;
	  }

          std::this_thread::yield ();
	}

	if (_quit.load ())
	  break;

	// Copy data to the output

	this->copy_data (b, outbuf);

	(*jpool)->validate (outbuf, b->n_objects ());
	(*ipool)->release (b);
      }

      // switch to next buffer

      if (++ipool == _inpools.end ())
	ipool = _inpools.begin ();

      if (++jpool == _outpools.end ())
	jpool = _outpools.begin ();
    }

    std::this_thread::yield ();
  }
}

void SIOM::SIOMSampler::pushEvent (char *pointer, unsigned int size)
{
  unsigned int imod = (size + _header_size) % s_uint_size;
  unsigned int irest = imod == 0 ? 0 : s_uint_size - imod;
  unsigned int totsize = size + _header_size + irest;
  unsigned int isize = totsize / s_uint_size;

  // Reserve buffer for header and
  // align to unsigned int boundary (avoid truncation)

  if (irest > 0 || _header_size > 0)
  {
    if (totsize > _seqdatalen)
    {
      if (_seqdata) delete [] _seqdata;
      _seqdata = new char[totsize];
      _seqdatalen = totsize;
    }

    char *p = _seqdata;

    if (_header_size)
    {
      memcpy (p, _header, _header_size);
      p += _header_size;
    }

    memcpy (p, pointer, size);
    p += size;

    if (irest)
      memset (p, 0, irest);

    this->pushEvent (reinterpret_cast <unsigned int *> (_seqdata), static_cast <size_t> (isize));
  }
  else
    this->pushEvent (reinterpret_cast <unsigned int *> (pointer), static_cast <size_t> (isize));
}

void SIOM::SIOMSampler::pushEvent (SIOM::MemoryBuffer <uint8_t> *buffer)
{
  auto pages = buffer->get_pages ();
  auto pit = pages.begin ();

  unsigned int size = buffer->n_objects ();

  unsigned int imod = (size + _header_size) % s_uint_size;
  unsigned int irest = imod == 0 ? 0 : s_uint_size - imod;
  unsigned int totsize = size + _header_size + irest;
  unsigned int isize = totsize / s_uint_size;

  // Reserve buffer for header and
  // align to unsigned int boundary (avoid truncation)

  if (totsize > _seqdatalen)
  {
    if (_seqdata) delete [] _seqdata;
    _seqdata = new char[totsize];
    _seqdatalen = totsize;
  }

  char *p = _seqdata;

  if (_header_size)
  {
    memcpy (p, _header, _header_size);
    p += _header_size;
  }

  for (; pit != pages.end (); ++pit)
  {
    unsigned int psize = (size > buffer->page_size () ? buffer->page_size () : size);
    memcpy (p, reinterpret_cast <char *> (*pit), psize);
    p += psize;
    size -= psize;
  }

  if (irest)
    memset (p, 0, irest);

  this->pushEvent (reinterpret_cast <unsigned int *> (_seqdata), static_cast <size_t> (isize));
}
