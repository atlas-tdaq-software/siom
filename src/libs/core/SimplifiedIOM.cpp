// Standard include files

#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/resource.h>
#include <sys/times.h>

#include <list>
#include <vector>
#include <sstream>
#include <ctime>

// TDAQ include files

#include "RunControl/Common/Controllable.h"

#include "is/infodictionary.h"
#include "is/exceptions.h"
#include "emon/EventSampler.h"

// Package include files

#include "siomdal/SIOMInput.h"
#include "siomdal/SIOMOutput.h"
#include "siomdal/SIOMProcessor.h"

#include "siom/core/SIOMCoreConfig.h"
#include "siom/core/SIOMBackTrace.h"
#include "siom/core/SIOMPluginFactory.h"
#include "siom/core/SIOMSamplerFactory.h"
#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/pSIOMPlugin.h"
#include "siom/core/pSIOMInput.h"
#include "siom/core/pSIOMOutput.h"
#include "siom/core/pSIOMProcessor.h"
#include "siom/core/SimplifiedIOM.h"

#include "siom_info/siomCoreNamed.h"

#define SEC_TO_USEC  1000000

// Signal handler to be redefined

struct sigaction oldact_segv;
struct sigaction oldact_abrt;
struct sigaction oldact_fpe;
struct sigaction oldact_sys;

// Signal handler

void SimplifiedIOM_SignalHandler (int sig)
{
  struct sigaction *oldact;

  printf ("Got signal %d\n", sig);
  SIOM::print_trace ();

  switch (sig)
  {
    case SIGSEGV:
      oldact = &oldact_segv;
      break;
    case SIGABRT:
      oldact = &oldact_abrt;
      break;
    case SIGFPE:
      oldact = &oldact_fpe;
      break;
    case SIGSYS:
      oldact = &oldact_sys;
      break;
    default:
      break;
  }

  if (oldact)
    oldact->sa_handler (sig);

  exit (sig);
}

SIOM::SimplifiedIOM::SimplifiedIOM (const std::string &name)
  : m_name (name)
{
  struct sigaction siom_handler;

  // Install signal handler

  siom_handler.sa_handler = SimplifiedIOM_SignalHandler;
  siom_handler.sa_flags = 0;

  sigaction (SIGSEGV, (const struct sigaction *) NULL, &oldact_segv);
  sigaction (SIGSEGV, &siom_handler, (struct sigaction *) NULL);
  sigaction (SIGABRT, (const struct sigaction *) NULL, &oldact_abrt);
  sigaction (SIGABRT, &siom_handler, (struct sigaction *) NULL);
  sigaction (SIGFPE, (const struct sigaction *) NULL, &oldact_fpe);
  sigaction (SIGFPE, &siom_handler, (struct sigaction *) NULL);
  sigaction (SIGSYS, (const struct sigaction *) NULL, &oldact_sys);
  sigaction (SIGSYS, &siom_handler, (struct sigaction *) NULL);

  // Initialize interactive mode flag

  m_interactiveMode = false;

  // Initialize timer

  gettimeofday (&m_probetime, (struct timezone *) NULL);
  memset (&m_starttime, 0, sizeof (struct timezone ));

  // Initialize core configurator pointer

  core_conf = 0;

  // Publish should be active only when the system is completely configured

  m_publish = false;
}

SIOM::SimplifiedIOM::~SimplifiedIOM () noexcept
{
  this->cleanup ();
}

void SIOM::SimplifiedIOM::configure (const daq::rc::TransitionCmd &cmd)
{
  int nin = 0;
  int nout = 0;

  try
  {
    // Get configuration core parameters

    core_conf = new SIOM::SIOMCoreConfig (m_name);
    core_conf->get_config ();

    const siomdal::SIOMApplicationBase *appConfiguration = core_conf->get_appBase ();

    // Load plug-ins and get configuration

    for (auto i = core_conf->get_Input ().begin ();
	 i != core_conf->get_Input ().end (); ++i, ++nin)
    {
      std::string className = (*i)->class_name ();

      inputFactory.load (className);
      SIOM::SIOMInput *input = inputFactory.create (className.c_str ());
      input->setup (&m_memory_manager, core_conf, appConfiguration, (*i));

      inList.push_back (input);
      pluginList.push_back (input);
    }

    for (auto o = core_conf->get_Output ().begin ();
	 o != core_conf->get_Output ().end (); ++o, ++nout)
    {
      std::string className = (*o)->class_name ();

      outputFactory.load (className);
      SIOM::SIOMOutput *output = outputFactory.create (className.c_str ());
      output->setup (&m_memory_manager, core_conf, appConfiguration, (*o));

      outList.push_back (output);
      pluginList.push_back (output);
    }

    for (auto p = core_conf->get_Processors ().begin ();
	 p != core_conf->get_Processors ().end (); ++p)
    {
      std::string className = (*p)->class_name ();

      processorFactory.load (className);
      SIOM::SIOMProcessor *processor = processorFactory.create (className.c_str ());
      processor->setup (&m_memory_manager, core_conf, appConfiguration, (*p));

      procList.push_back (processor);
      pluginList.push_back (processor);
    }

    if (nin == 0)
    {
      CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			     SIOMCoreException::NO_INPUT_PLUGIN,
			     "");
      ers::fatal (e);
    }

    if (nout == 0)
    {
      CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			     SIOMCoreException::NO_OUTPUT_PLUGIN,
			     "");
      ers::fatal (e);
    }

    // Build IS parameters

    if (! m_interactiveMode)
    {
      std::string serverName = appConfiguration->get_ISServerName ();
      m_isInfoName = serverName.length () ? 
                     serverName + "." + m_name :
                     serverName;
    }

    // Configure plug-ins

    for (auto iplug = pluginList.begin (); iplug != pluginList.end (); ++iplug)
      (*iplug)->configure (cmd);
  }

  catch (std::exception& e)
  {
    ENCAPSULATE_SIOM_EXCEPTION (e2, SIOM::SIOMCoreException,
				UNCLASSIFIED, e, "Thread stopped");
    ers::error (e2);
    this->cleanup ();
  }
}

void SIOM::SimplifiedIOM::connect (const daq::rc::TransitionCmd &cmd)
{
  // Initialize counters

  this->initInfo ();

  for (auto iplug = pluginList.begin (); iplug != pluginList.end (); ++iplug)
    (*iplug)->connect (cmd);

  m_publish = true;
}

void SIOM::SimplifiedIOM::prepareForRun (const daq::rc::TransitionCmd &cmd)
{
  // Initialize timers

  gettimeofday (&m_probetime, (struct timezone *) NULL);
  gettimeofday (&m_starttime, (struct timezone *) NULL);

  // Initialize counters

  this->clearInfo ();

  for (auto iplug = pluginList.begin (); iplug != pluginList.end (); ++iplug)
    (*iplug)->prepareForRun (cmd);
}

void SIOM::SimplifiedIOM::stopDC (const daq::rc::TransitionCmd &cmd)
{
  this->publish ();

  for (auto iplug = pluginList.begin (); iplug != pluginList.end (); ++iplug)
    (*iplug)->stopDC (cmd);
}

void SIOM::SimplifiedIOM::disconnect (const daq::rc::TransitionCmd &cmd)
{
  m_publish = false;

  for (auto iplug = pluginList.begin (); iplug != pluginList.end (); ++iplug)
    (*iplug)->disconnect (cmd);
}

void SIOM::SimplifiedIOM::unconfigure (const daq::rc::TransitionCmd &cmd)
{
  for (auto iplug = pluginList.begin (); iplug != pluginList.end (); ++iplug)
  {
    (*iplug)->unconfigure (cmd);
    (*iplug)->unsetup ();
  }

  this->cleanup ();
}

void SIOM::SimplifiedIOM::initInfo ()
{
  FILE* file;
  struct tms timeSample;
  char line[128];

  m_lastCPU = times(&timeSample);
  m_lastSysCPU = timeSample.tms_stime;
  m_lastUserCPU = timeSample.tms_utime;

  file = fopen("/proc/cpuinfo", "r");

  m_numProcessors = 0;
  while (fgets (line, 128, file) != NULL)
  {
    if (strncmp (line, "processor", 9) == 0) m_numProcessors++;
  }

  fclose (file);

  this->clearInfo ();
}

void SIOM::SimplifiedIOM::clearInfo ()
{
  m_old_ipack = 0, m_old_opack = 0;
  m_old_idata = 0, m_old_odata = 0;
}

void SIOM::SimplifiedIOM::publish ()
{
  uint64_t ipack = 0, opack = 0;
  double imbytes = 0, ombytes = 0;
  double dtstart, dt;
  struct timeval stattime;

  const IPCPartition *ipcPartition;

  if (! m_publish)
    return;

  if ((ipcPartition = core_conf->get_partition ()))
  {
    if (! m_interactiveMode && m_isInfoName.length () != 0)
    {
      // Publish specific plugin information

      try
      {
        for (auto iplug = pluginList.begin (); iplug != pluginList.end (); ++iplug)
        {
	  ISInfo *isInfo = (*iplug)->getISInfo ();

	  if (isInfo)
	    this->publishSpecificInfo ((*iplug)->getName (), ipcPartition, isInfo);
        }
      }

      catch (daq::is::Exception &exc)
      {
	ers::warning (exc);
      }
    }

    gettimeofday (&stattime, (struct timezone *) NULL);

    dt = (float) (stattime.tv_sec - m_probetime.tv_sec) +
         (float) (stattime.tv_usec - m_probetime.tv_usec) / SEC_TO_USEC;
    dtstart = (float) (stattime.tv_sec - m_starttime.tv_sec) +
              (float) (stattime.tv_usec - m_starttime.tv_usec) / SEC_TO_USEC;

    memcpy (&m_probetime, &stattime, sizeof (struct timeval));

    std::string coreInfoName = m_isInfoName + ".core";
    siom_info::siomCoreNamed siomInfo (*ipcPartition, coreInfoName);

    for (auto iin = inList.begin (); iin != inList.end (); ++iin)
    {
      ipack += (*iin)->packets ();
      imbytes += (*iin)->mbytes ();
    }

    for (auto iout = outList.begin (); iout != outList.end (); ++iout)
    {
      opack += (*iout)->packets ();
      ombytes += (*iout)->mbytes ();
    }

    siomInfo.ActiveInputPlugin         = inList.size ();
    siomInfo.ActiveOutputPlugin        = outList.size ();
    siomInfo.ActiveProcessors          = procList.size ();

    siomInfo.InputPackets              = ipack;
    siomInfo.InputMB                   = imbytes;
    siomInfo.InputRate                 = dt > 0 ? ((ipack - m_old_ipack) / dt) : 0;
    siomInfo.InputDataRate             = dt > 0 ? ((imbytes - m_old_idata) / dt) : 0;
    siomInfo.AverageInputRate          = dtstart > 0 ? (ipack / dtstart) : 0;
    siomInfo.AverageInputDataRate      = dtstart > 0 ? (imbytes / dtstart) : 0;

    siomInfo.OutputPackets             = opack;
    siomInfo.OutputMB                  = ombytes;
    siomInfo.OutputRate                = dt > 0 ? ((opack - m_old_opack) / dt) : 0;
    siomInfo.OutputDataRate            = dt > 0 ? ((ombytes - m_old_odata) / dt) : 0;
    siomInfo.AverageOutputRate         = dtstart > 0 ? (opack / dtstart) : 0;
    siomInfo.AverageOutputDataRate     = dtstart > 0 ? (ombytes / dtstart) : 0;

    struct tms timeSample;
    clock_t now = times (&timeSample);

    if (now <= m_lastCPU || timeSample.tms_stime < m_lastSysCPU ||
	timeSample.tms_utime < m_lastUserCPU)
    {
      //Overflow detection. Just skip this value.

      siomInfo.UserProcessor = 0;
      siomInfo.SystemProcessor = 0;
    }
    else
    {
      float norm = (now - m_lastCPU) * m_numProcessors;
      siomInfo.UserProcessor = (timeSample.tms_utime - m_lastUserCPU) / norm;
      siomInfo.SystemProcessor = (timeSample.tms_stime - m_lastSysCPU) / norm;
    }

    m_lastCPU = now;
    m_lastSysCPU = timeSample.tms_stime;
    m_lastUserCPU = timeSample.tms_utime;

    m_old_ipack = ipack;
    m_old_idata = imbytes;
    m_old_opack = opack;
    m_old_odata = ombytes;

    // Publish core info

    if (m_interactiveMode || m_isInfoName.length () == 0)
    {
      std::cout << siomInfo << std::endl;
    }
    else
    {
      try
      {
	siomInfo.checkin ();
      }

      catch (daq::is::Exception &exc)
      {
	ers::warning (exc);
      }
    }
  }
}

void SIOM::SimplifiedIOM::publishSpecificInfo (const std::string plugName,
					       const IPCPartition *ipcPartition,
					       ISInfo *info)
{
  if (m_interactiveMode)
  {
    std::cout << "\n" << plugName;
    std::cout << " info:\n" << *info << std::endl;
  }
  else
  {
    std::string itemNameStream;
    itemNameStream = m_isInfoName + "." + plugName ;

    ISInfoDictionary dictionary (*ipcPartition);

    // Keep track of what we've put in so we can clean up later

    if (m_isItems.find (itemNameStream) == m_isItems.end ())
    {
      m_isItems.insert (itemNameStream);

      try
      {
	dictionary.insert (itemNameStream, *info);
      }

      catch (daq::is::InfoAlreadyExist& exc)
      {
        // Ignore error at this stage.

	try
	{
	  dictionary.update (itemNameStream, *info);
	}

	catch (daq::is::Exception& exc)
	{
	  // Issue a warning and carry on

	  ers::warning (exc);
	}
      }

      catch (daq::is::Exception& exc)
      {
	// Issue a warning and carry on

	ers::warning (exc);
      }
    }
    else
    {
      try
      {
	dictionary.update (itemNameStream, *info);
      }

      catch (daq::is::InfoNotFound& exc1)
      {
	try
	{
	  dictionary.insert (itemNameStream, *info);
	}

	catch (daq::is::Exception& exc2)
	{
	  // Issue a warning and carry on

	  ers::warning (exc2);
	}
      }

      catch (daq::is::Exception& exc2)
      {
	// Issue a warning and carry on

	ers::warning (exc2);
      }
    }
  }
}

void SIOM::SimplifiedIOM::cleanup ()
{
  for (auto iplug = pluginList.begin (); iplug != pluginList.end (); ++iplug)
    delete (*iplug);

  inList.clear ();
  outList.clear ();
  procList.clear ();
  pluginList.clear ();

  if (core_conf)
  {
    if (! m_interactiveMode)
      this->isCleanup ();

    delete core_conf;
    core_conf = 0;
  }
}

void SIOM::SimplifiedIOM::isCleanup ()
{
  ISInfoDictionary dictionary (*(core_conf->get_partition ()));

  // Remove my named info

  try
  {
    std::string coreInfoName = m_isInfoName + ".core";
    dictionary.remove (coreInfoName);
  }

  catch (daq::is::Exception& exc)
  {
    // Ignore error at this stage.  It may be that we have never published
    // anything since configure or somebody removed the item behind our back.
  }

  // Remove the plugin info items

  for (auto iter = m_isItems.begin (); iter != m_isItems.end (); iter++)
  {
    try
    {
      dictionary.remove ((*iter).c_str ());
    }

    catch (daq::is::Exception& exc)
    {
      // Ignore error at this stage.
      // It may be that somebody removed the item behind our back.
    }
  }

  m_isItems.clear ();
}
