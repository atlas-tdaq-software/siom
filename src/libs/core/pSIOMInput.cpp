#include <iostream>
#include <sstream>

#include <map>
#include <vector>
#include <string>

// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"
#include "DFdal/MemoryPool.h"

// Package include files

#include "siomdal/SIOMInput.h"

#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMErrorReporting.h"
#include "siom/core/SIOMCoreException.h"
#include "siom/core/pSIOMInput.h"

SIOM::SIOMInput::SIOMInput ()
  : m_dbConfig (false), m_server (0)
{
}

SIOM::SIOMInput::~SIOMInput () noexcept
{
  this->disconnect ();
  this->unconfigure ();
  this->plugin_unsetup ();
}

void SIOM::SIOMInput::plugin_setup (SIOM::MemoryManager <uint8_t> *memory_manager,
				    SIOM::SIOMCoreConfig *core_conf,
				    const DalObject *appConfiguration,
				    const DalObject *objConfiguration, bool debug)
{
  m_memory_manager = memory_manager;

  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMInput *conf =
    confDB->cast <siomdal::SIOMInput> (objConfiguration);

  m_dbConfig = true;
  m_maxthreads = conf->get_MaxThreads ();

  if (debug)
  {
    std::cout << "Max input threads " << m_maxthreads << std::endl;
  }

  if (conf->get_MemoryPools ().size () == 0)
  {
    CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			   SIOMCoreException::NO_INTERNAL_POOL,
			   "SIOMInput plugin: no internal buffer");
    ers::fatal (e);
    throw e;
  }

  for (auto i = conf->get_MemoryPools ().begin ();
       i != conf->get_MemoryPools ().end (); ++i)
  {
    std::string poolName = (*i)->UID ();

    size_t poolPages = (*i)->get_NumberOfPages ();
    size_t poolPageSize = (*i)->get_PageSize ();

    if (debug)
    {
      std::cout << "Input pool name " << poolName << std::endl;
      std::cout << "Input pool pages " << poolPages << std::endl;
      std::cout << "Input pool page size " << poolPageSize << std::endl;
    }

    SIOM::MemoryPool <uint8_t> *mPool = m_memory_manager->create_pool (poolName, poolPageSize, poolPages);

    m_poolNames.push_back (poolName);
    m_poolVector.push_back (mPool); 
  }
}

void SIOM::SIOMInput::configure ()
{
  m_server = this->createServer ();
  m_server->clearInfo ();
}

void SIOM::SIOMInput::prepareForRun ()
{
  m_server->clearInfo ();
  m_server->startOfRun ();
}

void SIOM::SIOMInput::stopDC ()
{
  m_server->endOfRun ();
}

void SIOM::SIOMInput::unconfigure ()
{
  if (m_server)
  {
    this->deleteServer (m_server);
    m_server = 0;
  }
}

void SIOM::SIOMInput::plugin_unsetup ()
{
  if (m_dbConfig)
  {
    // Close buffers and clean up the descriptor list

    for (auto it = m_poolNames.begin (); it != m_poolNames.end (); ++it)
      m_memory_manager->delete_pool (*it);

    m_poolNames.clear ();
    m_poolVector.clear ();
  }
}
