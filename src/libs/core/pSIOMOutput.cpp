#include <iostream>
#include <sstream>

#include <map>
#include <vector>
#include <string>

// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"
#include "DFdal/MemoryPool.h"

// Package include files

#include "siomdal/SIOMOutput.h"

#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataSender.h"
#include "siom/core/SIOMErrorReporting.h"
#include "siom/core/SIOMCoreException.h"
#include "siom/core/pSIOMOutput.h"

SIOM::SIOMOutput::SIOMOutput ()
  : m_dbConfig (false), m_sender (0)
{
}

SIOM::SIOMOutput::~SIOMOutput () noexcept
{
  this->disconnect ();
  this->unconfigure ();
  this->plugin_unsetup ();
}

void SIOM::SIOMOutput::plugin_setup (SIOM::MemoryManager <uint8_t> *memory_manager,
				     SIOM::SIOMCoreConfig *core_conf,
				     const DalObject *appConfiguration,
				     const DalObject *objConfiguration, bool debug)
{
  m_memory_manager = memory_manager;

  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMOutput *conf =
    confDB->cast<siomdal::SIOMOutput> (objConfiguration);

  m_dbConfig = true;

  m_minBufElements = conf->get_MinBufElem ();
  m_pollRateFactor = conf->get_PollRateFactor ();

  if (debug)
  {
    std::cout << "Min buf elements " << m_minBufElements << std::endl;
    std::cout << "Poll rate factor " << m_pollRateFactor << std::endl;
  }

  if (conf->get_MemoryPools ().size () == 0)
  {
    CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			   SIOMCoreException::NO_INTERNAL_POOL,
			   "SIOMOutput plugin: no internal buffer");
    ers::fatal (e);
    throw e;
  }

  for (auto i = conf->get_MemoryPools ().begin ();
       i != conf->get_MemoryPools ().end (); ++i)
  {
    std::string poolName = (*i)->UID ();

    size_t poolPages = (*i)->get_NumberOfPages ();
    size_t poolPageSize = (*i)->get_PageSize ();

    if (debug)
    {
      std::cout << "Output pool name " << poolName << std::endl;
      std::cout << "Output pool pages " << poolPages << std::endl;
      std::cout << "Output pool page size " << poolPageSize << std::endl;
    }

    SIOM::MemoryPool <uint8_t> *mPool = m_memory_manager->create_pool (poolName, poolPageSize, poolPages);

    m_poolNames.push_back (poolName);
    m_poolVector.push_back (mPool);
  }
}

void SIOM::SIOMOutput::configure ()
{
}

void SIOM::SIOMOutput::connect ()
{
  m_sender = this->createSender ();
  m_sender->clearInfo ();
  m_sender->pollFactor (m_pollRateFactor);
}

void SIOM::SIOMOutput::prepareForRun ()
{
  m_sender->clearInfo ();
  m_sender->startOfRun ();
  m_sender->minElements (m_minBufElements);
}

void SIOM::SIOMOutput::stopDC ()
{
  m_sender->minElements (0);
  m_sender->endOfRun ();
}

void SIOM::SIOMOutput::disconnect ()
{
  if (m_sender)
  {
    this->deleteSender (m_sender);
    m_sender = 0;
  }
}

void SIOM::SIOMOutput::unconfigure ()
{
}

void SIOM::SIOMOutput::plugin_unsetup ()
{
  if (m_dbConfig)
  {
    // Close buffers and clean up the descriptor list

    for (auto it = m_poolNames.begin (); it != m_poolNames.end (); ++it)
      m_memory_manager->delete_pool (*it);

    m_poolNames.clear ();
    m_poolVector.clear ();
  }
}
