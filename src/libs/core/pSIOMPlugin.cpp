// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siomdal/SIOMPlugin.h"

#include "siom/core/SIOMCoreConfig.h"
#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/pSIOMPlugin.h"

void SIOM::SIOMPlugin::setup (SIOM::MemoryManager <uint8_t> *memory_manager,
			      SIOM::SIOMCoreConfig *core_conf,
			      const DalObject *appConfiguration,
			      const DalObject *objConfiguration, bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMPlugin *conf =
    confDB->cast <siomdal::SIOMPlugin> (objConfiguration);

  this->setName (conf->UID ());

  if (debug)
  {
    std::cout << "Name " << conf->class_name () << std::endl;
    std::cout << "UID " << conf->UID () << std::endl;
  }

  this->plugin_setup (memory_manager, core_conf, appConfiguration, objConfiguration, debug);
  this->specific_setup (core_conf, appConfiguration, objConfiguration, debug);
}

void SIOM::SIOMPlugin::unsetup ()
{
  this->specific_unsetup ();
  this->plugin_unsetup ();
}
