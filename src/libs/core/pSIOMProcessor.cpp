#include <iostream>
#include <sstream>

#include <map>
#include <vector>
#include <string>

// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"
#include "DFdal/MemoryPool.h"

// Package include files

#include "siomdal/SIOMProcessor.h"

#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataProcessor.h"
#include "siom/core/SIOMErrorReporting.h"
#include "siom/core/SIOMCoreException.h"
#include "siom/core/pSIOMProcessor.h"

SIOM::SIOMProcessor::SIOMProcessor ()
  : m_dbConfig (false), m_dataProcessor (0)
{
}

SIOM::SIOMProcessor::~SIOMProcessor () noexcept
{
  this->disconnect ();
  this->unconfigure ();
  this->plugin_unsetup ();
}

void SIOM::SIOMProcessor::plugin_setup (SIOM::MemoryManager <uint8_t> *memory_manager,
					SIOM::SIOMCoreConfig *core_conf,
					const DalObject *appConfiguration,
					const DalObject *objConfiguration, bool debug)
{
  m_memory_manager = memory_manager;

  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMProcessor *conf =
    confDB->cast<siomdal::SIOMProcessor> (objConfiguration);

  m_dbConfig = true;

  m_nthreads = conf->get_ThreadNumber ();
  m_minBufElements = conf->get_MinBufElem ();
  m_pollRateFactor = conf->get_PollRateFactor ();

  if (debug)
  {
    std::cout << "Processing threads " << m_nthreads << std::endl;
    std::cout << "Min buf elements " << m_minBufElements << std::endl;
    std::cout << "Poll rate factor " << m_pollRateFactor << std::endl;
  }

  if (conf->get_InputMemoryPools ().size () == 0 ||
      conf->get_OutputMemoryPools ().size () == 0)
  {
    CREATE_SIOM_EXCEPTION (e, SIOMCoreException,
			   SIOMCoreException::NO_INTERNAL_POOL,
			   "SIOMProcessor plugin: no internal buffer");
    ers::fatal (e);
    throw e;
  }

  for (auto i = conf->get_InputMemoryPools ().begin ();
       i != conf->get_InputMemoryPools ().end (); ++i)
  {
    std::string poolName = (*i)->UID ();

    int poolPages = (*i)->get_NumberOfPages ();
    int poolPageSize = (*i)->get_PageSize ();

    if (debug)
    {
      std::cout << "Processor input pool name " << poolName << std::endl;
      std::cout << "Processor input pool pages " << poolPages << std::endl;
      std::cout << "Processor input pool page size " << poolPageSize << std::endl;
    }

    SIOM::MemoryPool <uint8_t> *mPool = m_memory_manager->create_pool (poolName, poolPageSize, poolPages);

    m_poolInputNames.push_back (poolName); 
    m_poolInputVector.push_back (mPool); 
  }

  for (auto i = conf->get_OutputMemoryPools ().begin ();
       i != conf->get_OutputMemoryPools ().end (); ++i)
  {
    std::string poolName = (*i)->UID ();

    int poolPages = (*i)->get_NumberOfPages ();
    int poolPageSize = (*i)->get_PageSize ();

    if (debug)
    {
      std::cout << "Processor output pool name " << poolName << std::endl;
      std::cout << "Processor output pool pages " << poolPages << std::endl;
      std::cout << "Processor output pool page size " << poolPageSize << std::endl;
    }

    SIOM::MemoryPool <uint8_t> *mPool = m_memory_manager->create_pool (poolName, poolPageSize, poolPages);

    m_poolOutputNames.push_back (poolName); 
    m_poolOutputVector.push_back (mPool); 
  }
}

void SIOM::SIOMProcessor::configure ()
{
  m_dataProcessor = this->createProcessor ();
  m_dataProcessor->clearInfo ();
  m_dataProcessor->pollFactor (m_pollRateFactor);
}

void SIOM::SIOMProcessor::prepareForRun ()
{
  m_dataProcessor->clearInfo ();
  m_dataProcessor->minElements (m_minBufElements);
}

void SIOM::SIOMProcessor::stopDC ()
{
  m_dataProcessor->minElements (0);
}

void SIOM::SIOMProcessor::unconfigure ()
{
  if (m_dataProcessor)
  {
    this->deleteProcessor (m_dataProcessor);
    m_dataProcessor = 0;
  }
}

void SIOM::SIOMProcessor::plugin_unsetup ()
{
  if (m_dbConfig)
  {
    // Close buffers and clean up the descriptor list

    for (auto it = m_poolInputNames.begin (); it != m_poolInputNames.end (); ++it)
      m_memory_manager->delete_pool (*it);

    for (auto it = m_poolOutputNames.begin (); it != m_poolOutputNames.end (); ++it)
      m_memory_manager->delete_pool (*it);

    m_poolInputNames.clear ();
    m_poolInputVector.clear ();
    m_poolOutputNames.clear ();
    m_poolOutputVector.clear ();
  }
}
