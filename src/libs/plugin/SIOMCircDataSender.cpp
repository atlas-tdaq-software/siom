#include <vector>

// TDAQ include files

#include "circ/Circ.h"
#include "circ/Circservice.h"

// Package include files

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"
#include "siom/core/SIOMErrorReporting.h"
#include "siom/core/SIOMCoreException.h"

#include "siom/plugin/SIOMPluginException.h"
#include "siom/plugin/SIOMCircDataSender.h"
#include "siom/plugin/SIOMCircServerAddress.h"

SIOM::CircDataSender::CircDataSender (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
				      SIOM::DataDestination *dest)
  : SIOM::DataSender (mpool, dest), _circ (-1)
{
  // Server and buffer parameters

  auto destination_address = reinterpret_cast <SIOM::CircServerAddress *> (dest->address ());

  _bufname = destination_address->bufname ();
  _bufsize = destination_address->bufsize ();
  _port = destination_address->port ();

  // Connect to server.

  _circ = CircOpenCircConnection (_port, const_cast <char *> (_bufname.c_str ()), _bufsize);

  // Send exception in case of error

  if (_circ < 0)
  {
    CREATE_SIOM_EXCEPTION (e, SIOMPluginException,
			   SIOMPluginException::NO_BUFFER_CONNECTION,
			   "SIOMCircDataSender: Error in connecting to Circserver");

    ers::fatal (e);
    throw (e);
  }

  // Start the sender thread

  this->start ();
}

int SIOM::CircDataSender::send (SIOM::MemoryBuffer <uint8_t> *b)
{
  char *ptr;
  size_t datalen;
  static int evnumber = 0;

  size_t len = datalen = b->n_objects () * sizeof (uint8_t);

  // reserve buffer memory

  if ((ptr = CircReserve (_circ, evnumber, datalen)) != (char *) -1)
  {
    // copy data

    char *p = ptr;
    size_t page_size = b->page_size ();

    auto pages = b->get_pages ();

    for (auto page = pages.begin (); page != pages.end (); ++page)
    {
      size_t l = len > page_size ? page_size : len;

      if (l > 0)
      {
        memcpy (p, *page, l);
        p += l;
        len -= l;
      }
    }

    (void) CircValidate (_circ, evnumber, ptr, datalen);
    ++evnumber;
  }
  else
  {
    // no back-pressure, event is ignored

    return 0;
  }

  return datalen;
}
