#include <list>
#include <vector>
#include <mutex>

#include <time.h>
#include <sys/time.h>
#include <sys/types.h>

#include "circ/Error.h"
#include "circ/Circservice.h"

#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMErrorReporting.h"
#include "siom/core/SIOMCoreException.h"

#include "siom/plugin/SIOMPluginException.h"
#include "siom/plugin/SIOMCircServer.h"
#include "siom/plugin/SIOMCircServerThread.h"

#include "siom_info/siomCircInput.h"

#define SECTOUSEC                   1000000
#define GARBAGE_COLLECTOR_TIME      30.

SIOM::CircServer::CircServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
			      unsigned short maxthreads,
			      unsigned short port,
			      unsigned int pollfactor,
			      bool pack_events)
  : InputServer (mpool, maxthreads),
    _port (port), _pollRateFactor (pollfactor), _pack (pack_events)
{
  // Init timer

  gettimeofday (&_t0, 0);

  // Start server. Exceptions are not caught to be sent to the upper level.

  if (CircServerInit (_port) != Error_NO)
  {
    CREATE_SIOM_EXCEPTION (e, SIOMPluginException,
			   SIOMPluginException::NO_BUFFER_CONNECTION,
			   "SIOMCircServer: Error in initializing server");

    ers::fatal (e);
    throw (e);
  }

  // Init counters

  _nthreads = 0;
  _active_pools = _mempools.size ();

  this->start ();
}

SIOM::CircServer::~CircServer ()
{
  this->join ();

  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    delete (*i);

  for (auto imutex = _global_mutex.begin (); imutex != _global_mutex.end (); ++imutex)
    delete (*imutex);

  _threads.clear ();
  _nthreads = 0;

  _global_mutex.clear ();

  CircServerEnd ();
}

void SIOM::CircServer::run ()
{
  time_t t, t0;
  static double long_interval = GARBAGE_COLLECTOR_TIME;

  auto i = _mempools.begin ();

  time (&t0);

  while (! _quit.load ())
  {
    int fd, cid;
    struct circserver_request req;

    if ((fd = CircServerGetRequest (&req)) > 0)
    {
      if (req.reqtype == CIRC_SERVER_REQTYPE_CLOSE)
      {
	if ((cid = CircServerGetCid (req.circname)) >= 0)
	{
	  for (auto is = _threads.begin (); is != _threads.end (); ++is)
	    (*is)->remove (cid);
	}
      }

      if ((cid = CircServerExecuteRequest (fd, &req)) >= 0)
      {
	if (req.reqtype == CIRC_SERVER_REQTYPE_CONNECT)
	{
	  // Does a thread already exist for this cid?

	  bool found = false;

	  for (auto is = _threads.begin (); is != _threads.end (); ++is)
	  {
	    auto clist = (*is)->active_list ();

	    for (auto icid = clist.begin (); icid != clist.end (); ++icid)
	      if (*icid == cid)
		found = true;
	  }

	  if (!found)
	    req.reqtype = CIRC_SERVER_REQTYPE_OPEN;
	}

	if (req.reqtype == CIRC_SERVER_REQTYPE_OPEN)
	{
	  // Start thread - connection assignement

	  if (_maxthreads == 0 || _nthreads < _maxthreads)
	  {
	    // Create a new "global" mutex for this thread

	    std::mutex *glob_m = new std::mutex ();
	    _global_mutex.insert (glob_m);

	    auto thr = new SIOM::CircServerThread ((*i), glob_m,
						   _pollRateFactor, _pack);

	    thr->add (cid);

	    // Add thread to thread list

	    _threads.insert (thr);
	    ++_nthreads;

	    // Increment pool index

	    if (++i == _mempools.end ())
	      i = _mempools.begin ();
	  }
	  else
	  {
	    // Add link to less loaded thread

	    unsigned int n, min;
	    std::set <SIOM::CircServerThread *>::iterator is, js;

	    for (is = _threads.begin (), min = 0xffff; is != _threads.end (); ++is)
	      if ((n = (*is)->ncirc ()) < min)
	      {
		js = is;
		min = n;
	      }

	    (*js)->add (cid);
	  }
	}
      }
    }

    if (_quit.load ())
      break;

    // Garbage collection: clean-up of orphaned buffers

    time (&t);

    if (difftime (t, t0) > long_interval)
    {
      for (auto imutex = _global_mutex.begin (); imutex != _global_mutex.end (); ++imutex)
	(*imutex)->lock ();

      int ndel = CircServerGarbageCollector ();

      if (ndel)
      {
	int nc = 0, cidlen = 0;
	int *cids = (int *) NULL;

	// Get the list of active buffers

	nc = CircServerGetCircList (&cids, &cidlen);

	// Check which one has been deleted in each thread (and remove it from thread's internal list)

	for (auto is = _threads.begin (); is != _threads.end (); ++is)
	{
	  std::list <int> removed_cids;
	  std::list <int> thread_cids = (*is)->active_list ();

	  for (auto j_thread_cid = thread_cids.begin (); j_thread_cid != thread_cids.end (); ++j_thread_cid)
	  {
	    int i;
	    bool found = false;
	    int local_cid = *j_thread_cid;

	    for (i = 0; i < nc; ++i)
	      if (cids[i] == local_cid)
	      {
		found = true;
		break;
	      }

	    if (! found)
	      removed_cids.push_back (local_cid);
	  }

	  for (auto j_rem_cid = removed_cids.begin (); j_rem_cid != removed_cids.end (); ++j_rem_cid)
	    (*is)->remove ((*j_rem_cid));
	}

	if (cids)
	  free (cids);
      }

      for (auto imutex = _global_mutex.begin (); imutex != _global_mutex.end (); ++imutex)
	(*imutex)->unlock ();

      time (&t0);
    }

    // Take some time between cycles

    std::this_thread::sleep_for (std::chrono::milliseconds (100));
  }
}

void SIOM::CircServer::clearInfo ()
{
  // Reset timers and counters

  gettimeofday (&_t0, 0);

  for (auto it = _threads.begin (); it != _threads.end (); ++it)
    (*it)->reset_counters ();
}

ISInfo *SIOM::CircServer::getISInfo ()
{
  unsigned int i = 0;
  float dtime;
  struct timeval t;

  gettimeofday (&t, 0);
  dtime = (float) (t.tv_sec - _t0.tv_sec) + (float) (t.tv_usec - _t0.tv_usec) / SECTOUSEC;

  _info.ActiveThreads = _nthreads;
  _info.ActiveMemoryPools = _active_pools;

  _info.ActivePoolSize.clear ();
  _info.ActivePoolPages.clear ();
  _info.ActivePoolPageSize.clear ();
  _info.ActivePoolOccupancy.clear ();

  for (auto ipool = _mempools.begin (); ipool != _mempools.end () && i < _active_pools; ++ipool, ++i)
  {
    _info.ActivePoolSize.push_back ((float) ((*ipool)->page_size () *
					     (*ipool)->page_number ()) / (1024 * 1024));
    _info.ActivePoolPages.push_back ((*ipool)->page_number ());
    _info.ActivePoolPageSize.push_back ((*ipool)->page_size ());
    _info.ActivePoolOccupancy.push_back ((float) (*ipool)->used_pages () /
					 (float) (*ipool)->page_number ());
  }

  uint64_t old_packets = _info.ProcessedPackets;
  double old_data = _info.ProcessedData;

  _info.ProcessedPackets = 0;
  _info.ProcessedData = 0;
  _info.PacketsThrown = 0;
  _info.ActiveInputBuffers = 0;

  _info.ActiveInputBufferSize.clear ();
  _info.ActiveInputBufferOccupancy.clear ();

  if (!_quit.load ())
  {
    for (auto imutex = _global_mutex.begin (); imutex != _global_mutex.end (); ++imutex)
      (*imutex)->lock ();

    for (auto it = _threads.begin (); it != _threads.end (); ++it)
    {
      _info.ProcessedPackets += (*it)->packets ();
      _info.ProcessedData += (*it)->Mbytes ();
      _info.PacketsThrown += (*it)->thrown ();
      _info.ActiveInputBuffers += (*it)->ncirc ();

      auto cidlist = (*it)->active_list ();

      for (auto icid = cidlist.begin (); icid != cidlist.end (); ++icid)
      {
	int length = CircGetSize ((*icid));
	float occ = (float) CircGetOccupancy ((*icid)) / (float) length;

	_info.ActiveInputBufferSize.push_back (length);
	_info.ActiveInputBufferOccupancy.push_back (occ);
      }
    }

    for (auto imutex = _global_mutex.begin (); imutex != _global_mutex.end (); ++imutex)
      (*imutex)->unlock ();
  }

  if (old_packets > _info.ProcessedPackets)
    old_packets = 0;

  if (old_data > _info.ProcessedData)
    old_data = 0;

  _info.PacketRate = dtime > 0 ?
    (float) (_info.ProcessedPackets - old_packets) / dtime : 0;
  _info.DataRate = dtime > 0 ?
    (_info.ProcessedData - old_data) / dtime : 0;

  memcpy (&_t0, &t, sizeof (struct timeval));

  return &_info;
}
