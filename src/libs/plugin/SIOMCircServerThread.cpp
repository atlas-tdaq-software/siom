
#include <list>
#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>

#include "circ/Circservice.h"
#include "circ/Circ_params.h"

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"

#include "siom/plugin/SIOMCircServerThread.h"

SIOM::CircServerThread::CircServerThread (SIOM::MemoryPool <uint8_t> *memPool,
                                          std::mutex *m, unsigned int pfactor,
					  bool pack)
  : _pool (memPool), _global_mutex (m), _pollRateFactor (pfactor), _pack (pack)
{
  _ncirc = 0;

  this->reset_counters ();
  this->start ();
}

SIOM::CircServerThread::~CircServerThread ()
{
  _internal_mutex.lock ();
  _circlist.clear ();
  _ncirc = 0;
  _internal_mutex.unlock ();

  this->join ();
}

void SIOM::CircServerThread::add (int cid)
{
  std::lock_guard <std::mutex> lock (_internal_mutex);

  _circlist.push_back (cid);
  ++_ncirc;
}

void SIOM::CircServerThread::remove (int cid)
{
  std::lock_guard <std::mutex> lock (_internal_mutex);

  for (auto ibuf = _circlist.begin (); ibuf != _circlist.end (); ++ibuf)
    if ((*ibuf) == cid)
    {
      _circlist.remove (cid);
      --_ncirc;
      break;
    }
}

void SIOM::CircServerThread::run ()
{
  time_t t0, t1;
  int nwritten = 0;
  double invrate = s_circthr_initial_invrate;

  SIOM::MemoryBuffer <uint8_t> *bpointer = 0;
  int page_size = _pool->page_size () * sizeof (uint8_t);

  time (&t0);

  while (! _quit.load ())
  {
    double sec = 0;
    bool found;

    do
    {
      found = false;

      std::this_thread::yield ();
      std::scoped_lock lock (*_global_mutex, _internal_mutex);

      for (auto icirc = _circlist.begin (); icirc != _circlist.end (); ++icirc)
      {
	char *ptr;
	int number, evtsize;
	int cid = *icirc;

	if ((ptr = CircLocate (cid, &number, &evtsize)) != (char *) -1)
	{
	  found = true;

	  if (_pack)
	  {
	    // If data do not fit into a page, discard and clean-up
	    // input buffers

	    if (evtsize > page_size)
	    {
	      CircRelease (cid);
	      ++_nthrown;
	      continue;
	    }

	    if (evtsize + nwritten > page_size)
	    {
	      // Validate the current buffer if already allocated

	      if (bpointer)
		_pool->validate (bpointer, nwritten);

	      // Reserve a new buffer (single page)
	      // and set current pointer

	      if (! (bpointer = _pool->reserve (page_size)))
	      {
		CircRevalidate (cid);
		continue;
	      }

	      nwritten = 0;
	    }

	    bpointer->append (reinterpret_cast <uint8_t *> (ptr), evtsize);
	    nwritten += evtsize;
	  }
	  else
	  {
	    if (! (bpointer = _pool->reserve (evtsize)))
	    {
	      CircRevalidate (cid);
	      continue;
	    }

	    bpointer->append (reinterpret_cast <uint8_t *> (ptr), evtsize);
	    _pool->validate (bpointer, evtsize);
	  }

	  CircRelease (cid);

	  ++_npack;
	  ++_ndelta;
	  _Mbytes += ((double) evtsize / MBYTE);
	}
	else
	{
	  found = false;
	  break;
	}
      }
    } while (found);

    time (&t1);
    if ((t1 - t0) > DELTA)
    {
      invrate = _ndelta ? ((double) (t1 - t0) / (double) _ndelta)
	                : s_circthr_initial_invrate;
      _ndelta = 0;
      time (&t0);
    }

    // Check the rate and calculate waiting time

    sec = invrate * _pollRateFactor;

    if (sec > 1)
      std::this_thread::sleep_for (std::chrono::seconds (1));
    else
    {
      long int nsec = (long int) (sec * SECTONSEC);

      if (nsec > MINSLEEP)
	std::this_thread::sleep_for (std::chrono::nanoseconds (nsec));
    }

    // Release the scheduler

    std::this_thread::yield ();
  }
}
