#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/plugin/SIOMDummyDataProcessor.h"

unsigned int SIOM::DummyDataProcessor::process_data (SIOM::MemoryBuffer <uint8_t> *input,
						     SIOM::MemoryBuffer <uint8_t> *output)
{
  size_t to_copy = input->n_objects ();

  if (to_copy > output->size ())
    return 0;

  auto in_pages = input->get_pages ();

  for (auto pagein = in_pages.begin (); pagein != in_pages.end () && to_copy; ++pagein)
  {
    size_t n = to_copy > input->page_size () ? input->page_size () : to_copy;

    output->append (*pagein, n);
    to_copy -= n;
  }

  return output->n_objects ();
}
