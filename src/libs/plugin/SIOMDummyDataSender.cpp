// Package include files

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMDummyDataSender.h"

int SIOM::DummyDataSender::send (SIOM::MemoryBuffer <uint8_t> *b)
{
  size_t datalen = b->n_objects () * sizeof (uint8_t);

  return datalen;
}
