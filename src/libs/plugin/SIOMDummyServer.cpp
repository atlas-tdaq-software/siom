#include <set>
#include <vector>
#include <chrono>

#include <sys/time.h>

#include "siom/core/SIOMMemoryPool.h"
#include "siom/plugin/SIOMDummyServer.h"
#include "siom/plugin/SIOMDummyServerThread.h"

#include "siom_info/siomDummyInput.h"

#define SECTOUSEC 1000000

SIOM::DummyServer::DummyServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
				unsigned short maxthreads)
  : InputServer (mpool, maxthreads), _active_pools (0)
{
  // Init timer

  gettimeofday (&_t0, 0);

  // Start server threads

  this->startThreads ();

  // Start running

  this->start ();
}

SIOM::DummyServer::~DummyServer ()
{
  this->join ();
 
 // Close connections and stop server threads

  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    delete (*i);

  _threads.clear ();
}

void SIOM::DummyServer::startThreads ()
{
  bool add_active = true;

  // Create threads

  auto ipool = _mempools.begin ();

  for (unsigned int i = 0; i < _maxthreads; ++i)
  {
    auto thr = new SIOM::DummyServerThread (*ipool);

    // Add thread to thread list

    _threads.insert (thr);

    if (add_active)
      ++_active_pools;

    if (++ipool == _mempools.end ())
    {
      ipool = _mempools.begin ();
      add_active = false;
    }
  }
}

void SIOM::DummyServer::run ()
{
  while (! _quit.load ())
    std::this_thread::sleep_for (std::chrono::seconds (1));
}

void SIOM::DummyServer::clearInfo ()
{
  // Reset timers and counters

  gettimeofday (&_t0, 0);

  for (auto it = _threads.begin (); it != _threads.end (); ++it)
    (*it)->reset_counters ();
}

ISInfo *SIOM::DummyServer::getISInfo ()
{
  unsigned int i = 0;
  float dtime;
  struct timeval t;

  gettimeofday (&t, 0);
  dtime = (float) (t.tv_sec - _t0.tv_sec) + (float) (t.tv_usec - _t0.tv_usec) / SECTOUSEC;

  _info.ActiveThreads = _threads.size ();
  _info.ActiveMemoryPools = _active_pools;

  _info.ActivePoolSize.clear ();
  _info.ActivePoolPages.clear ();
  _info.ActivePoolPageSize.clear ();
  _info.ActivePoolOccupancy.clear ();

  for (auto ipool = _mempools.begin (); ipool != _mempools.end () && i < _active_pools; ++ipool, ++i)
  {
    auto pool = (*ipool);

    _info.ActivePoolSize.push_back ((float) (pool->page_size () *
					     pool->page_number ()) / (1024 * 1024));
    _info.ActivePoolPages.push_back (pool->page_number ());
    _info.ActivePoolPageSize.push_back (pool->page_size ());
    _info.ActivePoolOccupancy.push_back ((float) pool->used_pages () /
					 (float) pool->page_number ());
  }

  uint64_t old_packets = _info.ProcessedPackets;
  double old_data = _info.ProcessedData;

  _info.ProcessedPackets = 0;
  _info.ProcessedData = 0;

  for (auto it = _threads.begin (); it != _threads.end (); ++it)
  {
    _info.ProcessedPackets += (*it)->packets ();
    _info.ProcessedData += (*it)->Mbytes ();
  }

  if (old_packets > _info.ProcessedPackets)
    old_packets = 0;

  if (old_data > _info.ProcessedData)
    old_data = 0;

  _info.PacketRate = dtime > 0 ?
    (float) (_info.ProcessedPackets - old_packets) / dtime : 0;
  _info.DataRate = dtime > 0 ?
    (_info.ProcessedData - old_data) / dtime : 0;

  memcpy (&_t0, &t, sizeof (struct timeval));

  return &_info;
}
