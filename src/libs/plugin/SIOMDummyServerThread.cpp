#include "siom/core/SIOMMemoryPool.h"
#include "siom/plugin/SIOMDummyServerThread.h"

void SIOM::DummyServerThread::run ()
{
  while (! _quit.load ())
  {
    static const size_t size = _pool->page_size ();

    if (_running)
    {
      if (auto bpointer = _pool->reserve (size))
      {
	++_npack;
	_Mbytes += ((double) size / MBYTE);

	_pool->validate (bpointer, size);
      }
    }

    std::this_thread::yield ();
  }
}
