#include <set>
#include <vector>
#include <string>
#include <chrono>

#include <sys/time.h>

#include "ipc/partition.h"
#include "emon/SelectionCriteria.h"

#include "siom/core/SIOMMemoryPool.h"
#include "siom/plugin/SIOMEmonServer.h"
#include "siom/plugin/SIOMEmonServerThread.h"

#include "siom_info/siomEmonInput.h"

#define SECTOUSEC 1000000

SIOM::EmonServer::EmonServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
			      unsigned short maxthreads,
			      std::string &part_name,
			      const emon::dal::SamplingParameters *sampling_parameters)
  : InputServer (mpool, maxthreads), _partition_name (part_name), 
    _sampling_parameters (sampling_parameters), _selection_criteria (0), _active_pools (0)
{
  // Init timer

  gettimeofday (&_t0, 0);

  // Start server threads

  this->startThreads ();

  // Start running

  this->start ();
}

SIOM::EmonServer::~EmonServer ()
{
  // Close connections and stop server threads

  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    delete (*i);

  _threads.clear ();

  this->join ();

  // Delete selection criteria

  if (_selection_criteria)
  {
    delete _selection_criteria;
    _selection_criteria = 0;
  }
}

void SIOM::EmonServer::startThreads ()
{
  auto sampler_names = _sampling_parameters->get_SamplerNames ();

  // Create vectors of per-thread data source vectors

  std::vector <std::vector <std::string> > name_vectors (_maxthreads);

  auto iname = sampler_names.begin ();

  bool complete_nv = false;
  bool complete_th = false;

  do
  {
    for (unsigned int i = 0; i < _maxthreads; ++i, ++iname)
    {
      if (iname == sampler_names.end ())
      {
	iname = sampler_names.begin ();
	complete_nv = true;

	if (complete_th)
	  break;
      }

      name_vectors[i].push_back (*iname);
    }

    complete_th = true;
  } while (! complete_nv);

  // Build selection criteria

  emon::Logic  l1_bits_logic  = (bits_logic.find  (_sampling_parameters->get_L1Bits_Logic ()))->second;
  emon::Logic  stream_logic   = (bits_logic.find  (_sampling_parameters->get_Stream_Logic ()))->second;
  emon::Origin l1_bits_origin = (bits_origin.find (_sampling_parameters->get_L1Bits_Origin ()))->second;

  emon::L1TriggerType     l1_type       (_sampling_parameters->get_L1TriggerType (),
					 _sampling_parameters->get_L1TriggerType_Ignore ());
  emon::SmartBitValue     l1_bits_value (_sampling_parameters->get_L1Bits (),
					 l1_bits_logic, l1_bits_origin);
  emon::StatusWord        s_word        (_sampling_parameters->get_StatusWord (),
					 _sampling_parameters->get_StatusWord_Ignore ());
  emon::SmartStreamValue  stream_value  (_sampling_parameters->get_StreamType (),
					 _sampling_parameters->get_StreamNames (),
					 stream_logic);

  _selection_criteria = new emon::SelectionCriteria (l1_type, l1_bits_value,
						     stream_value, s_word);

  // Create threads (and assign pools)

  bool add_active = true;
  auto ipool = _mempools.begin ();

  for (unsigned int i = 0; i < _maxthreads; ++i)
  {
    auto thr = new SIOM::EmonServerThread (*ipool, _partition_name,
					   _sampling_parameters->get_SamplerType (),
					   name_vectors[i],
					   _selection_criteria,
					   _sampling_parameters->get_BufferSize ());

    // Add thread to thread list

    _threads.insert (thr);

    if (add_active)
      ++_active_pools;

    if (++ipool == _mempools.end ())
    {
      ipool = _mempools.begin ();
      add_active = false;
    }
  }
}

void SIOM::EmonServer::run ()
{
  while (! _quit.load ())
    std::this_thread::sleep_for (std::chrono::seconds (1));
}

void SIOM::EmonServer::clearInfo ()
{
  // Reset timers and counters

  gettimeofday (&_t0, 0);

  for (auto it = _threads.begin (); it != _threads.end (); ++it)
    (*it)->reset_counters ();
}

ISInfo *SIOM::EmonServer::getISInfo ()
{
  unsigned int i = 0;
  float dtime;
  struct timeval t;

  gettimeofday (&t, 0);
  dtime = (float) (t.tv_sec - _t0.tv_sec) + (float) (t.tv_usec - _t0.tv_usec) / SECTOUSEC;

  _info.ActiveThreads = _threads.size ();
  _info.ActiveMemoryPools = _active_pools;

  _info.ActivePoolSize.clear ();
  _info.ActivePoolPages.clear ();
  _info.ActivePoolPageSize.clear ();
  _info.ActivePoolOccupancy.clear ();

  for (auto ipool = _mempools.begin (); ipool != _mempools.end () && i < _active_pools; ++ipool, ++i)
  {
    auto pool = (*ipool);

    _info.ActivePoolSize.push_back ((float) (pool->page_size () *
					     pool->page_number ()) / (1024 * 1024));
    _info.ActivePoolPages.push_back (pool->page_number ());
    _info.ActivePoolPageSize.push_back (pool->page_size ());
    _info.ActivePoolOccupancy.push_back ((float) pool->used_pages () /
					 (float) pool->page_number ());
  }

  uint64_t old_packets = _info.ProcessedPackets;
  double old_data = _info.ProcessedData;

  _info.ProcessedPackets = 0;
  _info.ProcessedData = 0;

  for (auto it = _threads.begin (); it != _threads.end (); ++it)
  {
    _info.ProcessedPackets += (*it)->packets ();
    _info.ProcessedData += (*it)->Mbytes ();
  }

  if (old_packets > _info.ProcessedPackets)
    old_packets = 0;

  if (old_data > _info.ProcessedData)
    old_data = 0;

  _info.PacketRate = dtime > 0 ?
    (float) (_info.ProcessedPackets - old_packets) / dtime : 0;
  _info.DataRate = dtime > 0 ?
    (_info.ProcessedData - old_data) / dtime : 0;

  memcpy (&_t0, &t, sizeof (struct timeval));

  return &_info;
}
