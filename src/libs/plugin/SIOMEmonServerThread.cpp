#include <vector>
#include <string>
#include <mutex>

#include "ipc/partition.h"
#include "emon/EventIterator.h"
#include "emon/SelectionCriteria.h"

#include "siom/core/SIOMMemoryPool.h"
#include "siom/plugin/SIOMEmonServerThread.h"

void SIOM::EmonServerThread::startTrigger ()
{
  // Create the event iterator

  std::lock_guard <std::mutex> lock (_internal_mutex);

  try
  {
    IPCPartition p (_partition_name);
    emon::SamplingAddress address (_sampler_type, _sampler_names);

    /*    try{
      m_iter = new emon::EventIterator(partition, *sampler);
    }catch(const emon::Exception& ex){
      throw SamplerNotFound (ERS_HERE, this->dumpsampler(), ex);
      }*/
    _event_iterator = new emon::EventIterator (p, address,
					       *_selection_criteria, _buffer_limit);
  }

  catch (emon::Exception &e)
  {
    ers::fatal (e);
    throw (e);
  }

  _running = true;
}

void SIOM::EmonServerThread::stopTrigger ()
{
  std::lock_guard <std::mutex> lock (_internal_mutex);

  _running = false;

  // Delete event iterator

  delete (_event_iterator);
}

void SIOM::EmonServerThread::run ()
{
  while (! _quit.load ())
  {
    std::this_thread::yield ();
    std::lock_guard <std::mutex> lock (_internal_mutex);

    if (_running)
    {
      emon::Event event;

      try
      {
	event = _event_iterator->tryNextEvent ();
      }

      catch (emon::NoMoreEvents &ex)
      {
	std::this_thread::yield ();
	continue;
      }

      const unsigned int size = event.size () * sizeof (uint32_t);

      if (auto bpointer = _pool->reserve (size))
      {
	++_npack;
	_Mbytes += ((double) size / MBYTE);

	const uint8_t *p = reinterpret_cast <const uint8_t *> (event.data ());
	bpointer->append (const_cast <uint8_t *> (p), size);
	_pool->validate (bpointer, size);
      }

      std::this_thread::yield ();
    }
  }
}
