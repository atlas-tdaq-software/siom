#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>

#include <vector>
#include <string>
#include <thread>
#include <mutex>

// TDAQ include files

#include "is/infodictionary.h"
#include "rc/RunParams.h"

// Package include files

#include "siom_info/siomFileOutput.h"

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMFileDataSender.h"

SIOM::FileDataSender::FileDataSender (std::vector <MemoryPool <uint8_t> *> &mpool,
				      SIOM::DataDestination *dest)
  : SIOM::DataSender (mpool, dest), _descriptor (-1)
{
  auto par = reinterpret_cast <SIOM::FileOutParameters *> (dest->address ());

  _outpar = new SIOM::FileOutParameters (par->activation_flag (),
					 par->ipc_partition (),
					 par->file_location (),
					 par->file_base_name (),
					 par->maxsize (),
					 par->core_conf ());

  _index = 0;
  _written = 0;
  _event_number = 0;
  _writeData = false;
  _firstInDataSet = true;

  std::string FileActivationFlag ("Always");

  if (_outpar->activation_flag () == FileActivationFlag)
    _writeData = true;

  // Start the sender thread

  this->start ();
}

SIOM::FileDataSender::~FileDataSender ()
{
  this->endOfRun ();
  this->join ();

  delete _outpar;
}

void SIOM::FileDataSender::startOfRun ()
{
  std::string ISActivationFlag ("IS");

  _index = 0;
  _firstInDataSet = true;

  if (_outpar->activation_flag () == ISActivationFlag)
  {
    // Get the flag from IS

    RunParams run_params;
    ISInfoDictionary is_dict (*(_outpar->ipc_partition ()));

    run_params.recording_enabled = 0;

    try
    {
      is_dict.findValue ("RunParams.RunParams", run_params);
    }

    catch (daq::is::Exception &ex)
    {
      std::cout << "Couldn't get RunParams from IS" << std::endl;
    }

    _writeData = (run_params.recording_enabled != 0);
  }
}

void SIOM::FileDataSender::endOfRun ()
{
  // Close the file and rename it

  if (_descriptor >= 0)
  {
    std::lock_guard <std::mutex> lock (_fileMutex);

    close (_descriptor);
    _descriptor = -1;

    _filename = this->publish_file (_filename);
  }
}

int SIOM::FileDataSender::send (SIOM::MemoryBuffer <uint8_t> *b)
{
  size_t datalen;
  size_t len = datalen = b->n_objects () * sizeof (uint8_t);

  if (_writeData)
  {
    int ret = 0;

    if (_firstInDataSet)
    {
      RunParams rp;

      if (_outpar->core_conf ())
      {
	(_outpar->core_conf ())->get_runParams (rp);
	_RunNumber = rp.run_number;
      }
      else
	_RunNumber = 0;

      _filename = this->build_filename (_outpar->file_location (),
					_outpar->file_base_name (),
					_RunNumber, _index);

      // Open output file.

      if ((_descriptor = open (_filename.c_str (),
			       O_WRONLY | O_CREAT | O_EXCL,
			       S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) < 0)
      {
	std::cout << "Error in opening file " << _filename << std::endl;
      }

      _firstInDataSet = false;
    }

    if (_written + datalen > _outpar->maxsize ())
    {
      close (_descriptor);
      _filename = this->publish_file (_filename);

      ++_index;
      _filename = this->build_filename (_outpar->file_location (),
					_outpar->file_base_name (),
					_RunNumber, _index);

      if ((_descriptor = open (_filename.c_str (),
			       O_WRONLY | O_CREAT | O_EXCL,
			       S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) < 0)
      {
	std::cout << "Error in opening file " << _filename << std::endl;
	return false;
      }

      _written = 0;
      _event_number = 0;
    }

    // write data

    std::lock_guard <std::mutex> lock (_fileMutex);

    if (_descriptor >= 0) // Ignore events between runs
    {
      unsigned int written = 0;

      while (written < sizeof (uint32_t))
      {
	if ((ret = write (_descriptor, ((char *) &datalen),
			  sizeof (uint32_t) - written)) < 0)
	{
	  if (errno == EINTR)
	    continue;
	  else
	    break;
	}
	else
	  written += ret;
      }

      size_t page_size = b->page_size ();
      auto pages = b->get_pages ();

      for (auto page = pages.begin (); page != pages.end (); ++page) 
      {
	written = 0;
	size_t to_write = len > page_size ? page_size : len;

        if (to_write > 0)
        {
	  while (written < to_write)
	  {
	    if ((ret = write (_descriptor, ((char *) (*page) + written),
			      to_write - written)) < 0)
	    {
	      if (errno == EINTR)
	        continue;
	      else
	        break;
	    }
	    else
	      written += ret;
	  }
        }

	_written += written;
        len -= to_write;
      }

      ++_event_number;

      if (ret <= 0)
	std::cout << "file write returned " << ret << std::endl;
      else
      ++_event_number;	
    }

    return (ret > 0 ? datalen : 0);
  }
  else
    return datalen;
}
