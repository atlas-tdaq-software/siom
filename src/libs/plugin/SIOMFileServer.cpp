#include <vector>
#include <set>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "siom/core/SIOMMemoryPool.h"

#include "siom/plugin/SIOMFileServer.h"
#include "siom/plugin/SIOMFileServerThread.h"

#include "siom_info/siomFileInput.h"

#define SECTOUSEC 1000000

SIOM::FileServer::FileServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
			      unsigned short maxthreads,
			      std::vector <std::string> &input_files,
			      bool pack_events)
  : InputServer (mpool, maxthreads), _files (input_files), _pack (pack_events)
{
  // Init timer

  gettimeofday (&_t0, 0);

  // Init counters

  _nthreads = 0;
  _active_pools = _mempools.size ();

  this->startThreads ();
  this->start ();
}

SIOM::FileServer::~FileServer ()
{
  // Close files and stop server threads

  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    delete (*i);

  _threads.clear ();
  _nthreads = 0;

  // Close input files

  for (auto j = _fd.begin (); j != _fd.end () ; ++j)
    close ((*j));

  this->join ();
}

void SIOM::FileServer::startThreads ()
{
  auto i = _mempools.begin ();

  // Open files and create threads

  for (auto ifile = _files.begin (); ifile != _files.end (); ++ifile)
  {
    int fd;

    if ((fd = open ((*ifile).c_str (), O_RDONLY)) < 0)
      continue; /////////////////////////////////////////////////fixme

    _fd.push_back (fd);

    /* Start thread - file assignement */

    if (_maxthreads == 0 || _nthreads < _maxthreads)
    {
      auto *thr = new SIOM::FileServerThread ((*i), _pack);

      thr->add (fd);

      /* Add thread to thread list */

      _threads.insert (thr);
      ++_nthreads;

      // Increment pool index

      if (++i == _mempools.end ())
	i = _mempools.begin ();
    }
    else
    {
      /* Add link to less loaded thread */

      unsigned int n, min;
      std::set <SIOM::FileServerThread *>::iterator is, js;

      for (is = _threads.begin (), min = 0xffff; is != _threads.end (); ++is)
	if ((n = (*is)->nfile ()) < min)
	{
	  js = is;
	  min = n;
	}

      (*js)->add (fd);
    }
  }
}

void SIOM::FileServer::run ()
{
  while (true)
    std::this_thread::sleep_for (std::chrono::seconds (1));
}

void SIOM::FileServer::clearInfo ()
{
  // Reset timers

  gettimeofday (&_t0, 0);

  for (auto it = _threads.begin (); it != _threads.end (); ++it)
    (*it)->reset_counters ();
}

ISInfo *SIOM::FileServer::getISInfo ()
{
  unsigned int i = 0;
  float dtime;
  struct timeval t;

  gettimeofday (&t, 0);
  dtime = (float) (t.tv_sec - _t0.tv_sec) + (float) (t.tv_usec - _t0.tv_usec) / SECTOUSEC;

  _info.ActiveThreads = _nthreads;
  _info.ActiveMemoryPools = _active_pools;

  _info.ActivePoolSize.clear ();
  _info.ActivePoolPages.clear ();
  _info.ActivePoolPageSize.clear ();
  _info.ActivePoolOccupancy.clear ();

  for (auto ipool = _mempools.begin (); ipool != _mempools.end () && i < _active_pools; ++ipool, ++i)
  {
    _info.ActivePoolSize.push_back ((float) ((*ipool)->page_size () *
					     (*ipool)->page_number ()) / (1024 * 1024));
    _info.ActivePoolPages.push_back ((*ipool)->page_number ());
    _info.ActivePoolPageSize.push_back ((*ipool)->page_size ());
    _info.ActivePoolOccupancy.push_back ((float) (*ipool)->used_pages () /
					 (float) (*ipool)->page_number ());
  }

  uint64_t old_packets = _info.ProcessedPackets;
  double old_data = _info.ProcessedData;

  _info.ProcessedPackets = 0;
  _info.ProcessedData = 0;

  for (auto it = _threads.begin (); it != _threads.end (); ++it)
  {
    _info.ProcessedPackets += (*it)->packets ();
    _info.ProcessedData += (*it)->Mbytes ();
  }

  if (old_packets > _info.ProcessedPackets)
    old_packets = 0;

  if (old_data > _info.ProcessedData)
    old_data = 0;

  _info.PacketRate = dtime > 0 ?
    (float) (_info.ProcessedPackets - old_packets) / dtime : 0;
  _info.DataRate = dtime > 0 ?
    (_info.ProcessedData - old_data) / dtime : 0;

  _info.InputFileNumber = _fd.size ();

  memcpy (&_t0, &t, sizeof (struct timeval));

  return &_info;
}
