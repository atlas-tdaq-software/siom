#include <list>
#include <thread>
#include <mutex>

#include <sys/unistd.h>
#include <sys/uio.h>

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"

#include "siom/plugin/SIOMFileServerThread.h"

SIOM::FileServerThread::FileServerThread (SIOM::MemoryPool <uint8_t> *memPool, bool pack)
  : _pool (memPool), _pack (pack)
{
  _nfile = 0;
  _running = false;

  this->reset_counters ();
  this->start ();
}

void SIOM::FileServerThread::add (int fd)
{
  std::lock_guard <std::mutex> lock (_fdlist_mutex);

  _fdlist.push_back (fd);
  ++_nfile;
}

void SIOM::FileServerThread::remove (int fd)
{
  std::lock_guard <std::mutex> lock (_fdlist_mutex);

  _fdlist.remove (fd);
  --_nfile;
}

void SIOM::FileServerThread::run ()
{
  static int garbage_len = 0;
  static char *garbage = 0;
  uint8_t *current_pointer = 0;
  int nwritten = 0;

  SIOM::MemoryBuffer <uint8_t> *b = 0;

  int page_size = _pool->page_size ()* sizeof (uint8_t);

  while (! _quit.load ())
  {
    if (_running)
    {
      std::lock_guard <std::mutex> lock (_fdlist_mutex);

      for (auto i = _fdlist.begin (); i != _fdlist.end (); ++i)
      {
	ssize_t n;
        ssize_t nread;
	int32_t length;

	int fd = (*i);

        if ((n = read (fd, &length, sizeof (int32_t))) > 0)
        {
          if (_pack)
          {
	    // If data do not fit into a page, send a warning and clean-up
            // input buffers

	    if (length > page_size)
	    {
	      if (length > garbage_len)
	      {
	        garbage = (char *) realloc (garbage, length);
	        garbage_len = length;
	      }

	      if ((n = read (fd, garbage, length)) > 0)
	        ++_nthrown;
	      else
	        lseek (fd, 0, SEEK_SET);

	      continue;
	    }

	    if (length + nwritten > page_size)
	    {
	      // Validate the current buffer if already allocated

	      if (b)
	        _pool->validate (b, nwritten);

	      // Reserve a new buffer (single page)
	      // and set current pointer

	      while ((! (b = _pool->reserve (page_size))) && ! _quit.load ())
       	        std::this_thread::yield ();

	      if (_quit.load () && ! b)
	        break;

	      nwritten = 0;
	      auto p = b->get_pages ().begin ();

	      current_pointer = *p;
	    }

	    if ((nread = read (fd, current_pointer, n)) > 0)
	    {
	      nwritten += nread;
	      current_pointer += nread;
	    }
	    else
	    {
	      lseek (fd, 0, SEEK_SET);
	      continue;
	    }
          }
          else
	  {
            while ((! (b = _pool->reserve (n))) && ! _quit.load ())
       	      std::this_thread::yield ();

	    if (_quit.load () && ! b)
	      break;

            int j = 0, k = n;
	    auto pages = b->get_pages ();
	    struct iovec vec[UIO_MAXIOV];

            for (auto p = pages.begin (); p != pages.end (); ++p, ++j, k -= page_size)
	    {
	      vec[j].iov_base = *p;
              vec[j].iov_len = k > page_size ? page_size : k;
	    }

            if ((nread = readv (fd, vec, pages.size ())) > 0)
	    {
	      nwritten += nread;
	      current_pointer += nread;
	    }
	    else
	    {
	      lseek (fd, 0, SEEK_SET);
              _pool->release (b);
	      continue;
	    }

            _pool->validate (b, nread);
          }

	  ++_npack;
	  _Mbytes += ((double) nread / MBYTE);
        }
      }
    }

    // Release the scheduler

    std::this_thread::yield ();
  }
}
