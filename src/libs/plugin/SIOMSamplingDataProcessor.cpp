#include <vector>

#include "ipc/core.h"
#include "emon/EventSampler.h"

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMDataProcessor.h"
#include "siom/core/SIOMAlgoParams.h"
#include "siom/core/SIOMDataProcessor.h"

#include "siom/plugin/SIOMSamplingParameters.h"
#include "siom/plugin/SIOMSamplingDataProcessor.h"

SIOM::SamplingDataProcessor::SamplingDataProcessor (std::vector <SIOM::MemoryPool <uint8_t> *> &inputPoolVector,
						    std::vector <SIOM::MemoryPool <uint8_t> *> &outputPoolVector,
						    SIOM::AlgoParams *params)
  : SIOM::DataProcessor (inputPoolVector, outputPoolVector, params)
{
  auto sampling_params = reinterpret_cast <SIOM::SamplingParameters *> (params->address ());

  try
  {
    uint32_t h = sampling_params->header ();

    _sampler_factory = new SIOM::SIOMSamplerFactory (false,
						     reinterpret_cast <char *> (&h),
						     sizeof (h));

    for (auto p = inputPoolVector.begin (); p != inputPoolVector.end (); ++p)
      _sampler_factory->add_inpool (*p);

    for (auto p = outputPoolVector.begin (); p != outputPoolVector.end (); ++p)
      _sampler_factory->add_outpool (*p);

    const IPCPartition partition (sampling_params->partition_name ());
    emon::SamplingAddress address (sampling_params->type (), sampling_params->name ());

    _event_sampler = new emon::EventSampler (partition, address, _sampler_factory,
					     static_cast <size_t> (sampling_params->max_channels ()));
  }

  catch (daq::ipc::Exception &e)
  {
    ers::error (e);
    throw (e);
  }

  // Start the sender thread

  this->start ();
}

SIOM::SamplingDataProcessor::~SamplingDataProcessor ()
{
  this->join ();
  delete _event_sampler;
}

unsigned int SIOM::SamplingDataProcessor::process_data (SIOM::MemoryBuffer <uint8_t> *input,
							SIOM::MemoryBuffer <uint8_t> *output)
{
  size_t to_copy = input->n_objects ();

  if (to_copy > output->size ())
    return 0;

  auto in_pages = input->get_pages ();

  for (auto pagein = in_pages.begin (); pagein != in_pages.end () && to_copy; ++pagein)
  {
    size_t n = to_copy > input->page_size () ? input->page_size () : to_copy;

    output->append (*pagein, n);
    to_copy -= n;
  }

  return output->n_objects ();
}
