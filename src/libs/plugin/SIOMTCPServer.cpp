#include <set>
#include <vector>

#include <sys/time.h>

#include "transport/Transport.h"
#include "transport/TCP.h"

#include "siom/core/SIOMMemoryPool.h"

#include "siom/plugin/SIOMTCPServer.h"
#include "siom/plugin/SIOMTCPServerThread.h"

#include "siom_info/siomTCPInput.h"

#define SECTOUSEC 1000000

SIOM::TCPServer::TCPServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
			    unsigned short maxthreads,
			    unsigned short port,
			    bool pack_events)
  : InputServer (mpool, maxthreads), _port (port), _pack (pack_events)
{
  // Init timer

  gettimeofday (&_t0, 0);

  // Start server. Exceptions are not caught to be sent to the upper level.
  // It must be non-blocking to allow thread join.

  _server = new TCP (_port);
  _server->set_nonblocking (true);

  // Init counters

  _nthreads = 0;
  _active_pools = _mempools.size ();

  this->start ();
}

SIOM::TCPServer::~TCPServer ()
{
  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    delete (*i);

  _threads.clear ();
  _nthreads = 0;

  this->join ();

  delete _server;
}

void SIOM::TCPServer::run ()
{
  auto i = _mempools.begin ();

  while (! _quit.load ())
  {
    TCP *new_transport;

    if ((new_transport = _server->accept ()))
    {
      // Start thread - connection assignement

      std::cerr << "Connection accepted " << std::endl;
      if (_maxthreads == 0 || _nthreads < _maxthreads)
      {
	auto *thr = new SIOM::TCPServerThread ((*i), _pack);

	std::cerr << "Thread " << _nthreads << " created" << std::endl;
	thr->add (new_transport);

	// Add thread to thread list

	_threads.insert (thr);
	_nthreads++;

	// Increment pool index

	if (++i == _mempools.end ())
	  i = _mempools.begin ();
      }
      else
      {
	// Add link to less loaded thread

	unsigned int n, t, min, tmin;
	std::set <SIOM::TCPServerThread *>::iterator is, js;

	std::cerr << "Check which thread is less loaded" << std::endl;
	for (is = _threads.begin (), min = 0xffff, t = 0; is != _threads.end (); ++is, ++t)
	  if ((n = (*is)->ntrans ()) < min)
	  {
	    js = is;
	    min = n;
	    tmin = t;
	  }

	std::cerr << "Adding link to thread number " << tmin << std::endl;
	(*js)->add (new_transport);
      }
    }

    // Take some time between cycles

    std::this_thread::sleep_for (std::chrono::milliseconds (10));
    std::this_thread::yield ();
  }
}

void SIOM::TCPServer::clearInfo ()
{
  // Reset timers and counters

  gettimeofday (&_t0, 0);

  for (auto it = _threads.begin (); it != _threads.end (); ++it)
    (*it)->reset_counters ();
}

ISInfo *SIOM::TCPServer::getISInfo ()
{
  unsigned int i = 0;
  float dtime;
  struct timeval t;

  gettimeofday (&t, 0);
  dtime = (float) (t.tv_sec - _t0.tv_sec) + (float) (t.tv_usec - _t0.tv_usec) / SECTOUSEC;

  _info.ActiveThreads = _nthreads;
  _info.ActiveMemoryPools = _active_pools;

  _info.ActivePoolSize.clear ();
  _info.ActivePoolPages.clear ();
  _info.ActivePoolPageSize.clear ();
  _info.ActivePoolOccupancy.clear ();

  for (auto ipool = _mempools.begin (); ipool != _mempools.end () && i < _active_pools; ++ipool, ++i)
  {
    _info.ActivePoolSize.push_back ((float) ((*ipool)->page_size () *
					     (*ipool)->page_number ()) / (1024 * 1024));
    _info.ActivePoolPages.push_back ((*ipool)->page_number ());
    _info.ActivePoolPageSize.push_back ((*ipool)->page_size ());
    _info.ActivePoolOccupancy.push_back ((float) (*ipool)->used_pages () /
					 (float) (*ipool)->page_number ());
  }

  uint64_t old_packets = _info.ProcessedPackets;
  double old_data = _info.ProcessedData;

  _info.ProcessedPackets = 0;
  _info.ProcessedData = 0;
  _info.PacketsThrown = 0;
  _info.ActiveConnections = 0;

  for (auto it = _threads.begin (); it != _threads.end (); ++it)
  {
    _info.ProcessedPackets += (*it)->packets ();
    _info.ProcessedData += (*it)->Mbytes ();
    _info.PacketsThrown += (*it)->thrown ();
    _info.ActiveConnections += (*it)->ntrans ();
  }

  if (old_packets > _info.ProcessedPackets)
    old_packets = 0;

  if (old_data > _info.ProcessedData)
    old_data = 0;

  _info.PacketRate = dtime > 0 ?
    (float) (_info.ProcessedPackets - old_packets) / dtime : 0;
  _info.DataRate = dtime > 0 ?
    (_info.ProcessedData - old_data) / dtime : 0;

  memcpy (&_t0, &t, sizeof (struct timeval));

  return &_info;
}
