#include <vector>
#include <map>
#include <thread>
#include <mutex>
#include <chrono>

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/select.h>

#include "transport/Transport.h"

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"

#include "siom/plugin/SIOMTCPServerThread.h"

static struct timeval s_timeout = {0, 100000};

#define DISCARD_BUFFER_LEN 65536

SIOM::TCPServerThread::TCPServerThread (SIOM::MemoryPool <uint8_t> *memPool, bool pack)
  : _pool (memPool), _pack (pack)
{
  _nfds = 0;
  _ntrans = 0;

  FD_ZERO (&_select);

  this->reset_counters ();
  this->start ();
}

SIOM::TCPServerThread::~TCPServerThread ()
{
  for (auto j = _tcp_map.begin (); j != _tcp_map.end (); ++j)
    this->remove ((*j).second);

  _tcp_map.clear ();

  this->join ();
}

void SIOM::TCPServerThread::add (TCP *connection)
{
  int s = connection->get_handle ();

  std::lock_guard <std::mutex> lock (_fdset_mutex);

  if (s > _nfds - 1)
    _nfds = s + 1;

  FD_SET (s, &_select);
  ++_ntrans;

  std::pair <int, TCP *> conn_pair (s, connection);

  _tcp_map.insert (conn_pair);
}

void SIOM::TCPServerThread::remove (TCP *connection)
{
  int i, imax = -1;
  int s = connection->get_handle ();

  std::lock_guard <std::mutex> lock (_fdset_mutex);

  if (FD_ISSET (s, &_select))
  {
    FD_CLR (s, &_select);

    for (i=0; i < _nfds; ++i)
      if (FD_ISSET (i, &_select))
	imax = i;

    _nfds = imax + 1;
    --_ntrans;

    _tcp_map.erase (s);
    delete connection;
  }
}

void SIOM::TCPServerThread::run ()
{
  uint8_t *current_pointer = 0;
  int nwritten = 0;

  SIOM::MemoryBuffer <uint8_t> *b = 0;
  int page_size = _pool->page_size () * sizeof (uint8_t);

  std::vector <int> closed_connections;

  while (! _quit.load ())
  {
    int nsel = 0;
    int nfds;
    fd_set rfds;
    struct timeval t;

    for (auto ic = closed_connections.begin (); ic != closed_connections.end (); ++ic)
      this->remove (*ic);

    closed_connections.clear ();

    memcpy (&t, &s_timeout, sizeof (struct timeval));

    _fdset_mutex.lock ();

    nfds = _nfds;
    memcpy (&rfds, &_select, sizeof (fd_set));
    nsel = select (nfds, (nfds ? &rfds : 0), 0, 0, &t);

    _fdset_mutex.unlock ();

    if (nsel)
    {
      int i, received;

      for (i = 0; i < nfds && ! _quit.load (); ++i)
      {
	if (FD_ISSET (i, &rfds))
	{
	  uint32_t lenword;
	  int length = sizeof (uint32_t);

	  received = this->tcp_safe_receive (i, reinterpret_cast <uint8_t *> (&lenword), length);

	  if (received > 0)
	  {
	    length = ntohl (lenword);

	    if (_pack)
	    {
	      // If data do not fit into a page, discard and clean-up
	      // input buffers

	      if (length > page_size)
	      {
		uint8_t discard_buffer[DISCARD_BUFFER_LEN];
		int to_read = length;

		while (to_read > 0 && received > 0)
		{
		  received = this->tcp_safe_receive (i, discard_buffer,
						     to_read > DISCARD_BUFFER_LEN ? DISCARD_BUFFER_LEN : to_read);
		  to_read -= received;
		}

		++_nthrown;
		continue;
	      }

	      if (length + nwritten > page_size)
	      {
		// Validate current buffer if already allocated

		if (b)
		  _pool->validate (b, nwritten);

		// Reserve a new buffer (single page)
		// and set current pointer

		while ((! (b = _pool->reserve (page_size))) && ! _quit.load ())
		  std::this_thread::yield ();

		if (_quit.load () && ! b)
		  break;

		nwritten = 0;
		auto p = b->get_pages ().begin ();

		current_pointer = *p;
	      }

	      if ((received = this->tcp_safe_receive (i, current_pointer, length)) > 0)
	      {
		nwritten += received;
		current_pointer += received;
	      }
	      else
	      {
		closed_connections.push_back (i);
		std::cerr << "Error " << errno << " from connection, closing " << std::endl;
	      }
	    }
	    else
	    {
	      // Reserve a new buffer (multi page if needed)

	      while ((! (b = _pool->reserve (length))) && ! _quit.load ())
		std::this_thread::yield ();

	      if (_quit.load () && ! b)
		break;

	      nwritten = 0;
	      auto pages = b->get_pages ();

	      for (auto p = pages.begin (); p != pages.end (); ++p)
	      {
		if ((received = this->tcp_safe_receive (i, *p, length > page_size ? page_size : length)) > 0)
		{
		  length -= received;
		  nwritten += received;
		}
		else
		{
		  nwritten = 0;
		  break;
		}
	      }

	      if (nwritten)
		_pool->validate (b, nwritten);
	      else
	      {
		_pool->release (b);
		closed_connections.push_back (i);
		std::cerr << "Error " << errno << " from connection, closing " << std::endl;
	      }
	    }

	    // Update counters

	    ++_npack;
	    _Mbytes += (static_cast <double> (nwritten) / MBYTE);
	  }
	  else
	  {
	    closed_connections.push_back (i);
	    std::cerr << "Error " << errno << " from connection, closing " << std::endl;
	  }
	}
      }
    }
    else
    {
      std::this_thread::sleep_for (std::chrono::milliseconds (20));
    }

    std::this_thread::yield ();
  }
}
