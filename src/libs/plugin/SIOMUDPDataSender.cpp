#include <vector>

#include <errno.h>

// TDAQ include files

#include "transport/UDP.h"

// Package include files

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMUDPDataSender.h"

SIOM::UDPDataSender::UDPDataSender (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
				    SIOM::DataDestination *dest)
  : SIOM::DataSender (mpool, dest)
{
  NetAddress destination_address = *(reinterpret_cast <NetAddress *> (dest->address ()));

  // Connect to server. Exceptions are not caught.

  _client = new UDP (destination_address, 0);

  // Start the sender thread

  this->start ();
}

int SIOM::UDPDataSender::send (SIOM::MemoryBuffer <uint8_t> *b)
{
  size_t datalen;
  bool ret = true;

  // send data length

  size_t len = datalen = b->n_objects () * sizeof (uint8_t);

  // uint32_t lword = htonl (datalen);

  //if (! (ret = _client->send (&lword, sizeof (uint32_t))))
  //return 0;

  // send data

  size_t page_size = b->page_size ();

  auto pages = b->get_pages ();

  for (auto page = pages.begin (); page != pages.end (); ++page)
  {
    size_t l = len > page_size ? page_size : len;

    if (! (ret = _client->send (reinterpret_cast <char *> (*page), l)))
      break;

    len -= l;
  }

  if (! ret)
    std::cout << "UDP write returned " << ret << " errno = " << errno << std::endl;

  return (ret ? datalen : 0);
}
