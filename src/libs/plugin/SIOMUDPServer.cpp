#include <set>
#include <vector>

#include <sys/time.h>

#include "transport/Transport.h"
#include "transport/UDP.h"

#include "siom/core/SIOMMemoryPool.h"

#include "siom/plugin/SIOMUDPServer.h"
#include "siom/plugin/SIOMUDPServerThread.h"

#include "siom_info/siomUDPInput.h"

#define SECTOUSEC 1000000

SIOM::UDPServer::UDPServer (std::vector <SIOM::MemoryPool <uint8_t> *> &mpool,
			    unsigned short maxthreads,
			    unsigned short port,
			    bool pack_events)
  : InputServer (mpool, maxthreads), _port (port), _pack (pack_events)
{
  // Init timer

  gettimeofday (&_t0, 0);

  // Init counters

  _nthreads = 0;
  _active_pools = _mempools.size ();

  this->start ();
}

SIOM::UDPServer::~UDPServer ()
{
  for (auto i = _threads.begin (); i != _threads.end (); ++i)
    delete (*i);

  _threads.clear ();
  _nthreads = 0;

  this->join ();
}

void SIOM::UDPServer::run ()
{
  auto i = _mempools.begin ();

  unsigned short imax = (_maxthreads < _mempools.size ()) ? _mempools.size () : _maxthreads;

  for (unsigned short j = 0; j < imax; ++j)
  {
    // Start server. Exceptions are not caught to be sent to the upper level.

    UDP *server = new UDP (_port + j);
    SIOM::UDPServerThread *thr = new SIOM::UDPServerThread ((*i), _pack);

    thr->add (server);

    /* Add thread to thread list */

    _threads.insert (thr);
    _nthreads++;

    // Increment pool index

    if (++i == _mempools.end ())
      i = _mempools.begin ();
  }

  while (! _quit.load ())
    std::this_thread::sleep_for (std::chrono::milliseconds (100));
}

void SIOM::UDPServer::clearInfo ()
{
  // Reset timers

  gettimeofday (&_t0, 0);

  for (auto it = _threads.begin (); it != _threads.end (); ++it)
    (*it)->reset_counters ();
}

ISInfo *SIOM::UDPServer::getISInfo ()
{
  unsigned int i = 0;
  float dtime;
  struct timeval t;

  gettimeofday (&t, 0);
  dtime = (float) (t.tv_sec - _t0.tv_sec) + (float) (t.tv_usec - _t0.tv_usec) / SECTOUSEC;

  _info.ActiveThreads = _nthreads;
  _info.ActiveMemoryPools = _active_pools;

  _info.ActivePoolSize.clear ();
  _info.ActivePoolPages.clear ();
  _info.ActivePoolPageSize.clear ();
  _info.ActivePoolOccupancy.clear ();

  for (auto ipool = _mempools.begin (); ipool != _mempools.end () && i < _active_pools; ++ipool, ++i)
  {
    _info.ActivePoolSize.push_back ((float) ((*ipool)->page_size () *
					     (*ipool)->page_number ()) / (1024 * 1024));
    _info.ActivePoolPages.push_back ((*ipool)->page_number ());
    _info.ActivePoolPageSize.push_back ((*ipool)->page_size ());
    _info.ActivePoolOccupancy.push_back ((float) (*ipool)->used_pages () /
					 (float) (*ipool)->page_number ());
  }

  uint64_t old_packets = _info.ProcessedPackets;
  double old_data = _info.ProcessedData;

  _info.ProcessedPackets = 0;
  _info.ProcessedData = 0;
  _info.PacketsThrown = 0;
  _info.ActiveServers = 0;
  _info.SocketErrors = 0;

  _info.DataSources.clear ();
  _info.PacketsFromSource.clear ();

  for (auto it = _threads.begin (); it != _threads.end (); ++it)
  {
    _info.ProcessedPackets += (*it)->packets ();
    _info.ProcessedData += (*it)->Mbytes ();
    _info.PacketsThrown += (*it)->thrown ();
    _info.ActiveServers += (*it)->ntrans ();
    _info.SocketErrors += (*it)->errors ();

    auto th_source_map = (*it)->source_map ();

    for (auto isource = th_source_map.begin (); isource != th_source_map.end (); ++isource)
    {
      struct in_addr iadd;
      iadd.s_addr = isource->first;

      _info.DataSources.push_back (inet_ntoa (iadd));
      _info.PacketsFromSource.push_back (isource->second);
    }
  }

  if (old_packets > _info.ProcessedPackets)
    old_packets = 0;

  if (old_data > _info.ProcessedData)
    old_data = 0;

  _info.PacketRate = dtime > 0 ?
    (float) (_info.ProcessedPackets - old_packets) / dtime : 0;
  _info.DataRate = dtime > 0 ?
    (_info.ProcessedData - old_data) / dtime : 0;

  memcpy (&_t0, &t, sizeof (struct timeval));

  return &_info;
}
