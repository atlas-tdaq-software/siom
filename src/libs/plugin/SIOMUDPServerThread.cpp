#include <iostream>
#include <string>
#include <list>
#include <map>
#include <mutex>
#include <chrono>

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>

#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"

#include "siom/plugin/SIOMUDPServerThread.h"

static struct timeval s_timeout = {0, 200000};

#define DISCARD_BUFFER_LEN 65536

SIOM::UDPServerThread::UDPServerThread (SIOM::MemoryPool <uint8_t> *memPool, bool pack)
  : _pool (memPool), _pack (pack)
{
  _nfds = 0;
  _ntrans = 0;

  FD_ZERO (&_select);

  this->reset_counters ();
  this->start ();
}

SIOM::UDPServerThread::~UDPServerThread ()
{
  _fdset_mutex.lock ();

  _nfds = 0;
  FD_ZERO (&_select);
  _ntrans = 0;

  for (auto j = _udplist.begin (); j != _udplist.end (); ++j)
    delete (*j);

  _udplist.clear ();
  _smap.clear ();

  _fdset_mutex.unlock ();

  this->join ();
}

void SIOM::UDPServerThread::add (UDP *server)
{
  int s = server->get_handle ();

  std::lock_guard <std::mutex> lock (_fdset_mutex);

  if (s > _nfds - 1)
    _nfds = s + 1;

  FD_SET (s, &_select);
  ++_ntrans;

  _udplist.push_back (server);
}

void SIOM::UDPServerThread::remove (UDP *server)
{
  int i, imax = -1;
  int s = server->get_handle ();

  std::lock_guard <std::mutex> lock (_fdset_mutex);

  if (FD_ISSET (s, &_select))
  {
    FD_CLR (s, &_select);

    for (i=0; i < _nfds; ++i)
      if (FD_ISSET (i, &_select))
	imax = i;

    _nfds = imax + 1;
    --_ntrans;

    _udplist.remove (server);
    delete server;
  }
}

void SIOM::UDPServerThread::run ()
{
  uint8_t *current_pointer = 0;
  int nwritten = 0;

  SIOM::MemoryBuffer <uint8_t> *b = 0;
  int page_size = _pool->page_size () * sizeof (uint8_t);

  while (! _quit.load ())
  {
    int nsel = 0;
    int nfds;
    fd_set rfds;
    struct timeval t;

    memcpy (&t, &s_timeout, sizeof (struct timeval));

    _fdset_mutex.lock ();

    nfds = _nfds;
    memcpy (&rfds, &_select, sizeof (fd_set));
    nsel = select (nfds, (nfds ? &rfds : 0), 0, 0, &t);

    _fdset_mutex.unlock ();

    if (nsel)
    {
      int i;
      int length = 0;
      int received = 0;
      struct sockaddr_in sender_address;
      socklen_t address_length = sizeof (sender_address);

      for (i = 0; i < nfds && ! _quit.load (); ++i)
      {
	if (FD_ISSET (i, &rfds))
	{
	  if (ioctl (i, FIONREAD, &length) != -1)
	  {
	    if (_pack)
	    {
	      // If data do not fit into a page, discard and clean-up
	      // input buffers

	      if (length > page_size)
	      {
		// DISCARD_BUFFER_LEN accommodates the maximum size datagram

		uint8_t discard_buffer[DISCARD_BUFFER_LEN];

		do
		{
		  received = ::recvfrom (i, discard_buffer,
					 length, MSG_WAITALL, 
					 (struct sockaddr *) &sender_address,
					 &address_length);
		}
		while (received == -1 && (errno == EINTR || errno == EAGAIN));
    
		++_nthrown;
		continue;
	      }

	      // Does it fit in the current page ?

	      if (length + nwritten > page_size)
	      {
		// Validate current buffer if already allocated

		if (b)
		  _pool->validate (b, nwritten);

		// Reserve a new buffer (single page)
		// and set current pointer

		while ((! (b = _pool->reserve (page_size))) && ! _quit.load ())
		  std::this_thread::yield ();

		if (_quit.load () && ! b)
		  break;

		nwritten = 0;
		auto p = b->get_pages ().begin ();

		current_pointer = *p;
	      }

	      // Receive data and get source

	      do
	      {
		received = ::recvfrom (i, current_pointer,
				       length, MSG_WAITALL, 
				       (struct sockaddr *) &sender_address,
				       &address_length);
	      }
	      while (received == -1 && (errno == EINTR || errno == EAGAIN));
    
	      if (received == -1)
	      {
		// Ignore and count the error

		++_socket_errors;
		continue;
	      }
	      else
	      {
		nwritten += received;
		current_pointer += received;
	      }
	    }
	    else
	    {
	      // Reserve a new buffer (multi page if needed)

	      while ((! (b = _pool->reserve (length))) && ! _quit.load ())
		std::this_thread::yield ();

	      if (_quit.load () && ! b)
		break;

	      size_t np;

	      if ((np = b->n_pages ()) > UIO_MAXIOV)
	      {
		// Message is too large - discard it
		// DISCARD_BUFFER_LEN accommodates the maximum size datagram

		uint8_t discard_buffer[DISCARD_BUFFER_LEN];

		do
		{
		  received = ::recvfrom (i, discard_buffer,
					 length, MSG_WAITALL, 
					 (struct sockaddr *) &sender_address,
					 &address_length);
		}
		while (received == -1 && (errno == EINTR || errno == EAGAIN));
    
		++_nthrown;
                _pool->release (b);

		continue;
	      }

	      int j = 0;
	      auto pages = b->get_pages ();
	      struct iovec vec[UIO_MAXIOV];

	      for (auto p = pages.begin (); p != pages.end (); ++p, ++j)
	      {
		vec[j].iov_base = *p;
		vec[j].iov_len = page_size;
	      }

	      struct msghdr hdr = {&sender_address, address_length, vec, np, 0, 0, 0};
	      received = ::recvmsg (i, &hdr, 0);

	      if (received == -1)
	      {
		// Ignore and count the error

		++_socket_errors;
                _pool->release (b);

		continue;
	      }
	    }

            _pool->validate (b, received);

	    ++_npack;
	    _Mbytes += ((double) received / MBYTE);

	    std::map <unsigned int, uint64_t>::iterator isource;

	    if ((isource = _smap.find (ntohl (sender_address.sin_addr.s_addr))) != _smap.end ())
	      ++(isource->second);
	    else
	    {
	      uint64_t ipack = 1;
	      std::pair <unsigned int, uint64_t> p (ntohl (sender_address.sin_addr.s_addr), ipack);
	      _smap.insert (p);
	    }
	  }
	}
      }
    }
    else
    {
      std::this_thread::sleep_for (std::chrono::milliseconds (20));
    }

    std::this_thread::yield ();
  }
}
