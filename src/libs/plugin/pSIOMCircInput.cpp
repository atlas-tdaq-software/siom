// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siomdal/SIOMCircInput.h"
#include "siom/plugin/SIOMCircServer.h"
#include "siom/plugin/pSIOMCircInput.h"

void SIOM::SIOMCircInput::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					  const DalObject *appConfiguration,
					  const DalObject *objConfiguration, bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMCircInput *conf =
    confDB->cast <siomdal::SIOMCircInput> (objConfiguration);

  _port = conf->get_Port ();
  _pollRateFactor = conf->get_PollRateFactor ();
  _pack = conf->get_PackEvents ();

  if (debug)
  {
    std::cout << "Circ server port " << _port << std::endl;
    std::cout << "Poll rate factor " << _pollRateFactor << std::endl;
    std::cout << "Pack events      " << _pack << std::endl;
  }
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMCircInput ();
}

SIOM::SIOMPlugin *createSIOMCircInput ()
{
  return (new SIOM::SIOMCircInput ());
}
