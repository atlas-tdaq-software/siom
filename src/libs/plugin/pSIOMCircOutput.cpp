#include <string>

// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"

#include "DFdal/CircBuffer.h"

// Package include files

#include "siomdal/SIOMCircOutput.h"

#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMCircServerAddress.h"
#include "siom/plugin/SIOMCircDataSender.h"
#include "siom/plugin/pSIOMCircOutput.h"

void SIOM::SIOMCircOutput::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					   const DalObject *appConfiguration,
					   const DalObject *objConfiguration,
					   bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMCircOutput *conf =
    confDB->cast <siomdal::SIOMCircOutput> (objConfiguration);

  const daq::df::CircBuffer *cbuf = conf->get_CircBuffer ();

  int bufsize = cbuf->get_CircSize ();
  unsigned short port = conf->get_Port ();
  std::string name = cbuf->get_CircName ();

  if (debug)
  {
    std::cout << "Server port " << port << std::endl;
    std::cout << "Buffer name " << name << std::endl;
    std::cout << "Buffer size " << bufsize << std::endl;
  }

  _addr = new SIOM::CircServerAddress (name, bufsize, port);
  _dest.address (_addr);
}

void SIOM::SIOMCircOutput::specific_unsetup ()
{
  if (_addr)
  {
    delete _addr;
    _addr = 0;
  }
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMCircOutput ();
}

SIOM::SIOMPlugin *createSIOMCircOutput ()
{
  return (new SIOM::SIOMCircOutput ());
}
