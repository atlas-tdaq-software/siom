// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"

// Package include files

#include "siomdal/SIOMDummyInput.h"
#include "siom/plugin/pSIOMDummyInput.h"

void SIOM::SIOMDummyInput::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					   const DalObject *appConfiguration,
					   const DalObject *objConfiguration,
					   bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  confDB->cast <siomdal::SIOMDummyInput> (objConfiguration);
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMDummyInput ();
}

SIOM::SIOMPlugin *createSIOMDummyInput ()
{
  return (new SIOM::SIOMDummyInput ());
}
