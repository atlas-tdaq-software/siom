// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"

// Package include files

#include "siomdal/SIOMDummyOutput.h"

#include "siom/core/SIOMDataDestination.h"
#include "siom/plugin/pSIOMDummyOutput.h"

void SIOM::SIOMDummyOutput::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					    const DalObject *appConfiguration,
					    const DalObject *objConfiguration,
					    bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  confDB->cast <siomdal::SIOMDummyOutput> (objConfiguration);

  _dest.address (0);
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMDummyOutput ();
}

SIOM::SIOMPlugin *createSIOMDummyOutput ()
{
  return (new SIOM::SIOMDummyOutput ());
}
