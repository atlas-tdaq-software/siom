// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"

// Package include files

#include "siomdal/SIOMDummyProcessor.h"

#include "siom/plugin/SIOMDummyDataProcessor.h"
#include "siom/plugin/pSIOMDummyProcessor.h"

void SIOM::SIOMDummyProcessor::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					       const DalObject *appConfiguration,
					       const DalObject *objConfiguration,
					       bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  confDB->cast <siomdal::SIOMDummyProcessor> (objConfiguration);
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMDummyProcessor ();
}

SIOM::SIOMPlugin *createSIOMDummyProcessor ()
{
  return (new SIOM::SIOMDummyProcessor ());
}
