#include <vector>
#include <string>

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"

// Package include files

#include "siomdal/SIOMEmonInput.h"
#include "siom/plugin/SIOMEmonServer.h"
#include "siom/plugin/pSIOMEmonInput.h"

void SIOM::SIOMEmonInput::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					  const DalObject *appConfiguration,
					  const DalObject *objConfiguration, bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMEmonInput *conf =
    confDB->cast <siomdal::SIOMEmonInput> (objConfiguration);

  const IPCPartition *partition = core_conf->get_partition ();
  _partition_name = partition->name ();

  _sampling_parameters = conf->get_SamplingParameters ();

  if (debug)
  {
    auto buffer_size = _sampling_parameters->get_BufferSize ();
    auto sampler_type = _sampling_parameters->get_SamplerType ();
    auto sampler_names = _sampling_parameters->get_SamplerNames ();

    std::cout << "Partition name " << _partition_name << std::endl;
    std::cout << "Buffer size " << buffer_size << std::endl;
    std::cout << "Sampler type " << sampler_type << std::endl;

    for (auto iname = sampler_names.begin (); iname != sampler_names.end (); ++iname)
      std::cout << "Sampler name " << *iname << std::endl;
  }
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMEmonInput ();
}

SIOM::SIOMPlugin *createSIOMEmonInput ()
{
  return (new SIOM::SIOMEmonInput ());
}
