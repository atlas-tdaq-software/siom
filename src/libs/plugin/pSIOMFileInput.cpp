#include <map>
#include <vector>

// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"

// Package include files

#include "siomdal/SIOMFileInput.h"

#include "siom/plugin/SIOMFileServer.h"
#include "siom/plugin/pSIOMFileInput.h"

void SIOM::SIOMFileInput::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					  const DalObject *appConfiguration,
					  const DalObject *objConfiguration, bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMFileInput *conf =
    confDB->cast <siomdal::SIOMFileInput> (objConfiguration);

  _files = conf->get_FileList ();
  _pack = conf->get_PackEvents ();

  if (debug)
  {
    for (auto ifile = _files.begin (); ifile != _files.end (); ++ifile)
      std::cout << "Input file list " << *ifile << std::endl;
  }
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMFileInput ();
}

SIOM::SIOMPlugin *createSIOMFileInput ()
{
  return (new SIOM::SIOMFileInput ());
}
