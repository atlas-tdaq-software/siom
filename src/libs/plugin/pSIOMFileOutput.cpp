#include <string>

// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"
#include "ipc/partition.h"

// Package include files

#include "siomdal/SIOMFileOutput.h"

#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMFileOutParameters.h"
#include "siom/plugin/SIOMFileDataSender.h"

#include "siom/plugin/pSIOMFileOutput.h"

void SIOM::SIOMFileOutput::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					   const DalObject *appConfiguration,
					   const DalObject *objConfiguration,
					   bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMFileOutput *conf =
    confDB->cast <siomdal::SIOMFileOutput> (objConfiguration);

  std::string activation_flag = conf->get_OutputActivationFlag ();
  std::string file_location = conf->get_FileLocation ();
  std::string file_base_name = conf->get_FileBaseName ();
  int max_size = conf->get_MaxSize ();

  const IPCPartition *partition = core_conf->get_partition ();

  if (debug)
  {
    std::cout << "Activation flag " << activation_flag << std::endl;
    std::cout << "File location " << file_location << std::endl;
    std::cout << "File base name " << file_base_name << std::endl;
    std::cout << "Max size " << max_size << std::endl;
  }

  _addr = new SIOM::FileOutParameters (activation_flag,
				       const_cast <IPCPartition *> (partition),
				       file_location,
				       file_base_name,
				       max_size,
				       core_conf);

  _dest.address (_addr);
}

void SIOM::SIOMFileOutput::specific_unsetup ()
{
  if (_addr)
  {
    delete _addr;
    _addr = 0;
  }
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMFileOutput ();
}

SIOM::SIOMPlugin *createSIOMFileOutput ()
{
  return (new SIOM::SIOMFileOutput ());
}
