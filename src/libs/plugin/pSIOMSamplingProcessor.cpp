// TDAQ include files

#include "config/Configuration.h"
#include "config/DalObject.h"
#include "ipc/partition.h"

// Package include files

#include "siomdal/SIOMSamplingProcessor.h"

#include "siom/core/SIOMAlgoParams.h"
#include "siom/core/SIOMDataProcessor.h"

#include "siom/plugin/SIOMSamplingParameters.h"
#include "siom/plugin/SIOMSamplingDataProcessor.h"
#include "siom/plugin/pSIOMSamplingProcessor.h"

void SIOM::SIOMSamplingProcessor::specific_setup (SIOM::SIOMCoreConfig *core_conf,
						  const DalObject *appConfiguration,
						  const DalObject *objConfiguration,
						  bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMSamplingProcessor *conf = 
    confDB->cast <siomdal::SIOMSamplingProcessor> (objConfiguration);

  std::string sampler_type = conf->get_SamplerType ();
  std::string sampler_name = conf->get_SamplerName ();

  bool add_header = conf->get_AddHeader ();
  uint32_t packet_header = add_header ? conf->get_PacketHeader () : 0;

  unsigned int max_channels = conf->get_MaxChannels ();
  unsigned int inverse_sampling_rate = conf->get_MaxSamplingRate ();

  const IPCPartition *partition = core_conf->get_partition ();
  std::string part_name = partition->name ();

  if (debug)
  {
    std::cout << "Partition name      " << part_name << std::endl;
    std::cout << "Sampler type        " << sampler_type << std::endl;
    std::cout << "Sampler name        " << sampler_name << std::endl;
    std::cout << "Packet header       " << std::hex << packet_header << std::dec << std::endl;
    std::cout << "Max channels        " << max_channels << std::endl;
    std::cout << "Max sampling rate   " << inverse_sampling_rate << std::endl;
  }

  _samplingParameters = new SIOM::SamplingParameters (part_name, sampler_type, sampler_name,
						      packet_header, max_channels, inverse_sampling_rate);
  _params.address (_samplingParameters);
}

void SIOM::SIOMSamplingProcessor::specific_unsetup ()
{
  if (_samplingParameters)
  {
    delete _samplingParameters;
    _samplingParameters = 0;
  }
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMSamplingProcessor ();
}

SIOM::SIOMPlugin *createSIOMSamplingProcessor ()
{
  return (new SIOM::SIOMSamplingProcessor ());
}
