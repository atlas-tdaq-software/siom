// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"

// Package include files

#include "siomdal/SIOMTCPInput.h"
#include "siom/plugin/SIOMTCPServer.h"
#include "siom/plugin/pSIOMTCPInput.h"

void SIOM::SIOMTCPInput::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					 const DalObject *appConfiguration,
					 const DalObject *objConfiguration, bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMTCPInput *conf =
    confDB->cast <siomdal::SIOMTCPInput> (objConfiguration);

  _port = conf->get_Port ();
  _pack = conf->get_PackEvents ();

  if (debug)
  {
    std::cout << "Port number " << _port << std::endl;
    std::cout << "Pack events " << _pack << std::endl;
  }
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMTCPInput ();
}

SIOM::SIOMPlugin *createSIOMTCPInput ()
{
  return (new SIOM::SIOMTCPInput ());
}
