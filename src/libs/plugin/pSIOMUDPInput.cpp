// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"

// Package include files

#include "siomdal/SIOMUDPInput.h"

#include "siom/plugin/SIOMUDPServer.h"
#include "siom/plugin/pSIOMUDPInput.h"

void SIOM::SIOMUDPInput::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					 const DalObject *appConfiguration,
					 const DalObject *objConfiguration, bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMUDPInput *conf =
    confDB->cast <siomdal::SIOMUDPInput> (objConfiguration);

  _port = conf->get_Port ();
  _pack = conf->get_PackEvents ();

  if (debug)
  {
    std::cout << "Port number " << _port << std::endl;
    std::cout << "Pack events " << _pack << std::endl;
  }
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMUDPInput ();
}

SIOM::SIOMPlugin *createSIOMUDPInput ()
{
  return (new SIOM::SIOMUDPInput ());
}
