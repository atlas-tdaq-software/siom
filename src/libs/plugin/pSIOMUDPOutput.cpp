#include <string>

// TDAQ include files

#include "config/Configuration.h"
#include "config/ConfigObject.h"
#include "transport/NetAddress.h"

// Package include files

#include "siomdal/SIOMUDPOutput.h"

#include "siom/core/SIOMDataDestination.h"
#include "siom/plugin/SIOMUDPDataSender.h"

#include "siom/plugin/pSIOMUDPOutput.h"

void SIOM::SIOMUDPOutput::specific_setup (SIOM::SIOMCoreConfig *core_conf,
					  const DalObject *appConfiguration,
					  const DalObject *objConfiguration, bool debug)
{
  Configuration *confDB = core_conf->get_confDB ();
  const siomdal::SIOMUDPOutput *conf =
    confDB->cast <siomdal::SIOMUDPOutput> (objConfiguration);

  std::string server_node = conf->get_ServerNode ();
  unsigned short port = conf->get_Port ();

  if (debug)
  {
    std::cout << "Destination node " << server_node << std::endl;
    std::cout << "Destination port " << port << std::endl;
  }

  _addr = new NetAddress (server_node, port);
  _dest.address (_addr);
}

void SIOM::SIOMUDPOutput::specific_unsetup ()
{
  if (_addr)
  {
    delete _addr;
    _addr = 0;
  }
}

// This is the hook for the plugin factory

extern "C"
{
  extern SIOM::SIOMPlugin *createSIOMUDPOutput ();
}

SIOM::SIOMPlugin *createSIOMUDPOutput ()
{
  return (new SIOM::SIOMUDPOutput ());
}
