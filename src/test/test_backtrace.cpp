#include <unistd.h>
#include <sys/time.h>
#include <signal.h>

#include <iostream>
#include <chrono>
#include <thread>
#include <exception>

#include "siom/core/SIOMActiveObject.h"
#include "siom/core/SIOMBackTrace.h"

// Signal handler to be redefined

struct sigaction oldact_segv;
struct sigaction oldact_abrt;
struct sigaction oldact_quit;
struct sigaction oldact_term;
struct sigaction oldact_fpe;
struct sigaction oldact_sys;

// Test thread

class test_backtrace_Thread : public SIOM::ActiveObject
{
 public:
  test_backtrace_Thread (struct timeval timeout)
    : _timeout (timeout) {this->start ();};

  virtual ~test_backtrace_Thread () {this->join ();};

 private:
  virtual void run ();

 private:
  struct timeval _timeout;
};

void test_backtrace_Thread::run ()
{
  while (! _quit.load ())
  {
    std::this_thread::sleep_for (std::chrono::seconds (_timeout.tv_sec));
    std::this_thread::sleep_for (std::chrono::microseconds (_timeout.tv_usec));
  }
}

bool mystop = false;
test_backtrace_Thread *thr;
struct timeval g_timeout = {0, 100000};

void test_backtrace_SignalHandler (int sig)
{
  struct sigaction *oldact = 0;

  std::cerr << "Got signal " << sig << " parent = " << getppid ()
	    << " pid = " << getpid () << " thread id = " << thr->id () << std::endl;
  SIOM::print_trace ();

  switch (sig)
  {
    case SIGSEGV:
      oldact = &oldact_segv;
      break;
    case SIGABRT:
      oldact = &oldact_abrt;
      break;
    case SIGQUIT:
      oldact = &oldact_quit;
      break;
    case SIGTERM:
      oldact = &oldact_term;
      break;
    case SIGFPE:
      oldact = &oldact_fpe;
      break;
    case SIGSYS:
      oldact = &oldact_sys;
      break;
    default:
      break;
  }

  if (oldact)
    oldact->sa_handler (sig);

  exit (sig);
}

void test_backtrace_help (char *name)
{
  std::cerr << "Usage: " << name << " [options]\n"
	       "\t[-t <tout>]   timeout in seconds, default=0.1\n"
	       "\t[-h]          help\n"
	    << std::endl;
}

int test_backtrace_opt (int argc, char **argv)
{
  int err = 0;
  int i;

  for (i=1; i < argc; ++i)
    if (argv[i][0] == '-')
    {
      float t;

      switch (argv[i][1])
      {
        case 't':
	  t = atof (argv[++i]);
	  g_timeout.tv_sec = (int) t;
	  t -= (float) g_timeout.tv_sec;
	  g_timeout.tv_usec = (int) (t * 1000000);
	  break;
        case 'h':
	  test_backtrace_help (argv[0]);
	  exit (-1);
        default:
	  test_backtrace_help (argv[0]);
	  exit (-1);
      }
    }
    else
    {
      test_backtrace_help (argv[0]);
      exit (-1);
    }

  return err;
}

int test_backtrace_init ()
{
  int err = 0;
  struct sigaction handler;

  // Install signal handler

  handler.sa_handler = test_backtrace_SignalHandler;
  handler.sa_flags = 0;

  sigaction (SIGSEGV, (const struct sigaction *) NULL, &oldact_segv);
  sigaction (SIGSEGV, &handler, (struct sigaction *) NULL);
  sigaction (SIGABRT, (const struct sigaction *) NULL, &oldact_abrt);
  sigaction (SIGABRT, &handler, (struct sigaction *) NULL);
  sigaction (SIGQUIT, (const struct sigaction *) NULL, &oldact_quit);
  sigaction (SIGQUIT, &handler, (struct sigaction *) NULL);
  sigaction (SIGTERM, (const struct sigaction *) NULL, &oldact_term);
  sigaction (SIGTERM, &handler, (struct sigaction *) NULL);
  sigaction (SIGFPE, (const struct sigaction *) NULL, &oldact_fpe);
  sigaction (SIGFPE, &handler, (struct sigaction *) NULL);
  sigaction (SIGSYS, (const struct sigaction *) NULL, &oldact_sys);
  sigaction (SIGSYS, &handler, (struct sigaction *) NULL);

  thr = new test_backtrace_Thread (g_timeout);

  return err;
}

int test_backtrace_loop ()
{
  int err = 0;

  while (! mystop)
  {
    std::this_thread::sleep_for (std::chrono::seconds (g_timeout.tv_sec));
    std::this_thread::sleep_for (std::chrono::microseconds (g_timeout.tv_usec));
  }

  return err;
}

int test_backtrace_end ()
{
  int err = 0;

  printf ("Ending process\n");

  delete thr;
  return err;
}

int main (int argc, char **argv)
{
  int err = 0;

  if ((err = test_backtrace_opt (argc, argv)))
    return err;

  if ((err = test_backtrace_init ()))
    return err;

  if ((err = test_backtrace_loop ()))
    return err;

  if ((err = test_backtrace_end ()))
    return err;

  return err;
}
