#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <list>
#include <string>

#include "ipc/core.h"
#include "config/DalObject.h"

// Package include files

#include "siomdal/SIOMInput.h"
#include "siomdal/SIOMOutput.h"
#include "siomdal/SIOMProcessor.h"

#include "siom/core/SIOMBackTrace.h"
#include "siom/core/SIOMPluginFactory.h"
#include "siom/core/pSIOMInput.h"
#include "siom/core/pSIOMOutput.h"
#include "siom/core/pSIOMProcessor.h"

char *application_name;
SIOM::SIOMCoreConfig *core_conf;

SIOM::MemoryManager <uint8_t> memory_manager;

SIOM::SIOMPluginFactory <SIOM::SIOMInput>     inputFactory;
SIOM::SIOMPluginFactory <SIOM::SIOMOutput>    outputFactory;
SIOM::SIOMPluginFactory <SIOM::SIOMProcessor> processorFactory;

std::list <SIOM::SIOMInput *>      inList;
std::list <SIOM::SIOMOutput *>     outList;
std::list <SIOM::SIOMProcessor *>  procList;

unsigned int nloop = 10;
bool stop = false;

int test_siom_help ()
{
  printf ("Usage: test_siom <options>\n"
          "\t -n <name>         \tapplication name\n"
          "\t -t <nsec>         \trunning time, default = %d sec\n"
          "\t[-h]               \thelp\n",
          nloop);

  return 1;
}

void test_siom_sighandler (int sig)
{
  if (sig == SIGINT || sig == SIGQUIT || sig == SIGTERM)
  {
    printf ("Got signal %d, cleaning up...\n", sig);
    stop = true;
  }
  else if (sig == SIGSEGV || sig == SIGABRT || sig == SIGSYS)
  {
    printf ("Got signal %d, aborting...\n", sig);
    SIOM::print_trace ();
    exit (sig);
  }
}

int test_siom_options (int argc, char **argv)
{
  int err = 0;
  int i;

  for (i=1; i < argc; ++i)
    if (argv[i][0] == '-')
      switch (argv[i][1])
      {
        case 'n':
          application_name = argv[++i];
          break;
        case 't':
	  nloop = atoi (argv[++i]);
          break;
        case 'h':
          err = test_siom_help ();
          break;
        default:
          err = test_siom_help ();
          break;
      }

  // Initialize IPC

  try
  {
    IPCCore::init (argc, argv);
  }

  catch (daq::ipc::Exception &e)
  {
    ers::fatal (e);
    return -1;
  }

  return err;
}

void test_siom_init ()
{
  signal (SIGINT,  test_siom_sighandler);
  signal (SIGQUIT, test_siom_sighandler);
  signal (SIGTERM, test_siom_sighandler);
  signal (SIGSEGV, test_siom_sighandler);
  signal (SIGABRT, test_siom_sighandler);
  signal (SIGSYS,  test_siom_sighandler);

  core_conf = new SIOM::SIOMCoreConfig (application_name);
  core_conf->get_config ();

  for (auto i = core_conf->get_Input ().begin ();
       i != core_conf->get_Input ().end (); ++i)
  {
    std::string className = (*i)->class_name ();

    inputFactory.load (className);
    SIOM::SIOMInput *input = inputFactory.create (className.c_str ());
    input->setup (&memory_manager, core_conf, core_conf->get_appBase (), (*i));

    inList.push_back (input);
  }

  for (auto o = core_conf->get_Output ().begin ();
       o != core_conf->get_Output ().end (); ++o)
  {
    std::string className = (*o)->class_name ();

    outputFactory.load (className);
    SIOM::SIOMOutput *output = outputFactory.create (className.c_str ());
    output->setup (&memory_manager, core_conf, core_conf->get_appBase (), (*o));

    outList.push_back (output);
  }

  for (auto p = core_conf->get_Processors ().begin ();
       p != core_conf->get_Processors ().end (); ++p)
  {
    std::string className = (*p)->class_name ();

    processorFactory.load (className);
    SIOM::SIOMProcessor *processor = processorFactory.create (className.c_str ());
    processor->setup (&memory_manager, core_conf, core_conf->get_appBase (), (*p));

    procList.push_back (processor);
  }
}

void test_siom_configure ()
{
  std::string s;
  const daq::rc::TransitionCmd cmd (s);

  for (auto iin = inList.begin (); iin != inList.end (); ++iin)
    (*iin)->configure (cmd);

  for (auto iout = outList.begin (); iout != outList.end (); ++iout)
    (*iout)->configure (cmd);

  for (auto iproc = procList.begin (); iproc != procList.end (); ++iproc)
    (*iproc)->configure (cmd);

  for (auto iin = inList.begin (); iin != inList.end (); ++iin)
    (*iin)->connect (cmd);

  for (auto iout = outList.begin (); iout != outList.end (); ++iout)
    (*iout)->connect (cmd);

  for (auto iproc = procList.begin (); iproc != procList.end (); ++iproc)
    (*iproc)->connect (cmd);
}

void test_siom_start ()
{
  std::string s;
  const daq::rc::TransitionCmd cmd (s);

  for (auto iin = inList.begin (); iin != inList.end (); ++iin)
    (*iin)->prepareForRun (cmd);

  for (auto iout = outList.begin (); iout != outList.end (); ++iout)
    (*iout)->prepareForRun (cmd);

  for (auto iproc = procList.begin (); iproc != procList.end (); ++iproc)
    (*iproc)->prepareForRun (cmd);
}

void test_siom_stop ()
{
  std::string s;
  const daq::rc::TransitionCmd cmd (s);

  for (auto iin = inList.begin (); iin != inList.end (); ++iin)
    (*iin)->stopDC (cmd);

  for (auto iout = outList.begin (); iout != outList.end (); ++iout)
    (*iout)->stopDC (cmd);

  for (auto iproc = procList.begin (); iproc != procList.end (); ++iproc)
    (*iproc)->stopDC (cmd);
}

void test_siom_unconfigure ()
{
  std::string s;
  const daq::rc::TransitionCmd cmd (s);

  for (auto iin = inList.begin (); iin != inList.end (); ++iin)
    (*iin)->disconnect (cmd);

  for (auto iout = outList.begin (); iout != outList.end (); ++iout)
    (*iout)->disconnect (cmd);

  for (auto iproc = procList.begin (); iproc != procList.end (); ++iproc)
    (*iproc)->disconnect (cmd);

  for (auto iin = inList.begin (); iin != inList.end (); ++iin)
    (*iin)->unconfigure (cmd);

  for (auto iout = outList.begin (); iout != outList.end (); ++iout)
    (*iout)->unconfigure (cmd);

  for (auto iproc = procList.begin (); iproc != procList.end (); ++iproc)
    (*iproc)->unconfigure (cmd);

  for (auto iin = inList.begin (); iin != inList.end (); ++iin)
    (*iin)->unsetup ();

  for (auto iout = outList.begin (); iout != outList.end (); ++iout)
    (*iout)->unsetup ();

  for (auto iproc = procList.begin (); iproc != procList.end (); ++iproc)
    (*iproc)->unsetup ();
}

void test_siom_end ()
{
  for (auto iin = inList.begin (); iin != inList.end (); ++iin)
    delete (*iin);

  for (auto iout = outList.begin (); iout != outList.end (); ++iout)
    delete (*iout);

  for (auto iproc = procList.begin (); iproc != procList.end (); ++iproc)
    delete (*iproc);

  inList.clear ();
  outList.clear ();
  procList.clear ();

  delete core_conf;
}

void test_siom_loop ()
{
  unsigned int iloop;

  for (iloop = 0; iloop < nloop && stop == false; ++iloop)
  {
    std::this_thread::sleep_for (std::chrono::milliseconds (100));

    for (auto iin = inList.begin (); iin != inList.end (); ++iin)
    {
      std::cout << "Input packets " << (*iin)->packets () << std::endl;
      std::cout << "Input MBytes " << (*iin)->mbytes () << std::endl;
    }

    for (auto iout = outList.begin (); iout != outList.end (); ++iout)
    {
      std::cout << "Output packets " << (*iout)->packets () << std::endl;
      std::cout << "Output MBytes " << (*iout)->mbytes () << std::endl;
    }
  }
}

int main (int argc, char **argv)
{
  int err = 0;

  if ((err = test_siom_options (argc, argv)) == 0)
  {
    test_siom_init ();
    test_siom_configure ();
    test_siom_start ();
    test_siom_loop ();
    test_siom_stop ();
    test_siom_unconfigure ();
    test_siom_end ();
  }

  return err;
}
