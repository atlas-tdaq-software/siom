#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include <ctime>
#include <cstdlib>
#include <map>

#include <sys/time.h>
#include <signal.h>

#include "ipc/core.h"
#include "emon/EventSampler.h"

#include "siom/core/SIOMActiveObject.h"
#include "siom/core/SIOMSamplerFactory.h"
#include "siom/core/SIOMMemoryBuffer.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMBackTrace.h"

#define ERR_ERROR      1
#define ERR_NO_ERROR   0

#define DEFAULT_PAGE_NUMBER   1024
#define DEFAULT_PAGE_SIZE     1024
#define DEFAULT_EVENT_SIZE    1280
#define DEFAULT_EVENT_NUMBER     0
#define DEFAULT_MIN_ELEMENTS     0

#define DEFAULT_THREADS       1
#define DEFAULT_BUFFERS       1

#define DEFAULT_KEY           "siom"
#define DEFAULT_PARTITION     "test"
#define DEFAULT_CHANNELS      10

static bool stop = false;
static bool check = false;
static bool printout = false;
static bool append = false;
static bool debug = false;
static bool header = false;
static bool sample = false;

static unsigned int s_runTime         = 0;

static unsigned int s_poolPageNumber  = DEFAULT_PAGE_NUMBER;
static unsigned int s_poolPageSize    = DEFAULT_PAGE_SIZE;
static unsigned int s_eventSize       = DEFAULT_EVENT_SIZE;
static unsigned int s_maxEvents       = DEFAULT_EVENT_NUMBER;
static unsigned int s_minElements     = DEFAULT_MIN_ELEMENTS;

static std::string s_key              = DEFAULT_KEY;
static std::string s_partitionName    = DEFAULT_PARTITION;
static unsigned int s_maxChannels     = DEFAULT_CHANNELS;

static const uint32_t s_header = 0xee1234ee;

std::map <unsigned int, uint8_t *> test_map;

class TestBufferInputThread : public SIOM::ActiveObject
{
 public:
  explicit TestBufferInputThread (SIOM::MemoryPool <uint8_t> *p)
    :_validated (0), _full (0), _pool (p)
  {
    this->start ();
  };

  virtual ~TestBufferInputThread ()
  {
    this->join ();
  };

  unsigned int validated () {return _validated;};
  unsigned int full () {return _full;};

 private:
  virtual void run ();

 private:
  unsigned int _event_size;
  unsigned int _validated;
  unsigned int _full;

  SIOM::MemoryPool <uint8_t> *_pool;
};

class TestBufferOutputThread : public SIOM::ActiveObject
{
 public:
  explicit TestBufferOutputThread (SIOM::MemoryPool <uint8_t> *p)
    :_pool (p)
  {
    this->start ();
  };

  virtual ~TestBufferOutputThread ()
  {
    this->join ();
  };

 private:
  virtual void run ();

 private:
  SIOM::MemoryPool <uint8_t> *_pool;
};

// Thread implementation

void TestBufferInputThread::run ()
{
  uint8_t *tbuf = 0;
  size_t evsize = s_eventSize;

  auto test_map_elem = test_map.begin ();

  while (! _quit.load ())
  {
    if (s_maxEvents == 0 || _validated < s_maxEvents)
    {
      if (append)
      {
        tbuf = (*test_map_elem).second;
        evsize = sizeof (tbuf);
      }

      if (auto b = _pool->reserve (evsize))
      {
	if (append)
        {
	  b->append (tbuf, evsize);

          if (++test_map_elem == test_map.end ())
            test_map_elem = test_map.begin ();
        }

	_pool->validate (b, evsize);
	++_validated;
      }
      else
	++_full;
    }

    std::this_thread::yield ();
  }
}

void TestBufferOutputThread::run ()
{
  while (! _quit.load ())
  {
    if (_pool->elements () > s_minElements)
    {
      if (auto b = _pool->get_next ())
      {
	if (check)
	{
	  auto pages = b->get_pages ();

	  auto ipage = pages.begin ();
	  uint8_t *p = *ipage;
	  unsigned int *pint = reinterpret_cast <unsigned int *> (p);

	  uint8_t *test_buf_elem = test_map[*pint];
	  size_t test_buf_len = sizeof (test_buf_elem);

	  if (test_buf_len != b->n_objects ())
	    std::cerr << "Error: inconsistent buffer length: expected "
		      << test_buf_len << " found " << b->n_objects ();
	  else
	  {
	    for ( ; ipage != pages.end () && test_buf_len > 0; ++ipage)
	    {
	      p = *ipage;
	      size_t lencmp = test_buf_len < _pool->page_size () ? test_buf_len : _pool->page_size ();

	      if (memcmp (p, test_buf_elem, lencmp) != 0)
		std::cerr << "Error: inconsistent buffer" << std::endl;

	      if (debug)
		for (unsigned int ielem = 0; ielem < lencmp; ++ielem)
		  std::cout << static_cast <unsigned int> (*(p + ielem)) << " "
			    << static_cast <unsigned int> (*(test_buf_elem + ielem)) << std::endl;

	      test_buf_len -= lencmp;
	      test_buf_elem += lencmp;
	    }
	  }
	}

	_pool->release (b);
      }
    }

    std::this_thread::yield ();
  }
}

unsigned int ti = DEFAULT_THREADS, to = DEFAULT_THREADS, nb = DEFAULT_BUFFERS;
struct timeval t0;

std::vector <TestBufferInputThread *> thrlist_i;
std::vector <TestBufferOutputThread *> thrlist_o;

std::vector <SIOM::MemoryPool <uint8_t>* > pools;

emon::EventSampler *event_sampler;
SIOM::SIOMSamplerFactory *sampler_factory;

void test_siom_buffer_sighandler (int sig)
{
  if (sig == SIGINT || sig == SIGQUIT || sig == SIGTERM)
  {
    printf ("Got signal %d, cleaning up...\n", sig);
    stop = true;
  }
  else if (sig == SIGSEGV || sig == SIGABRT || sig == SIGSYS)
  {
    printf ("Got signal %d, aborting...\n", sig);
    SIOM::print_trace ();
    exit (sig);
  }
}

void test_siom_buffer_help (char *pname)
{
  unsigned int min_page_size = 2 * sizeof (unsigned int);

  printf ("Usage: %s <options>\n"
          "\t[-i <threads>]      \tnumber of input threads, default=%d\n"
          "\t[-o <threads>]      \tnumber of output threads, default=%d\n"
          "\t[-n <nbuffer>]      \twork on <nbuffer> buffers, default=%d\n"
          "\t[-p <npages>]       \tuse npages buffer pages, default=%d\n"
          "\t[-s <pagesize>]     \tpage size, default=%d (must be at least %d)\n"
          "\t[-e <eventsize>]    \tevent size, default=%d\n"
          "\t[-m <eventnum>]     \tmax event number, default=<infinite>\n"
          "\t[-q <minelements>]  \tmin event number in memory pool, default=%d\n"
	  "\t[-t <seconds>]      \trun seconds and then stop, default=<run forever>\n"
	  "\t[-k <key>]          \tkey for sampling address, default=%s\n"
	  "\t[-P <partition>]    \tpartition for sampling, default=%s\n"
	  "\t[-C <max channels>] \tmaximum number of sampling channels, default=%d\n"
          "\t[-a]                \ttest append method\n"
	  "\t[-v]                \tverbose printout\n"
          "\t[-c]                \tcheck data\n"
          "\t[-d]                \tdebug (printout output data)\n"
	  "\t[-S]                \tstart sampling\n"
          "\t[-h]                \thelp\n",
          pname, DEFAULT_THREADS, DEFAULT_THREADS, DEFAULT_BUFFERS,
	  DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, min_page_size, DEFAULT_EVENT_SIZE,
	  DEFAULT_MIN_ELEMENTS, DEFAULT_KEY, DEFAULT_PARTITION, DEFAULT_CHANNELS);

  exit (0);
}

void test_siom_buffer_options (int argc, char **argv)
{
  int i;

  for (i=1; i < argc; ++i)
    if (argv[i][0] == '-')
      switch (argv[i][1])
      {
        case 'i':
          ti = (unsigned short) atoi (argv[++i]);
          break;
        case 'o':
          to = (unsigned short) atoi (argv[++i]);
          break;
        case 'n':
          nb = (unsigned short) atoi (argv[++i]);
          break;
        case 'p':
          s_poolPageNumber = atoi (argv[++i]);
          break;
        case 's':
          s_poolPageSize = atoi (argv[++i]);
          break;
        case 'e':
          s_eventSize = (unsigned int) atoi (argv[++i]);
          break;
        case 'm':
          s_maxEvents = (unsigned int) atoi (argv[++i]);
          break;
        case 'q':
          s_minElements = (unsigned int) atoi (argv[++i]);
          break;
        case 'v':
	  printout = true;
          break;
        case 'a':
	  append = true;
          break;
        case 't':
          s_runTime = (unsigned int) atoi (argv[++i]);
          break;
        case 'k':
          s_key = argv[++i];
          break;
        case 'P':
          s_partitionName = argv[++i];
          break;
        case 'C':
          s_maxChannels = (unsigned int) atoi (argv[++i]);
          break;
        case 'c':
          check = true;
          break;
        case 'S':
          sample = true;
          break;
        case 'd':
          debug = true;
          break;
        case 'h':
        default:
          test_siom_buffer_help (argv[0]);
          break;
      }
    else
      test_siom_buffer_help (argv[0]);

  if (s_poolPageSize < 2 * sizeof (unsigned int))
    test_siom_buffer_help (argv[0]);

  append = append || check;
}

void test_siom_buffer_set_handlers ()
{
  signal (SIGINT,  test_siom_buffer_sighandler);
  signal (SIGQUIT, test_siom_buffer_sighandler);
  signal (SIGTERM, test_siom_buffer_sighandler);
  signal (SIGSEGV, test_siom_buffer_sighandler);
  signal (SIGABRT, test_siom_buffer_sighandler);
  signal (SIGSYS,  test_siom_buffer_sighandler);
}

void test_siom_buffer_init_events (SIOM::MemoryPool <uint8_t> *pool)
{
  if (append)
  {
    // Prepare buffers for check

    srand (static_cast <unsigned int> (time (0)));
    unsigned int test_buffer_num = s_poolPageNumber + (static_cast <double> (rand ()) / RAND_MAX) * s_poolPageNumber;

    for (unsigned int i = 0; i < test_buffer_num; ++i)
    {
      size_t real_evsize = s_poolPageSize / 2. + (static_cast <double> (rand ()) / RAND_MAX) * s_poolPageSize;

      if (real_evsize < sizeof (unsigned int))
        continue;

      uint8_t *test_data = new uint8_t[real_evsize];
      unsigned int *test_data_int = reinterpret_cast <unsigned int *> (test_data);

      // Generate random data as unsigned int (last bytes may be left uninitialised)

      for (unsigned int j = 0; j < real_evsize / sizeof (unsigned int); ++j)
        test_data_int[j] = rand ();

      std::pair <unsigned int, uint8_t *> test_data_elem (test_data_int[0], test_data);
      auto p = test_map.insert (test_data_elem);

      if (! p.second)
      {
        --i;
        continue;
      }
    }

    if (debug)
      std::cerr << "Buffers for check initialised, number = " << test_buffer_num << std::endl;
  }
}

int test_siom_buffer_init (int argc, char **argv)
{
  test_siom_buffer_set_handlers ();

  try
  {
    for (unsigned int j=0; j < nb; ++j)
    {
      // Create memory pools

      auto p = new SIOM::MemoryPool <uint8_t> (s_poolPageSize, s_poolPageNumber);
      pools.push_back (p);

      // Prepare pages to test append method and for event check

      test_siom_buffer_init_events (p);

      for (unsigned int i=0; i < to; ++i)
      {
	TestBufferOutputThread *out = new TestBufferOutputThread (p);
	thrlist_o.push_back (out);
      }

      for (unsigned int i=0; i < ti; ++i)
      {
	// Start input threads associated to the current pool

        TestBufferInputThread *in = new TestBufferInputThread (p);
	thrlist_i.push_back (in);
      }
    }

    if (sample)
    {
      IPCCore::init (argc, argv);

      const uint32_t *h_pointer = &s_header;
      sampler_factory = new SIOM::SIOMSamplerFactory (reinterpret_cast <char *> (&h_pointer),
						      header ? sizeof (uint32_t) : 0);

      for (auto p = pools.begin (); p != pools.end (); ++p)
	sampler_factory->add_pool (*p);

      std::string value = "test";

      const IPCPartition partition (s_partitionName);
      emon::SamplingAddress address (s_key, value);

      event_sampler = new emon::EventSampler (partition, address, sampler_factory,
       					      static_cast <size_t> (s_maxChannels));
    }
  }

  catch (daq::ipc::Exception &e)
  {
    ers::error (e);
    return ERR_ERROR;
  }

  catch (std::exception &e)
  {
    return ERR_ERROR;
  }

  gettimeofday (&t0, (struct timezone *) NULL);

  return ERR_NO_ERROR;
}

void test_siom_buffer_loop ()
{
  unsigned int s = 0;

  while (!stop)
  {
    std::this_thread::sleep_for (std::chrono::seconds (1));

    if (printout)
    {
      unsigned int r = 0;
      unsigned int f = 0;
      unsigned int p = 0;
      unsigned int e = 0;

      for (auto it = thrlist_i.begin (); it != thrlist_i.end (); ++it)
      {
	r += (*it)->validated ();
	f += (*it)->full ();
      }

      for (auto ip = pools.begin (); ip != pools.end (); ++ip)
      {
	p += (*ip)->free_pages ();
	e += (*ip)->elements ();
      }

      std::cout << " Validated = " << r
		<< " Full = " << f
		<< " Free pages " << p
		<< " Elements " << e << std::endl;
    }

    if (++s == s_runTime)
      break;

    if (s_maxEvents != 0)
    {
      bool cont = true;

      for (auto it = thrlist_i.begin (); it != thrlist_i.end (); ++it)
	cont = cont && ((*it)->validated () < s_maxEvents);

      if (!cont)
	break;
    }
  }
}

void test_siom_buffer_end ()
{
  // Stop threads

  unsigned int r = 0, f = 0;

  for (auto it = thrlist_i.begin (); it != thrlist_i.end (); ++it)
  {
    r += (*it)->validated ();
    f += (*it)->full ();
  }

  struct timeval t;
  gettimeofday (&t, (struct timezone *) NULL);

  if (sample)
    delete event_sampler;

  for (auto it = thrlist_i.begin (); it != thrlist_i.end (); ++it)
    delete (*it);

  for (auto ot = thrlist_o.begin (); ot != thrlist_o.end (); ++ot)
    delete (*ot);

  thrlist_i.clear ();
  thrlist_o.clear ();

  // Delete memory pools

  for (auto ip = pools.begin (); ip != pools.end (); ++ip)
    delete (*ip);

  pools.clear ();

  // Delete test buffers

  for (auto imap = test_map.begin (); imap != test_map.end (); ++imap)
    delete (*imap).second;

  test_map.clear ();

  // Print statistics

  float dt = (float) (t.tv_sec - t0.tv_sec) +
             (float) (t.tv_usec - t0.tv_usec) / 1000000;
  float rate = (float) r / dt;

  std::cout << r << " packets validated in "
	    << dt << " seconds, rate = " << rate << std::endl
	    << f << " buffer full count" << std::endl;
}

int main (int argc, char **argv)
{
  int err = ERR_NO_ERROR;

  test_siom_buffer_options (argc, argv);

  err = test_siom_buffer_init (argc, argv);

  if (err == ERR_NO_ERROR)
    test_siom_buffer_loop ();

  test_siom_buffer_end ();

  return err;
}
