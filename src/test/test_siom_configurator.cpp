#include <stdlib.h>
#include <string.h>

#include <string>

#include "config/DalObject.h"

// Package include files

#include "siomdal/SIOMInput.h"
#include "siomdal/SIOMOutput.h"

#include "siom/core/SIOMCoreConfig.h"
#include "siom/core/SIOMPluginFactory.h"
#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/pSIOMInput.h"
#include "siom/core/pSIOMOutput.h"

char *application_name;
SIOM::SIOMCoreConfig *core_conf;

SIOM::SIOMPluginFactory <SIOM::SIOMInput>     inputFactory;
SIOM::SIOMPluginFactory <SIOM::SIOMOutput>    outputFactory;

std::list <SIOM::SIOMInput *>  inList;
std::list <SIOM::SIOMOutput *> outList;

SIOM::MemoryManager <uint8_t> memory_manager;

void test_siom_configurator_help (char *pname)
{
  printf ("Usage: %s <options>\n"
          "\t -n <name>         \tapplication name\n"
          "\t[-h]               \thelp\n",
          pname);
}

void test_siom_configurator_options (int argc, char **argv)
{
  int i;

  for (i=1; i < argc; ++i)
    if (argv[i][0] == '-')
      switch (argv[i][1])
      {
        case 'n':
          application_name = argv[++i];
          break;
        case 'h':
        default:
          test_siom_configurator_help (argv[0]);
	  exit (0);
      }
}

void test_siom_configurator_init ()
{
  core_conf = new SIOM::SIOMCoreConfig (application_name);
}

void test_siom_configurator_end ()
{
  delete core_conf;
}

void test_siom_configurator_loop ()
{
  core_conf->get_config ();

  for (auto i = core_conf->get_Input ().begin ();
       i != core_conf->get_Input ().end (); ++i)
  {
    std::string className = (*i)->class_name ();

    inputFactory.load (className);
    SIOM::SIOMInput *input = inputFactory.create (className.c_str ());
    input->setup (&memory_manager, core_conf, core_conf->get_appBase (), (*i));

    inList.push_back (input);
  }

  for (auto o = core_conf->get_Output ().begin ();
       o != core_conf->get_Output ().end (); ++o)
  {
    std::string className = (*o)->class_name ();

    outputFactory.load (className);
    SIOM::SIOMOutput *output = outputFactory.create (className.c_str ());
    output->setup (&memory_manager, core_conf, core_conf->get_appBase (), (*o));

    outList.push_back (output);
  }

  for (auto iin = inList.begin (); iin != inList.end (); ++iin)
  {
    (*iin)->unsetup ();
    delete (*iin);
  }

  for (auto iout = outList.begin (); iout != outList.end (); ++iout)
  {
    (*iout)->unsetup ();
    delete (*iout);
  }

  inList.clear ();
  outList.clear ();
}

int main (int argc, char **argv)
{
  test_siom_configurator_options (argc, argv);
  test_siom_configurator_init ();
  test_siom_configurator_loop ();
  test_siom_configurator_end ();

  return 0;
}
