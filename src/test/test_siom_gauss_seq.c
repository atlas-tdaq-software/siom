#include <stdio.h>
#include <stdlib.h>

#include "siom/apps/genseq.h"

#define DEF_MEAN    0.5
#define DEF_SIGMA   0.5
#define DEF_SEQLEN  1000

int seqlen = DEF_SEQLEN;

float mean  = DEF_MEAN;
float sigma = DEF_SIGMA;

void test_gauss_seq_help (char **argv)
{
  int n = DEF_SEQLEN;
  float m = DEF_MEAN;
  float s = DEF_SIGMA;

  printf ("Usage: %s [options]\n"
          "\t[-n <seqlen>]\tsequence length, default=%d\n"
          "\t[-m <mean>]  \tgaussian mean, default=%f\n"
          "\t[-s <sigma>] \tgaussian sigma, default=%f\n"
          "\t[-h]         \thelp\n",
          argv[0], n, m, s);
}

int test_gauss_seq_args (int argc, char **argv)
{
  int i;

  for (i=1; i < argc; ++i)
    if (argv[i][0] == '-')
      switch (argv[i][1])
      {
        case 'n':
	  seqlen = atoi (argv[++i]);
	  break;
        case 'm':
	  mean = atof (argv[++i]);
	  break;
        case 's':
	  sigma = atof (argv[++i]);
	  break;
        case 'h':
	  test_gauss_seq_help (argv);
	  return -1;
        default:
	  test_gauss_seq_help (argv);
	  return -2;
      }

  return 0;
}

int main (int argc, char **argv)
{
  int err = 0;
  float *seq;

  int i;

  if ((err = test_gauss_seq_args (argc, argv)) < 0)
    return (err);

  if ((seq = (float *) malloc (seqlen * sizeof (float))) == (float *) NULL)
  {
    printf ("Error allocating sequence\n");
    return -3;
  }

  if (gen_gauss_seq (seq, seqlen, mean, sigma) != seqlen)
  {
    printf ("Error in gen_gauss_seq\n");
    return -4;
  }

  for (i=0; i < seqlen; ++i)
    printf ("%f\n", seq[i]);

  free (seq);

  return err;
}
