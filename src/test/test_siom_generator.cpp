#include "siom/apps/generator.h"

int main (int argc, char **argv)
{
  int errcode = GENERATOR_ERR_NOERR;

  if ((errcode = generator_args (argc, argv)) != GENERATOR_ERR_NOERR)
    return errcode;

  if ((errcode = generator_init ()) != GENERATOR_ERR_NOERR)
    return errcode;

  generator_set_running (true);

  if ((errcode = generator_loop ()) != GENERATOR_ERR_NOERR)
    return errcode;

  if ((errcode = generator_end ()) != GENERATOR_ERR_NOERR)
    return errcode;

  return errcode;
}
