#include <iostream>
#include <sstream>
#include <vector>
#include <thread>
#include <chrono>
#include <set>
#include <string>
#include <random>

#include "siom/core/SIOMActiveObject.h"
#include "siom/core/SIOMMemoryManager.h"

#define ERR_ERROR      1
#define ERR_NO_ERROR   0

#define DEFAULT_POOL_NUMBER   5
#define DEFAULT_THREAD_NUMBER 8
#define DEFAULT_RUN_TIME      300
#define DEFAULT_PAGE_NUMBER   1024
#define DEFAULT_PAGE_SIZE     1024

static unsigned int s_nPools          = DEFAULT_POOL_NUMBER;
static unsigned int s_nThreads        = DEFAULT_THREAD_NUMBER;
static unsigned int s_runTime         = DEFAULT_RUN_TIME;
static unsigned int s_poolPageNumber  = DEFAULT_PAGE_NUMBER;
static unsigned int s_poolPageSize    = DEFAULT_PAGE_SIZE;

class TestMemoryManagerThread : public SIOM::ActiveObject
{
 public:
  TestMemoryManagerThread (SIOM::MemoryManager <uint8_t> *mm,
			   std::set <std::string> &pool_names)
    : _manager (mm), _pnames (pool_names)
  {
    this->start ();
  };

  virtual ~TestMemoryManagerThread ()
  {
    this->join ();
  };

 private:
  virtual void run ();

 private:
  SIOM::MemoryManager <uint8_t> *_manager;
  std::set <std::string> _pnames;
};

void TestMemoryManagerThread::run ()
{
  auto t0 = std::chrono::high_resolution_clock::now ();

  std::mt19937_64 engine (static_cast<uint64_t> (t0.time_since_epoch ().count ()));
  std::uniform_real_distribution <double> oneToTen (1.0, 10.0);
  std::uniform_real_distribution <double> zeroToOne (0.0, 1.0);

  while (! _quit.load ())
  {
    // Wait for a random time between 1 and 10 seconds

    double f_interval = oneToTen (engine) * 1000.;
    int i_interval = static_cast <int> (f_interval);

    std::this_thread::sleep_for (std::chrono::milliseconds (i_interval));

    // Get a random pool and decide whether to create/get or destroy a pool

    double fran = zeroToOne (engine);
    int iran = static_cast <int> (fran * _pnames.size ());

    std::set <std::string>::iterator iname = _pnames.begin ();

    for (int i=0; i < iran; ++i)
      ++iname;

    // Remove or get/create the pool

    std::string pool_name = *iname;
    double fcreator = zeroToOne (engine);

    if (fcreator > 0.5)
    {
      std::cout << "Pool " << pool_name << " retrieve" << std::endl;
      _manager->create_pool (pool_name, s_poolPageSize, s_poolPageNumber);
    }
    else
    {
      std::cout << "Pool " << pool_name << " remove" << std::endl;
      _manager->delete_pool (pool_name);
    }
  }
}

std::set <std::string> pool_names;
SIOM::MemoryManager <uint8_t> *memory_manager;
std::vector <TestMemoryManagerThread *> thrlist;

void test_siom_memory_manager_help (char *pname)
{
  unsigned int min_page_size = 2 * sizeof (unsigned int);

  printf ("Usage: %s <options>\n"
          "\t[-t <threads>]   \tnumber of threads, default=%d\n"
          "\t[-n <npools>]    \tnumber of pools per memory manager, default=%d\n"
          "\t[-r <runtime>]   \trun time in seconds, default=%d\n"
          "\t[-p <npages>]    \tuse npages buffer pages, default=%d\n"
          "\t[-s <pagesize>]  \tpage size, default=%d (must be at least %d)\n"
          "\t[-h]             \thelp\n",
          pname, DEFAULT_THREAD_NUMBER, DEFAULT_POOL_NUMBER, DEFAULT_RUN_TIME,
	  DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE, min_page_size);

  exit (0);
}

void test_siom_memory_manager_options (int argc, char **argv)
{
  int i;

  for (i=1; i < argc; ++i)
    if (argv[i][0] == '-')
      switch (argv[i][1])
      {
        case 't':
          s_nThreads = static_cast <unsigned int> (atoi (argv[++i]));
          break;
        case 'n':
          s_nPools = static_cast <unsigned int> (atoi (argv[++i]));
          break;
        case 'p':
          s_poolPageNumber = static_cast <unsigned int> (atoi (argv[++i]));
          break;
        case 's':
          s_poolPageSize = static_cast <unsigned int> (atoi (argv[++i]));
          break;
        case 'r':
          s_runTime = static_cast <unsigned int> (atoi (argv[++i]));
          break;
        case 'h':
        default:
          test_siom_memory_manager_help (argv[0]);
          break;
      }
    else
      test_siom_memory_manager_help (argv[0]);

  if (s_poolPageSize < 2 * sizeof (unsigned int))
    test_siom_memory_manager_help (argv[0]);
}

int test_siom_memory_manager_init ()
{
  for (unsigned int i=0; i < s_nPools; ++i)
  {
    std::ostringstream pname;

    pname << "poolName_" << i;
    pool_names.insert (pname.str ());
  }

  try
  {  
    memory_manager = new SIOM::MemoryManager <uint8_t> ();

    for (unsigned int i=0; i < s_nThreads; ++i)
    {
      TestMemoryManagerThread *t = new TestMemoryManagerThread (memory_manager, pool_names);
      thrlist.push_back (t);
    }
  }

  catch (std::exception &e)
  {
    return ERR_ERROR;
  }

  return ERR_NO_ERROR;
}

void test_siom_memory_manager_loop ()
{
  unsigned int s = 0;

  while (s < s_runTime)
  {
    std::this_thread::sleep_for (std::chrono::seconds (1));

    for (auto ip = pool_names.begin (); ip != pool_names.end (); ++ip)
    {
      std::string pool_name = *ip;
      bool allocated = memory_manager->get_pool (pool_name) == 0 ? false: true;
      std::cout << "Pool : " << *ip << " allocation status : " << allocated << std::endl;
    }

    ++s;
  }
}

void test_siom_memory_manager_end ()
{
  // Stop threads

  for (auto it = thrlist.begin (); it != thrlist.end (); ++it)
    delete (*it);

  thrlist.clear ();

  // Check pools

  for (auto ip = pool_names.begin (); ip != pool_names.end (); ++ip)
  {
    std::string pool_name = *ip;
    bool allocated = memory_manager->get_pool (pool_name) == 0 ? false: true;
    std::cout << "Pool : " << *ip << " allocation status : " << allocated << std::endl;
  }

  // Delete memory manager

  delete memory_manager;
}

int main (int argc, char **argv)
{
  int err = ERR_NO_ERROR;

  test_siom_memory_manager_options (argc, argv);

  err = test_siom_memory_manager_init ();

  if (err == ERR_NO_ERROR)
    test_siom_memory_manager_loop ();

  test_siom_memory_manager_end ();

  return err;
}
