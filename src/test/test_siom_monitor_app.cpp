#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <iostream>
#include <string>
#include <vector>

#include <emon/EventIterator.h>
#include <emon/SelectionCriteria.h>
#include <cmdl/cmdargs.h>
#include <owl/timer.h>
#include <ipc/core.h>

#include <rcc_time_stamp/tstamp.h>

/* error codes */

#define TEST_MONITOR_ERR_NOERR       0
#define TEST_MONITOR_ERR_ARGS       -1
#define TEST_MONITOR_ERR_SIGINST    -2
#define TEST_MONITOR_ERR_INIT       -3

/* default parameters */

#define DEFAULT_NEV          -1
#define DEFAULT_ASYNCH        false
#define DEFAULT_SLEEP         3
#define DEFAULT_DELAY         0
#define DEFAULT_VERBOSITY     0
#define DEFAULT_PARTITION     ""
#define DEFAULT_BUFLIMIT      1024
#define DEFAULT_TIMEOUT       3000

/* global library variables */

bool                       g_asynch     = DEFAULT_ASYNCH;
int                        g_nev        = DEFAULT_NEV;
int                        g_buflimit   = DEFAULT_BUFLIMIT;
int                        g_verbosity  = DEFAULT_VERBOSITY;
int                        g_timeout    = DEFAULT_TIMEOUT;

unsigned int               g_delay      = DEFAULT_DELAY;

std::string                g_partition  = DEFAULT_PARTITION;
std::vector <std::string>  g_keys;
std::vector <std::string>  g_values;

bool gotSignal = false;
int signum;
long eventCount = 0;
long nyield = 0;
float elapsed = 0;
OWLTimer timer;

/* event iterator */

std::vector <emon::EventIterator *> g_it;

void test_monitor_sighandler (int sig)
{
  gotSignal = true;
  std::cout << "Sampler got signal ..." << sig << std::endl;
  signum = sig;
}

void test_monitor_help (char *progname)
{
  fprintf (stderr,
	   "Usage: %s [options]\n"
	   "\t-p <part>     partition to run in\n"
	   "\t-k <key>      key (multiple option)\n"
	   "\t-v <value>    value (multiple option)\n"
	   "\t[-a]          run in non-blocking mode\n"
	   "\t[-b <buf>]    buffer limit, default=%d\n"
	   "\t[-t <tout>]   timeout, default=%d (usec)\n"
	   "\t[-n <nev>]    event number, default=forever\n"
	   "\t[-d <del>]    processing delay, default=%d (msec)\n"
	   "\t[-V <level>]  verbosity level, default=%d\n"
	   "\t[-h]          help\n",
	   progname, DEFAULT_BUFLIMIT, DEFAULT_TIMEOUT, DEFAULT_DELAY,
	   DEFAULT_VERBOSITY);
}

int test_monitor_opt (int argc, char **argv)
{
  int errcode = TEST_MONITOR_ERR_NOERR;
  int i;

  std::string key;
  std::string value;

  for (i=1; i < argc; ++i)
    if (argv[i][0] == '-')
      switch (argv[i][1])
      {
        case 'p':
	  g_partition = argv[++i];
	  break;
        case 'a':
	  g_asynch = true;
	  break;
        case 'k':
	  key = argv[++i];
	  g_keys.push_back (key);
	  break;
        case 'v':
	  value = argv[++i];
	  g_values.push_back (value);
	  break;
        case 'b':
	  g_buflimit = atoi (argv[++i]);
	  break;
        case 't':
	  g_timeout = atoi (argv[++i]);
	  break;
        case 'n':
	  g_nev = atoi (argv[++i]);
	  break;
        case 'd':
	  g_delay = atoi (argv[++i]);
	  break;
        case 'V':
	  g_verbosity = atoi (argv[++i]);
	  break;
        case 'h':
	  test_monitor_help (argv[0]);
	  exit (TEST_MONITOR_ERR_NOERR);
        default:
	  test_monitor_help (argv[0]);
	  exit (TEST_MONITOR_ERR_ARGS);
      }
    else
    {
      test_monitor_help (argv[0]);
      exit (TEST_MONITOR_ERR_ARGS);
    }

  return errcode;
}

int test_monitor_init (int argc, char **argv)
{
  int errcode = TEST_MONITOR_ERR_NOERR;
  TS_ErrorCode_t terr;

  /* Install termination signal handler */

  if (signal (SIGINT, test_monitor_sighandler) == SIG_ERR)
    return TEST_MONITOR_ERR_SIGINST;

  if (signal (SIGTERM, test_monitor_sighandler) == SIG_ERR)
    return TEST_MONITOR_ERR_SIGINST;

  /* Init IPC */

  IPCCore::init (argc, argv);
  IPCPartition p (g_partition);

  /* Define selection criteria as all */

  const std::string stream_type;
  const std::vector <std::string> stream_names;
  const std::vector <unsigned short> bit_positions;

  emon::SmartBitValue     l1_bits (bit_positions, emon::logic::IGNORE);
  emon::SmartStreamValue  s_value (stream_type, stream_names, emon::logic::IGNORE);

  emon::L1TriggerType l1_type (0, true);
  emon::StatusWord    s_word  (0, true);

  emon::SelectionCriteria criteria (l1_type, l1_bits, s_value, s_word);

  if (g_keys.size () != g_values.size ())
  {
    std::cout << " [test_monitor] Number of keys and values mismatch!"
	      << std::endl;
    return TEST_MONITOR_ERR_ARGS;
  }

  /* Select and at the EventSampler(s) */

  try
  {
    for (unsigned int i = 0; i < g_keys.size (); ++i)
    {
      emon::EventIterator *it;
      emon::SamplingAddress address (g_keys[i], g_values[i]);

      // Select and at the EventSampler

      it = new emon::EventIterator (p, address, criteria, g_buflimit);
      g_it.push_back (it);
    }
  }

  catch (emon::Exception &e)
  {
    std::cerr << OWLTime() << " [test_monitor] Error: Cannon Initialize "
	      << std::endl;
    ers::fatal (e);

    return TEST_MONITOR_ERR_INIT;
  }

  /* open ts library */

  if ((terr = ts_open (1, TS_DUMMY)) != TSE_OK)
  {
    std::cout << " [test_monitor]: Error in opening ts library" << std::endl;
    return TEST_MONITOR_ERR_INIT;
  }

  return errcode;
}

int test_monitor_loop ()
{
  int errcode = TEST_MONITOR_ERR_NOERR;

  std::cout << OWLTime() << " [test_monitor] EventMonitor started with "
	    << g_delay << "ms delay..." << std::endl;

  /* Start the timer */

  timer.start ();

  while (eventCount != g_nev && (! gotSignal))
  {
    tstamp t1, t2;
    emon::Event event;

    for (auto it = g_it.begin (); it != g_it.end (); ++it)
    {
      ts_clock (&t1);

      try
      {
        if (g_asynch)
	  event = (*it)->tryNextEvent ();
        else
	  event = (*it)->nextEvent (g_timeout);
      }

      catch (emon::NoMoreEvents &ex)
      {
        if (!g_asynch)
	  std::cerr << OWLTime() 
	            << " [test_monitor] Error: EventBuffer empty" << std::endl;

	++nyield;
        omni_thread::yield ();

        continue;
      }

      catch (emon::SamplerStopped &ex)
      {
        std::cerr << OWLTime()
		  << " [test_monitor] Error: EventSampler stopped sampling"
		  << std::endl;
        break;
      }
 
      eventCount++;

      if (g_verbosity > 0)
        std::cout << "Event count = " << eventCount
		  << " Event size = " << event.size () << std::endl;

      if (g_verbosity > 1)
      {
        std::cout << "\tEvent data : " ;

        const unsigned int *data = event.data ();

        for (unsigned int i = 0;
	     i < (event.size () < 30 ? event.size () : 30 ); ++i)
	  std::cout << data[i] << " ";

        std::cout << std::endl;
      }

      ts_clock (&t2);
      elapsed += ts_duration (t1, t2);

      if (g_delay)
        usleep (g_delay*1000);
    }
  }

  timer.stop();

  return errcode;
}

int test_monitor_end ()
{
  int errcode = TEST_MONITOR_ERR_NOERR;

  for (auto it = g_it.begin (); it != g_it.end (); ++it)
    delete (*it);

  g_it.clear ();

  ts_close (TS_DUMMY);

  std::cout << "[test_monitor] ---------------------------------- "
	    << std::endl;
  std::cout << "[test_monitor] Total number of events received : "
	    << eventCount << std::endl;
  std::cout << "[test_monitor] Number of events not found : "
	    << nyield << std::endl;
  std::cout << "[test_monitor] Total CPU Time : "
	    << timer.userTime() + timer.systemTime() << std::endl;
  std::cout << "[test_monitor] Total Time : " << timer.totalTime()
	    << std::endl;
  std::cout << "[test_monitor] Event/second : "
            << eventCount/timer.totalTime() << std::endl;
  std::cout << "[test_monitor] Total readout elapsed time : "
            << elapsed << std::endl;
  std::cout << "[test_monitor] Elapsed time per event : "
            << elapsed/eventCount << std::endl;

  return errcode;
}

int main (int argc, char **argv)
{
  int errcode = TEST_MONITOR_ERR_NOERR;

  if ((errcode = test_monitor_opt (argc, argv)) != TEST_MONITOR_ERR_NOERR)
    return errcode;

  if ((errcode = test_monitor_init (argc, argv)) != TEST_MONITOR_ERR_NOERR)
    return errcode;

  if ((errcode = test_monitor_loop ()) != TEST_MONITOR_ERR_NOERR)
    return errcode;

  if ((errcode = test_monitor_end ()) != TEST_MONITOR_ERR_NOERR)
    return errcode;

  return errcode;
}
