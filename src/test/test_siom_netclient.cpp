#include <signal.h>
#include <unistd.h>

#include <thread>
#include <chrono>
#include <string>
#include <vector>

// TDAQ include files

#include "transport/NetAddress.h"

// Package include files

#include "siom/core/SIOMBackTrace.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/SIOMDataDestination.h"
#include "siom/core/SIOMDataSender.h"

#include "siom/plugin/SIOMTCPDataSender.h"
#include "siom/plugin/SIOMUDPDataSender.h"

#include "siom_info/siomPlugin.h"

#define TCP_PROTO    0
#define UDP_PROTO    1

class TestBufferInputThread : public SIOM::ActiveObject
{
 public:
  explicit TestBufferInputThread (SIOM::MemoryPool <uint8_t> *pool, unsigned int packlen)
    : _memoryPool (pool), _packlen (packlen)
  {
    this->start ();
  };

  virtual ~TestBufferInputThread ()
  {
    this->join ();
  };

 protected:
  virtual void run ();

 private:
  SIOM::MemoryPool <uint8_t> *_memoryPool;
  unsigned int _packlen;
};

// default parameters

static const std::string defdestination ("localhost");

static const unsigned int defport       = 3333;
static const unsigned int defnthreads   = 1;
static const unsigned int defpagenumber = 64;
static const unsigned int defpagesize   = 1024;
static const unsigned int defdatalen    = 1680;
static const unsigned int defprotocol   = TCP_PROTO;

char *       destination = (char *) defdestination.c_str ();

unsigned int port        = defport;
unsigned int nthreads    = defnthreads;
unsigned int pagenumber  = defpagenumber;
unsigned int pagesize    = defpagesize;
unsigned int datalen     = defdatalen;
unsigned int protocol    = defprotocol;

static bool stop = false;

std::vector <SIOM::DataSender *> clients;
std::vector <SIOM::MemoryPool <uint8_t> *> poolvector;
std::vector <TestBufferInputThread *> invector;

void TestBufferInputThread::run ()
{
  while (! _quit.load ())
  {
    if (auto *b = _memoryPool->reserve (_packlen))
      _memoryPool->validate (b, _packlen);

    std::this_thread::yield ();
  }
}

void test_siom_netclient_sighandler (int sig)
{
  if (sig == SIGINT || sig == SIGQUIT || sig == SIGTERM)
  {
    printf ("Got signal %d, cleaning up...\n", sig);
    stop = true;
  }
  else if (sig == SIGSEGV || sig == SIGABRT || sig == SIGSYS)
  {
    printf ("Got signal %d, aborting...\n", sig);
    SIOM::print_trace ();
    exit (sig);
  }
}

void test_siom_netclient_help (char *pname)
{
  printf ("Usage: %s <options>\n"
          "\t[-d <destination>] \tserver name, default=%s\n"
          "\t[-p <port>]        \tserver port, default=%d\n"
          "\t[-t <nthreads>]    \tnumber of client threads, default=%d\n"
          "\t[-n <pagenumber>]  \tnumber of memory pages per pool, default=%d\n"
          "\t[-s <pagesize>]    \tsize of memory pages, default=%d\n"
          "\t[-l <datalen>]     \tlength of data packets, default=%d\n"
	  "\t[-u]               \tuse UDP (default = TCP)\n"
          "\t[-h]               \thelp\n",
          pname, destination, port, nthreads,
	  pagenumber, pagesize, datalen);

  exit (0);
}

void test_siom_netclient_options (int argc, char **argv)
{
  int i;

  for (i=1; i < argc; ++i)
    if (argv[i][0] == '-')
      switch (argv[i][1])
      {
        case 'd':
          destination = argv[++i];
          break;
        case 'p':
          port = atoi (argv[++i]);
          break;
        case 't':
          nthreads = atoi (argv[++i]);
          break;
        case 'n':
          pagenumber = atoi (argv[++i]);
          break;
        case 's':
          pagesize = atoi (argv[++i]);
          break;
        case 'l':
          datalen = atoi (argv[++i]);
          break;
        case 'u':
          protocol = UDP_PROTO;
          break;
        case 'h':
          test_siom_netclient_help (argv[0]);
          break;
        default:
          break;
      }
}

int test_siom_netclient_init ()
{
  signal (SIGINT,  test_siom_netclient_sighandler);
  signal (SIGQUIT, test_siom_netclient_sighandler);
  signal (SIGTERM, test_siom_netclient_sighandler);
  signal (SIGSEGV, test_siom_netclient_sighandler);
  signal (SIGABRT, test_siom_netclient_sighandler);
  signal (SIGSYS,  test_siom_netclient_sighandler);

  // Start one pool and input thread for each TCP client

  std::string hostname (destination);
  NetAddress address (hostname, port);

  for (unsigned int i=0; i < nthreads; ++i)
  {
    SIOM::DataDestination data_dest (&address);

    auto p = new SIOM::MemoryPool <uint8_t> (pagesize, pagenumber);
    auto in = new TestBufferInputThread (p, datalen);

    poolvector.push_back (p);

    SIOM::DataSender *c;

    try
    {
      switch (protocol)
      { 
        case TCP_PROTO:
	  c = new SIOM::TCPDataSender (poolvector, &data_dest);
	  break;
        case UDP_PROTO:
	  c = new SIOM::UDPDataSender (poolvector, &data_dest);
	  break;
      }
    }

    catch (transport::TransportException &e)
    {
      ers::fatal (e);
      return 1;
    }

    clients.push_back (c);
    invector.push_back (in);
  }

  return 0;
}

void test_siom_netclient_end ()
{
  unsigned int i;

  for (i=0; i < nthreads; ++i)
  {
    delete clients[i];
    delete invector[i];
    delete poolvector[i];
  }
}

void test_siom_netclient_loop ()
{
  while (! stop)
  {
    double Mbytes_sent = 0;
    uint64_t packets_sent = 0;

    for (unsigned int i=0; i < nthreads; ++i)
    {
      siom_info::siomPlugin *SenderInfo;
      SenderInfo = dynamic_cast <siom_info::siomPlugin *> (clients[i]->getISInfo ());
      Mbytes_sent  += SenderInfo->ProcessedData;
      packets_sent += SenderInfo->ProcessedPackets;
    }

    std::cout << "MBytes sent " << Mbytes_sent << std::endl;
    std::cout << "Packets sent " << packets_sent << std::endl;

    std::this_thread::sleep_for (std::chrono::seconds (3));

    std::cout << "stop = " << stop << std::endl;
  }
}

int main (int argc, char **argv)
{
  int err = 0;

  test_siom_netclient_options (argc, argv);

  if ((err = test_siom_netclient_init ()) != 0)
    return err;

  test_siom_netclient_loop ();
  test_siom_netclient_end ();

  return 0;
}
