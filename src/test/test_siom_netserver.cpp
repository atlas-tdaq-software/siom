#include <stdio.h>
#include <signal.h>
#include <unistd.h>

#include <thread>
#include <chrono>
#include <string>
#include <vector>

// TDAQ include files

#include "is/infodictionary.h"

// Package include files

#include "siom/core/SIOMBackTrace.h"
#include "siom/core/SIOMMemoryPool.h"
#include "siom/core/SIOMMemoryManager.h"
#include "siom/core/SIOMActiveObject.h"

#include "siom/plugin/SIOMTCPServer.h"
#include "siom/plugin/SIOMUDPServer.h"

#include "siom_info/siomPlugin.h"

#define TCP_PROTO    0
#define UDP_PROTO    1

class TestBufferOutputThread : public SIOM::ActiveObject
{
 public:
  explicit TestBufferOutputThread (SIOM::MemoryPool <uint8_t> *pool)
    : _memoryPool (pool) , _npackets (0)
  {
    this->start ();
  };

  virtual ~TestBufferOutputThread ()
  {
    this->join ();
  };

  uint64_t npackets () {return _npackets;};

 protected:
  virtual void run ();

 private:
  SIOM::MemoryPool <uint8_t> *_memoryPool;
  uint64_t _npackets;
};

// default parameters

static const unsigned int defport       = 3333;
static const unsigned int defmaxthreads = 1;
static const unsigned int defpoolnumber = 1;
static const unsigned int defpagenumber = 64;
static const unsigned int defpagesize   = 1024;
static const unsigned int defprotocol   = TCP_PROTO;

unsigned int port        = defport;
unsigned int maxthreads  = defmaxthreads;
unsigned int poolnumber  = defpoolnumber;
unsigned int pagenumber  = defpagenumber;
unsigned int pagesize    = defpagesize;
unsigned int protocol    = defprotocol;

static bool pack = false;
static bool stop = false;

std::vector <SIOM::MemoryPool <uint8_t> *> poolvector;
std::vector <TestBufferOutputThread *> outvector;

SIOM::InputServer *s;

void TestBufferOutputThread::run ()
{
  while (! _quit.load ())
  {
    auto *b = _memoryPool->get_next ();

    if (b)
    {
      _memoryPool->release (b);
      ++_npackets;
    }

    std::this_thread::yield ();
  }
}

void test_siom_netserver_sighandler (int sig)
{
  if (sig == SIGINT || sig == SIGQUIT || sig == SIGTERM)
  {
    printf ("Got signal %d, cleaning up...\n", sig);
    stop = true;
  }
  else if (sig == SIGSEGV || sig == SIGABRT || sig == SIGSYS)
  {
    printf ("Got signal %d, aborting...\n", sig);
    SIOM::print_trace ();
    exit (sig);
  }
}

void test_siom_netserver_help (char *pname)
{
  printf ("Usage: %s <options>\n"
          "\t[-p <port>]        \tserver port, default=%d\n"
          "\t[-t <maxthreads>]  \tmaximum number of threads, default=%d\n"
          "\t[-b <poolnumber>]  \tnumber of memory pools, default=%d\n"
          "\t[-n <pagenumber>]  \tnumber of memory pages per pool, default=%d\n"
          "\t[-s <pagesize>]    \tsize of memory pages, default=%d\n"
          "\t[-u]               \tused UDP protocol (default = TCP)\n"
          "\t[-p]               \tpack events in bigger pages\n"
          "\t[-h]               \thelp\n",
          pname, port, maxthreads, poolnumber,
	  pagenumber, pagesize);

  exit (0);
}

void test_siom_netserver_options (int argc, char **argv)
{
  int i;

  for (i=1; i < argc; ++i)
    if (argv[i][0] == '-')
      switch (argv[i][1])
      {
        case 'p':
          port = atoi (argv[++i]);
          break;
        case 't':
          maxthreads = atoi (argv[++i]);
          break;
        case 'b':
          poolnumber = atoi (argv[++i]);
          break;
        case 'n':
          pagenumber = atoi (argv[++i]);
          break;
        case 's':
          pagesize = atoi (argv[++i]);
          break;
        case 'c':
	  pack = true;
	  break;
        case 'u':
          protocol = UDP_PROTO;
          break;
        case 'h':
          test_siom_netserver_help (argv[0]);
          break;
        default:
          break;
      }
}

void test_siom_netserver_init ()
{
  signal (SIGINT,  test_siom_netserver_sighandler);
  signal (SIGQUIT, test_siom_netserver_sighandler);
  signal (SIGTERM, test_siom_netserver_sighandler);
  signal (SIGSEGV, test_siom_netserver_sighandler);
  signal (SIGABRT, test_siom_netserver_sighandler);
  signal (SIGSYS,  test_siom_netserver_sighandler);

  for (unsigned int i=0; i < poolnumber; ++i)
  {
    auto p = new SIOM::MemoryPool <uint8_t> (pagesize, pagenumber);
    auto out = new TestBufferOutputThread (p);

    poolvector.push_back (p);
    outvector.push_back (out);
  }

  switch (protocol)
  {
    case TCP_PROTO:
      s = new SIOM::TCPServer (poolvector, maxthreads, port, pack);
      break;
    case UDP_PROTO:
      s = new SIOM::UDPServer (poolvector, maxthreads, port, pack);
      break;
  }
}

void test_siom_netserver_end ()
{
  delete s;

  for (unsigned int i=0; i < poolnumber; ++i)
  {
    delete outvector[i];
    delete poolvector[i];
  }
}

void test_siom_netserver_loop ()
{
  while (! stop)
  {
    unsigned int np;

    siom_info::siomPlugin *Info;
    Info = dynamic_cast <siom_info::siomPlugin *> (s->getISInfo ());

    np = 0;

    for (unsigned int i=0; i < poolnumber; ++i)
      np += outvector[i]->npackets ();

    std::cout << "Active threads "
	      << Info->ActiveThreads << std::endl;
    std::cout << "Packets received "
	      << Info->ProcessedPackets << std::endl;
    std::cout << "MBytes received "
	      << Info->ProcessedData << std::endl;
    std::cout << "Event rate "
              << Info->PacketRate << std::endl;
    std::cout << "Data rate "
	      << Info->DataRate << " MB/s" << std::endl;
    std::cout << "Packets processed " << np << std::endl;

    std::this_thread::sleep_for (std::chrono::seconds (5));

    std::cout << "stop = " << stop << std::endl;
  }
}

int main (int argc, char **argv)
{
  test_siom_netserver_options (argc, argv);
  test_siom_netserver_init ();
  test_siom_netserver_loop ();
  test_siom_netserver_end ();

  return 0;
}
